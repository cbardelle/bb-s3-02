package eu.unicreditgroup.test.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.MappedRow;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.ResultColumn;
import eu.unicreditgroup.config.SourceColumn;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.mapper.AgeMapper;
import eu.unicreditgroup.mapper.CompetenceLineMapper;
import eu.unicreditgroup.mapper.CountryIDMapper;
import eu.unicreditgroup.mapper.CurrencyIDMapper;
import eu.unicreditgroup.mapper.DirectMapper;
import eu.unicreditgroup.mapper.DirectMapperPlusGeMngr;
import eu.unicreditgroup.mapper.DivisionMapper;
import eu.unicreditgroup.mapper.DomainDescMapper;
import eu.unicreditgroup.mapper.DomainIDMapper;
import eu.unicreditgroup.mapper.EmployeeStatusDescMapper;
import eu.unicreditgroup.mapper.EmployeeStatusIDMapper;
import eu.unicreditgroup.mapper.FixedValueMapper;
import eu.unicreditgroup.mapper.FixedValuePlusGeMngrMapper;
import eu.unicreditgroup.mapper.FteMapper;
import eu.unicreditgroup.mapper.FullDepartmentCodeMapper;
import eu.unicreditgroup.mapper.FullTimeMapper;
import eu.unicreditgroup.mapper.IMapper;
import eu.unicreditgroup.mapper.LocalJobPositionMapper;
import eu.unicreditgroup.mapper.LocaleIDMapper;
import eu.unicreditgroup.mapper.NotActiveMapper;
import eu.unicreditgroup.mapper.OrgDescMapper;
import eu.unicreditgroup.mapper.OrgIdMapper;
import eu.unicreditgroup.mapper.OrgUnitLocTypeMapper;
import eu.unicreditgroup.mapper.PlateauDateMapper;
import eu.unicreditgroup.mapper.RegionIDMapper;
import eu.unicreditgroup.mapper.RoleMapper;
import eu.unicreditgroup.mapper.RoleSeniorityMapper;
import eu.unicreditgroup.mapper.TableMapper;
import eu.unicreditgroup.mapper.TimeZoneMapper;
import eu.unicreditgroup.mapper.YesNoMapper;
import eu.unicreditgroup.mapper.YesNoPlusGeMngrMapper;
import eu.unicreditgroup.test.BaseSpringTest;


public class TableMapperTest extends BaseSpringTest{
	
	
	
	private final int SAP_SRC_COLS_CNT = 91;
	
	//@Resource(name = "reportDataBean")  
	//private ReportDataBean reportDataBean;  
	
	@Resource(name = "tableMapper")
	private TableMapper tableMapper;
	
	@Resource(name = "sourceColumns")
	private List<SourceColumn> sourceColumns;
	
	
	@Resource(name = "sourceFileConf")
	private SourceFileConf sourceFileConf;
	
	
	private String toId(String s){
		if (s == null) return s;
		
		if (StringUtils.equals(s,StringUtils.upperCase(s))){
			return s;
		}
		
		StringBuilder sb = new StringBuilder();
		
		int upperCounter = 0;
		
		for(int i = 0; i < s.length() ; i++){			
			char ch= s.charAt(i);
			if (i !=0 && upperCounter == 0 && (Character.isUpperCase(ch) || Character.isDigit(ch) ) ){
			
				sb.append("_");				
				upperCounter+=1;
			} else {
				upperCounter=0;	
			}
			sb.append(ch);
		}
		return sb.toString().toUpperCase();		
	}
	
	//Source test data
	private final String USER_STATUS_ID = "3";  //UserStatusID, length: 1
	private final String USER_STATUS_DESC = "Active";  //UserStatusDesc, length: 80
	private final String USER_ID = "UV00000";  //UserID, length: 30
	private final String GERMAN_MANAGER_USER_ID = "UV00001";  //UserID, length: 30
	private final String FIRST_NAME = "Scott";  //FirstName, length: 80
	private final String LAST_NAME = "Tiger";  //LastName, length: 80
	private final String GENDER = "M";  //Gender, length: 1
	private final String GLOBAL_JOB_CODE = "GlobalJobCode";  //GlobalJobCode, length: 15
	private final String GLOBAL_JOB_ABBR = "GlobalJobAbbr";  //GlobalJobAbbr, length: 25
	private final String GLOBAL_JOB_DESC = "GlobalJobDesc";  //GlobalJobDesc, length: 70
	private final String USER_OFFICE_CODE = "UserOfficeCode";  //UserOfficeCode, length: 20
	private final String USER_OFFICE_ABBR = "UserOfficeAbbr";  //UserOfficeAbbr, length: 30
	private final String USER_OFFICE_DESC = "UserOfficeDesc";  //UserOfficeDesc, length: 80
	private final String HOST_LEGAL_ENT_CODE = "HostLegalEntCode";  //HostLegalEntCode, length: 20
	private final String HOST_LEGAL_ENT_DESC = "HostLegalEntDesc";  //HostLegalEntDesc, length: 80		
	private final String CONTRACT_TYPE_CODE = "X1";  //ContractTypeCode, length: 5
	private final String CONTRACT_TYPE_DESC = "Full-time Contract";  //ContractTypeDesc, length: 40
	private final String CONTRACT_DETAILS = "ContractDetails";  //ContractDetails, length: 20
	private final String OFFICE_ADDR = "OfficeAddr";  //OfficeAddr, length: 150
	private final String OFFICE_CITY = "OfficeCity";  //OfficeCity, length: 100
	private final String OFFICE_PROVINCE = "OfficeProvince";  //OfficeProvince, length: 3
	private final String OFFICE_PROVINCE_DESC = "OfficeProvinceDesc";  //OfficeProvinceDesc, length: 50
	private final String OFFICE_POSTAL_CODE = "OfficePostalCode";  //OfficePostalCode, length: 30
	private final String OFFICE_COUNTRY = "PL";  //OfficeCountry, length: 3
	private final String OFFICE_COUNTRY_DESC = "Polish";  //OfficeCountryDesc, length: 50
	private final String OFFICE_EMAIL = "SCOTT.TIGER@UNICREDITGROUP.EU";  //OfficeEmail, length: 350
	private final String HIRE_DATE = "01.12.1999";  //HireDate, length: 30
	private final String TERM_DATE = "";  //TermDate, length: 30
	private final String LINE_MANAGER_ID = "UV00001";  //LineManagerID, length: 20
	private final String RESUME = "UV00000";  //Resume, length: 30
	private final String COST_CENTER_CODE = "CostCenterCode";  //CostCenterCode, length: 20
	private final String OFFICE_MOBILE_PHONE = "OfficeMobilePhone";  //OfficeMobilePhone, length: 100
	private final String OFFICE_FIX_PHONE = "OfficeFixPhone";  //OfficeFixPhone, length: 100
	private final String FISCAL_CODE = "FiscalCode";  //FiscalCode, length: 50
	private final String ORG_1ST_LEVEL_CODE = "Org1stLevelCode";  //Org1stLevelCode, length: 15
	private final String ORG_1ST_LEVEL_ABBR = "Org1stLevelAbbr";  //Org1stLevelAbbr, length: 25
	private final String ORG_1ST_LEVEL_DESC = "Org1stLevelDesc";  //Org1stLevelDesc, length: 70
	private final String ORG_2ND_LEVEL_CODE = "Org2ndLevelCode";  //Org2ndLevelCode, length: 15
	private final String ORG_2ND_LEVEL_ABBR = "Org2ndLevelAbbr";  //Org2ndLevelAbbr, length: 25
	private final String ORG_2ND_LEVEL_DESC = "Org2ndLevelDesc";  //Org2ndLevelDesc, length: 70
	private final String HOME_LEGAL_ENTITY_CODE = "HomeLegalEntityCode";  //HomeLegalEntiryCode, length: 10
	private final String HOME_LEGAL_ENTITY_DESC = "HomeLegalEntityDesc";  //HomeLegalEntiryDesc, length: 50
	private final String DIVISION_CODE = "DivisionCode";  //DivisionCode, length: 20
	private final String DIVISION_ABBR = "DivisionAbbr";  //DivisionAbbr, length: 30
	private final String DIVISION_DESC = "DivisionDesc";  //DivisionDesc, length: 60
	private final String COMPETENCE_LINE_CODE = "CompetenceLineCode";  //CompetenceLineCode, length: 20
	private final String COMPETENCE_LINE_ABBR = "CompetenceLineAbbr";  //CompetenceLineAbbr, length: 30
	private final String COMPETENCE_LINE_DESC = "CompetenceLineDesc";  //CompetenceLineDesc, length: 60
	private final String HRBPID = "UV00002";  //HRBPID, length: 30
	private final String HRBP = "N";  //HRBP, length: 1
	private final String LINE_MANAGER = "N";  //LineManager, length: 1
	private final String GLOBAL_JOB_BAND_CODE = "GlobalJobBandCode";  //GlobalJobBandCode, length: 15
	private final String GLOBAL_JOB_BAND_ABBR = "GlobalJobBandAbbr";  //GlobalJobBandAbbr, length: 20
	private final String GLOBAL_JOB_BAND_DESC = "GlobalJobBandDesc";  //GlobalJobBandDesc, length: 60
	private final String JOB_ROLE_CODE = "JobRoleCode";  //JobRoleCode, length: 15
	private final String JOB_ROLE_ABBR = "JobRoleAbbr";  //JobRoleAbbr, length: 25
	private final String JOB_ROLE_DESC = "JobRoleDesc";  //JobRoleDesc, length: 70
	private final String PAY_GRADE_CODE = "PayGradeCode";  //PayGradeCode, length: 10
	private final String PAY_GRADE_DESC = "PayGradeDesc";  //PayGradeDesc, length: 60
	private final String PREV_JOB_ROLE_CODE = "PrevJobRoleCode";  //PrevJobRoleCode, length: 15
	private final String PREV_JOB_ROLE_ABBR = "PrevJobRoleAbbr";  //PrevJobRoleAbbr, length: 25
	private final String PREV_JOB_ROLE_DESC = "PrevJobRoleDesc";  //PrevJobRoleDesc, length: 70
	private final String SECOND_PREV_JOB_ROLE_CODE = "SecondPrevJobRoleCode";  //SecondPrevJobRoleCode, length: 15
	private final String SECOND_PREV_JOB_ROLE_ABBR = "SecondPrevJobRoleAbbr";  //SecondPrevJobRoleAbbr, length: 25
	private final String SECOND_PREV_JOB_ROLE_DESC = "SecondPrevJobRoleDesc";  //SecondPrevJobRoleDesc, length: 70
	private final String JOB_ROLE_START_DATE = "01.12.2009";  //JobRoleStartDate, length: 30
	private final String DATE_OF_BIRTH = "28.12.1976";  //DateOfBirth, length: 30
	private final String NATION_ID = "PL";  //NationID, length: 3
	private final String NATION_DESC = "Poland";  //NationDesc, length: 80
	private final String EXPATRIATE = "N";  //Expatriate, length: 1
	private final String EDU_TITLE_ID = "02";  //EduTitleID, length: 5
	private final String EDU_TITLE_DESC = "University";  //EduTitleDesc, length: 50
	private final String LOCAL_LANG = "PL";  //LocalLang, length: 3
	private final String LOCAL_LANG_DESC = "Polish";  //LocalLangDesc, length: 100
	private final String CURRENCY_ID = "PLN";  //CurrencyID, length: 3
	private final String COST_CENTER_DESC = "CostCenterDesc";  //CostCenterDesc, length: 80
	private final String USER_SAP_ID = "UserSapID";  //UserSapID, length: 30
	private final String SUPERVISOR_SAP_ID = "SupervisorSapID";  //SupervisorSapID, length: 30
	private final String HRBP_SAP_ID = "HrbpSapID";  //HrbpSapID, length: 30
	private final String JOB_SENIORITY_CODE = "JobSeniorityCode";  //JobSeniorityCode, length: 1
	private final String JOB_SENIORITY_DESC = "JobSeniorityDesc";  //JobSeniorityDesc, length: 15
	private final String SERVICE_LINE_CODE = "ServiceLineCode";  //ServiceLineCode, length: 8
	private final String SERVICE_LINE_DESC = "ServiceLineDesc";  //ServiceLineDesc, length: 40
	private final String BUSINESS_LINE_CODE = "BusinessLineCode";  //BusinessLineCode, length: 8
	private final String BUSINESS_LINE_DESC = "BusinessLineDesc";  //BusinessLineDesc, length: 40
	
	private final String EXPAT_TYPE = "I"; //ExpatType I(nbound), O(utbound) or null, length: 1
	private final String FUNCTIONAL_RESP = ""; // Functional responsible (Y/N), length: 1
	private final String FTE	= "75.0"; //FTE value (decimal number), length: 6
	private final String ORG_DISTRICT_CODE = "OrgDstCd"; // Organization district code, length: 8
	private final String ORG_DISTRICT_NAME = "OrgDistrictName"; // Organization district name, length: 40
	private final String ORG_DISTRICT_DESC = "OrgDistrictDesc"; // Organization district desc, length: 40
	private final String ORG_UNIT_LOC_TYPE = "l"; // Org unit location type, length: 1
	
	
	private void setupGeManagerSourceData(String[] data){
		setupSourceData(data);
		data[2]= GERMAN_MANAGER_USER_ID; 
	}
	
	private void setupSourceData(String[] data){
		data[0]= USER_STATUS_ID; //UserStatusID, length: 1
		data[1]= USER_STATUS_DESC; //UserStatusDesc, length: 80
		data[2]= USER_ID; //UserID, length: 30
		data[3]= FIRST_NAME; //FirstName, length: 80
		data[4]= LAST_NAME; //LastName, length: 80
		data[5]= GENDER; //Gender, length: 1
		data[6]= GLOBAL_JOB_CODE; //GlobalJobCode, length: 15
		data[7]= GLOBAL_JOB_ABBR; //GlobalJobAbbr, length: 25
		data[8]= GLOBAL_JOB_DESC; //GlobalJobDesc, length: 70
		data[9]= USER_OFFICE_CODE; //UserOfficeCode, length: 20
		data[10]= USER_OFFICE_ABBR; //UserOfficeAbbr, length: 30
		data[11]= USER_OFFICE_DESC; //UserOfficeDesc, length: 80
		data[12]= HOST_LEGAL_ENT_CODE; //HostLegalEntCode, length: 20
		data[13]= HOST_LEGAL_ENT_DESC; //HostLegalEntDesc, length: 80
		data[14]= CONTRACT_TYPE_CODE; //ContractTypeCode, length: 5
		data[15]= CONTRACT_TYPE_DESC; //ContractTypeDesc, length: 40
		data[16]= CONTRACT_DETAILS; //ContractDetails, length: 20
		data[17]= OFFICE_ADDR; //OfficeAddr, length: 150
		data[18]= OFFICE_CITY; //OfficeCity, length: 100
		data[19]= OFFICE_PROVINCE; //OfficeProvince, length: 3
		data[20]= OFFICE_PROVINCE_DESC; //OfficeProvinceDesc, length: 50
		data[21]= OFFICE_POSTAL_CODE; //OfficePostalCode, length: 30
		data[22]= OFFICE_COUNTRY; //OfficeCountry, length: 3
		data[23]= OFFICE_COUNTRY_DESC; //OfficeCountryDesc, length: 50
		data[24]= OFFICE_EMAIL; //OfficeEmail, length: 350
		data[25]= HIRE_DATE; //HireDate, length: 30
		data[26]= TERM_DATE; //TermDate, length: 30
		data[27]= LINE_MANAGER_ID; //LineManagerID, length: 20
		data[28]= RESUME; //Resume, length: 30
		data[29]= COST_CENTER_CODE; //CostCenterCode, length: 20
		data[30]= OFFICE_MOBILE_PHONE; //OfficeMobilePhone, length: 100
		data[31]= OFFICE_FIX_PHONE; //OfficeFixPhone, length: 100
		data[32]= FISCAL_CODE; //FiscalCode, length: 50
		data[33]= ORG_1ST_LEVEL_CODE; //Org1stLevelCode, length: 15
		data[34]= ORG_1ST_LEVEL_ABBR; //Org1stLevelAbbr, length: 25
		data[35]= ORG_1ST_LEVEL_DESC; //Org1stLevelDesc, length: 70
		data[36]= ORG_2ND_LEVEL_CODE; //Org2ndLevelCode, length: 15
		data[37]= ORG_2ND_LEVEL_ABBR; //Org2ndLevelAbbr, length: 25
		data[38]= ORG_2ND_LEVEL_DESC; //Org2ndLevelDesc, length: 70
		data[39]= HOME_LEGAL_ENTITY_CODE; //HomeLegalEntityCode, length: 10
		data[40]= HOME_LEGAL_ENTITY_DESC; //HomeLegalEntityDesc, length: 50
		data[41]= DIVISION_CODE; //DivisionCode, length: 20
		data[42]= DIVISION_ABBR; //DivisionAbbr, length: 30
		data[43]= DIVISION_DESC; //DivisionDesc, length: 60
		data[44]= COMPETENCE_LINE_CODE; //CompetenceLineCode, length: 20
		data[45]= COMPETENCE_LINE_ABBR; //CompetenceLineAbbr, length: 30
		data[46]= COMPETENCE_LINE_DESC; //CompetenceLineDesc, length: 60
		data[47]= HRBPID; //HRBPID, length: 30
		data[48]= HRBP; //HRBP, length: 1
		data[49]= LINE_MANAGER; //LineManager, length: 1
		data[50]= GLOBAL_JOB_BAND_CODE; //GlobalJobBandCode, length: 15
		data[51]= GLOBAL_JOB_BAND_ABBR; //GlobalJobBandAbbr, length: 20
		data[52]= GLOBAL_JOB_BAND_DESC; //GlobalJobBandDesc, length: 60
		data[53]= JOB_ROLE_CODE; //JobRoleCode, length: 15
		data[54]= JOB_ROLE_ABBR; //JobRoleAbbr, length: 25
		data[55]= JOB_ROLE_DESC; //JobRoleDesc, length: 70
		data[56]= PAY_GRADE_CODE; //PayGradeCode, length: 10
		data[57]= PAY_GRADE_DESC; //PayGradeDesc, length: 60
		data[58]= PREV_JOB_ROLE_CODE; //PrevJobRoleCode, length: 15
		data[59]= PREV_JOB_ROLE_ABBR; //PrevJobRoleAbbr, length: 25
		data[60]= PREV_JOB_ROLE_DESC; //PrevJobRoleDesc, length: 70
		data[61]= SECOND_PREV_JOB_ROLE_CODE; //SecondPrevJobRoleCode, length: 15
		data[62]= SECOND_PREV_JOB_ROLE_ABBR; //SecondPrevJobRoleAbbr, length: 25
		data[63]= SECOND_PREV_JOB_ROLE_DESC; //SecondPrevJobRoleDesc, length: 70
		data[64]= JOB_ROLE_START_DATE; //JobRoleStartDate, length: 30
		data[65]= DATE_OF_BIRTH; //DateOfBirth, length: 30
		data[66]= NATION_ID; //NationID, length: 3
		data[67]= NATION_DESC; //NationDesc, length: 80
		data[68]= EXPATRIATE; //Expatriate, length: 1
		data[69]= EDU_TITLE_ID; //EduTitleID, length: 5
		data[70]= EDU_TITLE_DESC; //EduTitleDesc, length: 50
		data[71]= LOCAL_LANG; //LocalLang, length: 3
		data[72]= LOCAL_LANG_DESC; //LocalLangDesc, length: 100
		data[73]= CURRENCY_ID; //CurrencyID, length: 3
		data[74]= COST_CENTER_DESC; //CostCenterDesc, length: 80
		data[75]= USER_SAP_ID; //UserSapID, length: 30
		data[76]= SUPERVISOR_SAP_ID; //SupervisorSapID, length: 30
		data[77]= HRBP_SAP_ID; //HrbpSapID, length: 30
		data[78]= JOB_SENIORITY_CODE; //JobSeniorityCode, length: 1
		data[79]= JOB_SENIORITY_DESC; //JobSeniorityDesc, length: 15
		data[80]= SERVICE_LINE_CODE; //ServiceLineCode, length: 8
		data[81]= SERVICE_LINE_DESC; //ServiceLineDesc, length: 40
		data[82]= BUSINESS_LINE_CODE; //BusinessLineCode, length: 8
		data[83]= BUSINESS_LINE_DESC; //BusinessLineDesc, length: 40
		
		data[84]= EXPAT_TYPE; //ExpatType I(nbound), O(utbound) or null, length: 1
		data[85]= FUNCTIONAL_RESP; // Functional responsible (Y/N), length: 1
		data[86]= FTE; //FTE value (decimal number), length: 6
		data[87]= ORG_DISTRICT_CODE; // Organization district code, length: 8
		data[88]= ORG_DISTRICT_NAME; // Organization district name, length: 40
		data[89]= ORG_DISTRICT_DESC; // Organization district description, length: 40
		
		data[90]= ORG_UNIT_LOC_TYPE; // Org unit location type, length: 1
		
	}
	
	
	
	
	
	/**
	 * EmpStatusIDMapper value not existing in the dictionary table 
	 */
	@Test
	public void employeeStatusIDMapperNonDictValueTest(){

		ResultColumn empStatusID = tableMapper.getResultColumns().get(18); // empStatusID result column
		
		IMapper maper = empStatusID.getMapper();
		
		String[] srcVals = new String[]{"AAA" /*srcContractTypeCode*/, "desc" /*srcContractDetails*/ };
		
		MappedColumn mappedCol = maper.map(srcVals,  "DEFAULT", new SourceRow());// value not in the dictionary
		
		assertTrue(StringUtils.startsWith(mappedCol.getMappingException().getMessage(), "Failed to map"));
		assertNull(mappedCol.getValue());		
	}
	
	/**
	 * EmpStatusIDMapper Value present in the dictionary test
	 */
	@Test
	public void employeeStatusIDMapperProperDictValueTest(){

		ResultColumn empStatusID = tableMapper.getResultColumns().get(18); // empStatusID result column
		
		IMapper maper = empStatusID.getMapper();
		
		String[] srcVals = new String[]{"X1" /*srcContractTypeCode*/, "NOT USED" /*srcContractDetails*/ };
		
		MappedColumn mappedCol = maper.map(srcVals,  "DEFAULT", new SourceRow());// value not in the dictionary
		
		//System.out.println(mappedCol.getMappingException().getMessage());
		//System.out.println(mappedCol.getValue());
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"FT");		
	}
	
	/**
	 * Local italian contract type code
	 * (local italian values are not in use any more)
	 */
	@Test	
	public void employeeStatusIDMapperLocalValueTest(){

		ResultColumn empStatusID = tableMapper.getResultColumns().get(18); // empStatusID result column
		
		IMapper maper = empStatusID.getMapper();
		
		String[] srcVals = new String[]{"Z1" /*srcContractTypeCode*/, null /*srcContractDetails*/ };
		
		MappedColumn mappedCol = maper.map(srcVals,  "DEFAULT", new SourceRow());// value not in the dictionary
		
		//System.out.println(mappedCol.getMappingException().getMessage());
		//System.out.println(mappedCol.getValue());
		
		//assertTrue(StringUtils.startsWith(mappedCol.getMappingException().getMessage(), "Failed to map"));
		assertNull(mappedCol.getValue());		
	}
	
	/**
	 * EmpStatusDescMapper value present in the dictionary test
	 */
	@Test
	public void employeeStatusDescMapperProperDictValueTest(){

		ResultColumn empStatusDesc = tableMapper.getResultColumns().get(19); // empStatusDesc result column
		
		EmployeeStatusDescMapper maper = (EmployeeStatusDescMapper)empStatusDesc.getMapper();
		
		String[] srcVals = new String[]{"X1" /*srcContractTypeCode*/, "NOT USED" /*srcContractDetails*/ };
		
		MappedColumn mappedCol = maper.map(srcVals,  "DEFAULT", new SourceRow());// value not in the dictionary
		
		//System.out.println(mappedCol.getErrorMsg());
		//System.out.println(mappedCol.getValue());
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"FULL TIME");		
	}
	
	/**
	 * EmpStatusDescMapper local value present in the dictionary test
	 */
	@Test
	public void employeeStatusDescMapperLocalValueTest(){

		ResultColumn empStatusDesc = tableMapper.getResultColumns().get(19); // empStatusDesc result column
		
		EmployeeStatusDescMapper maper = (EmployeeStatusDescMapper)empStatusDesc.getMapper();
		
		String[] srcVals = new String[]{"Z1" /*srcContractTypeCode*/, "NOT USED" /*srcContractDetails*/ };
		
		MappedColumn mappedCol = maper.map(srcVals,  "DEFAULT", new SourceRow());// value not in the dictionary
		
		//System.out.println(getMappingException().getMessage());
		//System.out.println(mappedCol.getValue());
		
		//assertTrue(StringUtils.startsWith(mappedCol.getErrorMsg(),"Failed to map"));
		assertNull(mappedCol.getValue());		
	}

	/**
	 * EmpStatusDescMapper value not present in the dictionary test
	 */
	@Test
	public void employeeStatusDescMapperNotPresentValueTest(){

		ResultColumn empStatusDesc = tableMapper.getResultColumns().get(19); // empStatusDesc result column
		
		EmployeeStatusDescMapper maper = (EmployeeStatusDescMapper)empStatusDesc.getMapper();
		
		String[] srcVals = new String[]{"AAA" /*srcContractTypeCode*/, "NOT USED" /*srcContractDetails*/ };
		
		MappedColumn mappedCol = maper.map(srcVals,  "DEFAULT", new SourceRow());// value not in the dictionary
		
		//System.out.println(mappedCol.getErrorMsg());
		//System.out.println(mappedCol.getValue());
		
		assertTrue(StringUtils.startsWith(mappedCol.getMappingException().getMessage(),"Failed to map"));
		assertNull(mappedCol.getValue());		
	}
	
	/**
	 * Age mapper positive test
	 */
	@Test 
	public void ageMapperPositiveTest(){
		
		
		DateTimeFormatter parser = DateTimeFormat.forPattern("dd.MM.yyyy");		
		DateTime dateOfBirth = parser.parseDateTime("28.12.1976");		
		Years years = Years.yearsBetween(dateOfBirth, new DateTime());
		Integer age = years.getYears();
		
		ResultColumn ageCust160 = tableMapper.getResultColumns().get(62); //  result column age CC160
		
		AgeMapper mapper = (AgeMapper)ageCust160.getMapper();
		MappedColumn mappedCol = mapper.map(new String[]{"28.12.1976"}, "DEFAULT", new SourceRow());
		
		assertEquals( Integer.valueOf(mappedCol.getValue()), age);
		
	}
	
	/**
	 * Age mapper test - wrong format of birth date 
	 */
	@Test 
	public void ageMapperWrongDateFormatTest(){
		
		
		DateTimeFormatter parser = DateTimeFormat.forPattern("dd.MM.yyyy");		
		DateTime dateOfBirth = parser.parseDateTime("28.12.1976");		
		Years years = Years.yearsBetween(dateOfBirth, new DateTime());
		Integer age = years.getYears();
		
		ResultColumn ageCust160 = tableMapper.getResultColumns().get(62); //  result column age CC160
		
		AgeMapper mapper = (AgeMapper)ageCust160.getMapper();
		MappedColumn mappedCol = mapper.map(new String[]{"28-12-1976"}, "DEFAULT", new SourceRow());
		
		//System.out.println(mappedCol.getErrorMsg());
		//System.out.println(mappedCol.getValue());
		assertNull( mappedCol.getValue());
		assertTrue(StringUtils.startsWith(mappedCol.getMappingException().getMessage(), "Failed to parse"));
		
	}
	
	@Test
	public void localeIDMapperTest(){

		ResultColumn localeIdCol = tableMapper.getResultColumns().get(50); // locale result column
		
		LocaleIDMapper maper = (LocaleIDMapper)localeIdCol.getMapper();
		
		String[] srcVals = new String[]{"EN"};		
		MappedColumn mappedCol = maper.map(srcVals,  "English (UK)", new SourceRow());// value NOT IN the dictionary		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"English (UK)");
		
		srcVals = new String[]{"IT"};		
		MappedColumn mappedCol1 = maper.map(srcVals,  "English (UK)", new SourceRow());// value IN the dictionary		
		assertNull(mappedCol1.getMappingException());
		assertEquals(mappedCol1.getValue(),"Italian");		
		
		srcVals = new String[]{"DE"};		
		MappedColumn mappedCol2 = maper.map(srcVals,  "English (UK)", new SourceRow());// value IN the dictionary		
		assertNull(mappedCol2.getMappingException());
		assertEquals(mappedCol2.getValue(),"German");		
				
		/*
		 (refactored)
		srcVals = new String[]{"ENG"};		
		MappedColumn mappedCol3 = maper.map(srcVals,  "English (UK)", new SourceRow());// value not the dictionary		
		assertTrue(StringUtils.startsWith(mappedCol3.getErrorMsg(),"Failed to"));
		assertNull(mappedCol3.getValue());
		*/		
				
		srcVals = new String[]{"CS"};		
		MappedColumn mappedCol4 = maper.map(srcVals,  "English (UK)", new SourceRow());// value NOT IN the dictionary		
		assertNull(mappedCol4.getMappingException());
		assertEquals(mappedCol4.getValue(),"English (UK)");

		srcVals = new String[]{"GB"};		
		MappedColumn mappedCol5 = maper.map(srcVals,  "English (UK)", new SourceRow());// value IN the dictionary		
		assertNull(mappedCol5.getMappingException());
		assertEquals(mappedCol5.getValue(),"English (UK)");

		
		//System.out.println(mappedCol4.getErrorMsg());
		//System.out.println(mappedCol4.getValue());				
	}

	@Test
	public void directMapperTest(){
		
		ResultColumn userId = tableMapper.getResultColumns().get(1);
		DirectMapper maper = (DirectMapper)userId.getMapper();
		String[] srcVals = new String[]{"AAA","BBB","CCC","DDD","EEE"};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"AAA-BBB-CCC-DDD-EEE");		
	}
	
	
	/**
	 * This a test for the custom configuration of user role 
	 * for user involved in "raccolta formativa 2015-2016" 
	 * 
	 */
	@Test
	public void roleMapperRaccoltaFormativa01Test(){		
		final String USER_ROLE_TRAIN_COLLECT = "UCG_USER_RF"; 		
		String[] srcVals = new String[]{"UV00074"};		
		String[] data = new String[SAP_SRC_COLS_CNT];
		// setup data
		data[0]= "3"; //UserStatusID, length: 1
		data[1]= "Active"; //UserStatusDesc, length: 80
		data[2]= "UV00074"; //UserID, length: 30
		data[49]= "Y"; //LineManager, length: 1
		data[12]= "UC01"; //HostLegalEntCode, length: 20
		data[44]= "10000007"; //CompetenceLineCode, length: 20, "10000007" - RM	Risk Management
		data[41]= "10000046"; //DivisionCode, length: 20, "10000046", CC_GOV	CORPORATE CENTER GOVERNANCE"
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		ResultColumn role = tableMapper.getResultColumns().get(9);
		RoleMapper rm = (RoleMapper) role.getMapper();
		MappedColumn roleCol = rm.map(srcVals, "defaultVal", sourceRow);		
		assertNull(roleCol.getMappingException());		
		assertEquals(roleCol.getValue(),USER_ROLE_TRAIN_COLLECT);
		
	}
	
	
	/**
	 * This a test for the custom configuration of user role 
	 * for user involved in "raccolta formativa 2015-2016" 
	 * 
	 */
	@Test
	public void roleMapperRaccoltaFormativa02Test(){		
		final String USER_ROLE_TRAIN_COLLECT = "UCG_USER_RF"; 		
		String[] srcVals = new String[]{"UV00074"};		
		String[] data = new String[SAP_SRC_COLS_CNT];
		// setup data
		data[0]= "3"; //UserStatusID, length: 1
		data[1]= "Active"; //UserStatusDesc, length: 80
		data[2]= "UV00074"; //UserID, length: 30
		data[49]= "Y"; //LineManager, length: 1
		data[12]= "UC01"; //HostLegalEntCode, length: 20
		data[44]= "10000025"; //CompetenceLineCode, length: 20, "10000025", ICT	GBS - ICT
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		ResultColumn role = tableMapper.getResultColumns().get(9);
		RoleMapper rm = (RoleMapper) role.getMapper();
		MappedColumn roleCol = rm.map(srcVals, "defaultVal", sourceRow);		
		assertNull(roleCol.getMappingException());		
		assertEquals(roleCol.getValue(),USER_ROLE_TRAIN_COLLECT);
		
	}

	
	/**
	 * This a test for the custom configuration of user role 
	 * for user involved in "raccolta formativa 2015-2016" 
	 * 
	 */
	@Test
	public void roleMapperRaccoltaFormativa03Test(){		
		final String USER_ROLE_TRAIN_COLLECT = "UCG_USER_RF"; 		
		String[] srcVals = new String[]{"UV00074"};		
		String[] data = new String[SAP_SRC_COLS_CNT];
		// setup data
		data[0]= "3"; //UserStatusID, length: 1
		data[1]= "Active"; //UserStatusDesc, length: 80
		data[2]= "UV00074"; //UserID, length: 30
		data[49]= "Y"; //LineManager, length: 1
		data[12]= "UC12"; //HostLegalEntCode, length: 20
		
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		ResultColumn role = tableMapper.getResultColumns().get(9);
		RoleMapper rm = (RoleMapper) role.getMapper();
		MappedColumn roleCol = rm.map(srcVals, "defaultVal", sourceRow);		
		assertNull(roleCol.getMappingException());		
		assertEquals(roleCol.getValue(),USER_ROLE_TRAIN_COLLECT);		
	}
	

	/**
	 * This a test for the custom configuration of user role 
	 * for user involved in "raccolta formativa 2015-2016" 
	 * 
	 */
	@Test
	public void roleMapperRaccoltaFormativa04Test(){		
		//final String USER_ROLE_TRAIN_COLLECT = "UCG_USER_RF"; 		
		String[] srcVals = new String[]{"UV00074"};		
		String[] data = new String[SAP_SRC_COLS_CNT];
		// setup data
		data[0]= "3"; //UserStatusID, length: 1
		data[1]= "Active"; //UserStatusDesc, length: 80
		data[2]= "UV00074"; //UserID, length: 30
		data[49]= "Y"; //LineManager, length: 1
		data[12]= "UC01"; //HostLegalEntCode, length: 20
		
		data[44]= "10000035"; //CompetenceLineCode, length: 20, "10000035", BOT	Other Business
		data[41]= "10000046"; //DivisionCode, length: 20, "10000046", CC_GOV	CORPORATE CENTER GOVERNANCE"	
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		ResultColumn role = tableMapper.getResultColumns().get(9);
		RoleMapper rm = (RoleMapper) role.getMapper();
		MappedColumn roleCol = rm.map(srcVals, "UCG_USER", sourceRow);		
		assertNull(roleCol.getMappingException());		
		assertEquals(roleCol.getValue(),"UCG_USER");		
	}
	
	/**
	 * This a test for the custom configuration of user role 
	 * for user involved in "raccolta formativa 2015-2016" 
	 * 
	 */
	@Test
	public void roleMapperRaccoltaFormativa05Test(){		
		// final String USER_ROLE_TRAIN_COLLECT = "UCG_USER_RF"; 		
		String[] srcVals = new String[]{"UV00074"};		
		String[] data = new String[SAP_SRC_COLS_CNT];
		// setup data
		data[0]= "3"; //UserStatusID, length: 1
		data[1]= "Active"; //UserStatusDesc, length: 80
		data[2]= "UV00074"; //UserID, length: 30
		data[49]= "Y"; //LineManager, length: 1
		data[12]= "UC01"; //HostLegalEntCode, length: 20
		
		data[44]= "10000035"; //CompetenceLineCode, length: 20, "10000035", BOT	Other Business
		data[41]= "10000048"; //DivisionCode, length: 20, "10000048", CC_OT	OTHER CORPORATE CENTER
		
		// user does not match this org 2nd lvl
		data[36]= "50009880"; //Org2ndLevelCode, length: 15, 50009880-UC0142012-REGION NORD OVEST 
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		ResultColumn role = tableMapper.getResultColumns().get(9);
		RoleMapper rm = (RoleMapper) role.getMapper();
		MappedColumn roleCol = rm.map(srcVals, "UCG_USER", sourceRow);		
		assertNull(roleCol.getMappingException());		
		assertEquals(roleCol.getValue(), "UCG_USER");		
	}
	
	
	/**
	 * This a test for the custom configuration of user role 
	 * for user involved in "raccolta formativa 2015-2016" 
	 * 
	 */
	@Test
	public void roleMapperRaccoltaFormativa06Test(){		
		// final String USER_ROLE_TRAIN_COLLECT = "UCG_USER_RF"; 		
		String[] srcVals = new String[]{"UV00074"};		
		String[] data = new String[SAP_SRC_COLS_CNT];
		// setup data
		data[0]= "3"; //UserStatusID, length: 1
		data[1]= "Active"; //UserStatusDesc, length: 80
		data[2]= "UV00074"; //UserID, length: 30
		data[49]= "Y"; //LineManager, length: 1
		data[12]= "UC01"; //HostLegalEntCode, length: 20
		
		data[44]= "10000034"; //CompetenceLineCode, length: 20, "10000034", MKG	Marketing		
		data[41]= "10000048"; //DivisionCode, length: 20, "10000050", RTL	RETAIL
		
		// user does not match this org 1st lvl
		data[33]= "11541035"; //Org1stLevelCode, length: 15, 11541035-UC01CALLCEN-UNICREDIT DIRECT
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		ResultColumn role = tableMapper.getResultColumns().get(9);
		RoleMapper rm = (RoleMapper) role.getMapper();
		MappedColumn roleCol = rm.map(srcVals, "UCG_USER", sourceRow);		
		assertNull(roleCol.getMappingException());		
		assertEquals(roleCol.getValue(), "UCG_USER");		
	}
	
	/**
	 * This a test for the custom configuration of user role 
	 * for user involved in "raccolta formativa 2015-2016" 
	 * 
	 */
	@Test
	public void roleMapperRaccoltaFormativa07Test(){		
		final String USER_ROLE_TRAIN_COLLECT = "UCG_USER_RF"; 		
		String[] srcVals = new String[]{"UV00074"};		
		String[] data = new String[SAP_SRC_COLS_CNT];
		// setup data
		data[0]= "3"; //UserStatusID, length: 1
		data[1]= "Active"; //UserStatusDesc, length: 80
		data[2]= "UV00074"; //UserID, length: 30
		data[49]= "Y"; //LineManager, length: 1
		data[12]= "UC01"; //HostLegalEntCode, length: 20
		
		data[44]= "10000034"; //CompetenceLineCode, length: 20, "10000034", MKG	Marketing		
		data[41]= "10000050"; //DivisionCode, length: 20, "10000050", RTL	RETAIL
		
		// user does not match this org 1st lvl
		data[33]= "Org1stLevelCode"; //Org1stLevelCode, length: 15, 11541035-UC01CALLCEN-UNICREDIT DIRECT
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		ResultColumn role = tableMapper.getResultColumns().get(9);
		RoleMapper rm = (RoleMapper) role.getMapper();
		MappedColumn roleCol = rm.map(srcVals, "UCG_USER", sourceRow);		
		assertNull(roleCol.getMappingException());		
		assertEquals(roleCol.getValue(), USER_ROLE_TRAIN_COLLECT);		
	}
	
	
	
	
	
	
	//CompetenceLineMapper
	
	
	@Test
	public void germanManagereMappersTest(){
		
		
		String[] srcVals = new String[]{"AAA","BBB","CCC","DDD","EEE"};		
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupGeManagerSourceData(data);
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		
		
		ResultColumn jobLocationID = tableMapper.getResultColumns().get(10);
		DirectMapperPlusGeMngr directMapperPlusGeMngr = (DirectMapperPlusGeMngr)jobLocationID.getMapper();
		MappedColumn jobLocIdCol = directMapperPlusGeMngr.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(jobLocIdCol.getMappingException());		
		assertEquals(jobLocIdCol.getValue(),null);
		
		
		
		//YesNoPlusGeMngrMapper
		ResultColumn hRBusinessPartnerCust80 = tableMapper.getResultColumns().get(46);
		YesNoPlusGeMngrMapper yesNoPlusGeMngrMapper = (YesNoPlusGeMngrMapper)hRBusinessPartnerCust80.getMapper();
		MappedColumn hRBusinessPartnerCust80Col = yesNoPlusGeMngrMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(hRBusinessPartnerCust80Col.getMappingException());		
		assertEquals(hRBusinessPartnerCust80Col.getValue(),null);
		

		//RoleSeniorityMapper			
		ResultColumn roleSeniorityCust150 = tableMapper.getResultColumns().get(61);
		RoleSeniorityMapper roleSeniorityMapper = (RoleSeniorityMapper)roleSeniorityCust150.getMapper();
		MappedColumn roleSeniorityCust150Col = roleSeniorityMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(roleSeniorityCust150Col.getMappingException());		
		assertEquals(roleSeniorityCust150Col.getValue(),null);


		//RoleMapper
		ResultColumn role = tableMapper.getResultColumns().get(9);
		RoleMapper roleMapper = (RoleMapper)role.getMapper();
		MappedColumn roleCol = roleMapper.map(srcVals, "UCG_USER", sourceRow);
		
		assertNull(roleCol.getMappingException());		
		assertEquals(roleCol.getValue(), "UCG_USER");
				

		//RegionIDMapper
		ResultColumn regionID = tableMapper.getResultColumns().get(25);
		RegionIDMapper regionIDMapper = (RegionIDMapper)regionID.getMapper();
		MappedColumn regionIDCol = regionIDMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(regionIDCol.getMappingException());		
		assertNull(regionIDCol.getValue());
		

		//PlateauDateMapper		
		ResultColumn hiredDate = tableMapper.getResultColumns().get(27);
		PlateauDateMapper plateauDateMapper = (PlateauDateMapper)hiredDate.getMapper();
		MappedColumn hiredDateCol = plateauDateMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(hiredDateCol.getMappingException());		
		assertNull(hiredDateCol.getValue());

		
		
		//OrgUnitLocTypeMapper
		ResultColumn orgUnitLocTypeCust240 = tableMapper.getResultColumns().get(70);
		OrgUnitLocTypeMapper orgUnitLocTypeMapper = (OrgUnitLocTypeMapper)orgUnitLocTypeCust240.getMapper();
		MappedColumn orgUnitLocTypeCust240Col = orgUnitLocTypeMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(orgUnitLocTypeCust240Col.getMappingException());		
		assertNull(orgUnitLocTypeCust240Col.getValue());

		
		
		//LocalJobPositionMapper
		ResultColumn localJobPositionCust110 = tableMapper.getResultColumns().get(57);
		LocalJobPositionMapper localJobPositionMapper = (LocalJobPositionMapper)localJobPositionCust110.getMapper();
		MappedColumn localJobPositionCust110Col = localJobPositionMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(localJobPositionCust110Col.getMappingException());		
		assertNull(localJobPositionCust110Col.getValue());


		
		//EmployeeStatusIDMapper
		ResultColumn empStatusID = tableMapper.getResultColumns().get(18);
		EmployeeStatusIDMapper employeeStatusIDMapper = (EmployeeStatusIDMapper)empStatusID.getMapper();
		MappedColumn empStatusIDCol = employeeStatusIDMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(empStatusIDCol.getMappingException());		
		assertNull(empStatusIDCol.getValue());

		

		//EmployeeStatusDescMapper
		ResultColumn empStatusDesc = tableMapper.getResultColumns().get(19);
		EmployeeStatusDescMapper employeeStatusDescMapper = (EmployeeStatusDescMapper)empStatusDesc.getMapper();
		MappedColumn empStatusDescCol = employeeStatusDescMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(empStatusDescCol.getMappingException());		
		assertNull(empStatusDescCol.getValue());

		
		//FullTimeMapper
		ResultColumn fullTime = tableMapper.getResultColumns().get(105);
		FullTimeMapper fullTimeMapper = (FullTimeMapper)fullTime.getMapper();
		MappedColumn fullTimeCol = fullTimeMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(fullTimeCol.getMappingException());		
		assertNull(fullTimeCol.getValue());
		
		
		
		
		//FullDepartmentCodeMapper
		ResultColumn fullDepartmentCodeCust250 = tableMapper.getResultColumns().get(71);
		FullDepartmentCodeMapper fullDepartmentCodeMapper = (FullDepartmentCodeMapper)fullDepartmentCodeCust250.getMapper();
		MappedColumn fullDepartmentCodeCust250Col = fullDepartmentCodeMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(fullDepartmentCodeCust250Col.getMappingException());		
		assertNull(fullDepartmentCodeCust250Col.getValue());

		
		
		// defaults to 0
		//FteMapper
		ResultColumn fteCust230 = tableMapper.getResultColumns().get(69);
		FteMapper fteMapper = (FteMapper)fteCust230.getMapper();
		MappedColumn fteCust230Col = fteMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(fteCust230Col.getMappingException());		
		assertEquals(fteCust230Col.getValue(),"0");
		
		
		
		//FixedValuePlusGeMngrMapper
		ResultColumn phoneNrDesc1 = tableMapper.getResultColumns().get(34);
		FixedValuePlusGeMngrMapper fixedValuePlusGeMngrMapper = (FixedValuePlusGeMngrMapper)phoneNrDesc1.getMapper();
		MappedColumn phoneNrDesc1Col = fixedValuePlusGeMngrMapper.map(srcVals, "Office mobile number", sourceRow);
		
		assertNull(phoneNrDesc1Col.getMappingException());		
		assertNull(phoneNrDesc1Col.getValue());

		
		//MANAGERS_DE
		//DomainIDMapper
		ResultColumn domainID = tableMapper.getResultColumns().get(12);
		DomainIDMapper domainIDMapper = (DomainIDMapper)domainID.getMapper();
		MappedColumn domainIDCol = domainIDMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(domainIDCol.getMappingException());		
		assertEquals(domainIDCol.getValue(),"MANAGERS_DE");

		
		//"Domain for German managers"
		//DomainDescMapper
		ResultColumn domainDesc = tableMapper.getResultColumns().get(13);
		DomainDescMapper domainDescMapper = (DomainDescMapper)domainDesc.getMapper();
		MappedColumn domainDescCol = domainDescMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(domainDescCol.getMappingException());		
		assertEquals(domainDescCol.getValue(),"Domain for German managers");

		
		//DivisionMapper
		ResultColumn divisionCust50 = tableMapper.getResultColumns().get(43);
		DivisionMapper divisionMapper = (DivisionMapper)divisionCust50.getMapper();
		MappedColumn divisionCust50Col = divisionMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(divisionCust50Col.getMappingException());		
		assertNull(divisionCust50Col.getValue());
	

		
		//CountryIDMapper
		ResultColumn countryID = tableMapper.getResultColumns().get(24);
		CountryIDMapper countryIDMapper = (CountryIDMapper)countryID.getMapper();
		MappedColumn countryIDCol = countryIDMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(countryIDCol.getMappingException());		
		assertNull(countryIDCol.getValue());

		
		//CompetenceLineMapper
		ResultColumn competenceLineCust60 = tableMapper.getResultColumns().get(44);
		CompetenceLineMapper competenceLineMapper = (CompetenceLineMapper)competenceLineCust60.getMapper();
		MappedColumn competenceLineCust60Col = competenceLineMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(competenceLineCust60Col.getMappingException());		
		assertNull(competenceLineCust60Col.getValue());

		
		//AgeMapper
		ResultColumn ageCust160 = tableMapper.getResultColumns().get(62);
		AgeMapper AgeMapper = (AgeMapper)ageCust160.getMapper();
		MappedColumn ageCust160Col = AgeMapper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(ageCust160Col.getMappingException());		
		assertNull(ageCust160Col.getValue());	
		
	}
	
	
	@Test
	public void competenceLineMapperTest(){
		
		ResultColumn competenceLineCol = tableMapper.getResultColumns().get(44);
		CompetenceLineMapper maper = (CompetenceLineMapper)competenceLineCol.getMapper();
		String[] srcVals = new String[]{"code","abbr","desc"};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"abbr-desc");		
	}
	
	@Test
	public void divisionMapperTest(){
		
		ResultColumn competenceLineCol = tableMapper.getResultColumns().get(43);
		DivisionMapper maper = (DivisionMapper)competenceLineCol.getMapper();
		String[] srcVals = new String[]{"code","abbr","desc"};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"abbr-desc");
		
	}
	
	@Test
	public void currencyIdMapperNonDictValueTest(){
		
		ResultColumn currencyIdCol = tableMapper.getResultColumns().get(52);
		CurrencyIDMapper maper = (CurrencyIDMapper)currencyIdCol.getMapper();
		String[] srcVals = new String[]{"HKD"};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());
		
		assertNotNull(mappedCol.getMappingException().getMessage());
		assertNull(mappedCol.getValue());		
	}
	
	@Test
	public void currencyIdMapperDictValueTest(){
		
		ResultColumn currencyIdCol = tableMapper.getResultColumns().get(52);
		CurrencyIDMapper maper = (CurrencyIDMapper)currencyIdCol.getMapper();
		String[] srcVals = new String[]{"GBP"};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"GBP");		
	}
	
	
	
	@Test
	public void domainDescMapperNullValTest(){
		
		ResultColumn domainDescCol = tableMapper.getResultColumns().get(13);
		DomainDescMapper maper = (DomainDescMapper)domainDescCol.getMapper();
		String hrbp_id = null;
		String[] srcVals = new String[]{hrbp_id,""};
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
		
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"No Learners Portfolio available");		
	}
	
	
	@Test
	public void domainIdAuditAcademyUserTest(){
		
		ResultColumn domainIDCol = tableMapper.getResultColumns().get(12);
		
		DomainIDMapper maper = (DomainIDMapper)domainIDCol.getMapper();
		
		String hrbp_id = null;
		String[] srcVals = new String[]{hrbp_id,"AA2"};
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"LP_AUDIT");		
	}
	
	@Test
	public void domainDescAuditAcademyUserTest(){
		
		ResultColumn domainDescCol = tableMapper.getResultColumns().get(13);
		
		DomainDescMapper maper = (DomainDescMapper)domainDescCol.getMapper();
		
		String hrbp_id = null;
		String[] srcVals = new String[]{hrbp_id,"AA2"};
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"Learners Portfolio of Audit");		
	}

	@Test
	public void orgIdAuditAcademyUserTest(){
		
		ResultColumn orgIdCol = tableMapper.getResultColumns().get(14);
		
		OrgIdMapper maper = (OrgIdMapper)orgIdCol.getMapper();
		
		
		String[] srcVals = new String[]{null,"AA2"};
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"AUDIT_ACADEMY_OU");		
	}
	

	@Test
	public void orgDescAuditAcademyUserTest(){
		
		ResultColumn orgDescCol = tableMapper.getResultColumns().get(15);
		
		OrgDescMapper maper = (OrgDescMapper)orgDescCol.getMapper();
		
		
		String[] srcVals = new String[]{null,null,"AA2"};
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"Default OU for Audit Academy users");		
	}
	
	
	@Test
	public void orgDescAlleatiUserTest(){
		
		ResultColumn orgDescCol = tableMapper.getResultColumns().get(15);
		
		OrgDescMapper maper = (OrgDescMapper)orgDescCol.getMapper();
		
		
		String[] srcVals = new String[]{null,null,"AA"};
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"Default OU for Anagrafe Alleati users");		
	}
	
	
	@Test
	public void orgIdAlleatiUserTest(){
		
		ResultColumn orgIdCol = tableMapper.getResultColumns().get(14);
		
		OrgIdMapper maper = (OrgIdMapper)orgIdCol.getMapper();
		
		
		String[] srcVals = new String[]{null,"AA"};
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"ANAGRAFE_ALLEATI_OU");		
	}
	
	
	
	
	

	@Test
	public void domainDescMapperNonDictHrbpTest(){
		
		ResultColumn domainDescCol = tableMapper.getResultColumns().get(13);
		DomainDescMapper maper = (DomainDescMapper)domainDescCol.getMapper();
		String hrbp_id = "UV00000";
		String[] srcVals = new String[]{hrbp_id,""};
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"Learners Portfolio of " + hrbp_id);		
	}
	
	@Test
	public void domainDescMapperDictHrbpTest(){
		
		ResultColumn domainDescCol = tableMapper.getResultColumns().get(13);
		DomainDescMapper maper = (DomainDescMapper)domainDescCol.getMapper();
		
		// has to be done because this is not done in Dictionary.afterPropertiesSet()
		maper.getDictionary().initHrbpUsersMap();
		
		String hrbp_id = "UV00007";
		String[] srcVals = new String[]{hrbp_id,""};
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"Learners Portfolio of Scott Tiger" );		
	}
	
	@Test
	public void domainIDMapperDictHrbpTest(){
		
		ResultColumn domainIdCol = tableMapper.getResultColumns().get(12);
		DomainIDMapper maper = (DomainIDMapper)domainIdCol.getMapper();
		
		// has to be done because this is not done in Dictionary.afterPropertiesSet()
		maper.getDictionary().initHrbpUsersMap();
		
		String hrbp_id = "UV00007";
		String[] srcVals = new String[]{hrbp_id,""};
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"LP_UV00007" );		
	}
	
	@Test
	public void domainIDMapperNotInDictHrbpTest(){
		
		ResultColumn domainIdCol = tableMapper.getResultColumns().get(12);
		DomainIDMapper maper = (DomainIDMapper)domainIdCol.getMapper();
		
		// has to be done because this is not done in Dictionary.afterPropertiesSet()
		maper.getDictionary().initHrbpUsersMap();
		
		String hrbp_id = "UV00008";
		String[] srcVals = new String[]{hrbp_id,""};
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"LP_UV00008" );		
	}

	@Test
	public void domainIDMapperNullHrbpTest(){
		
		ResultColumn domainIdCol = tableMapper.getResultColumns().get(12);
		DomainIDMapper maper = (DomainIDMapper)domainIdCol.getMapper();
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		
		// has to be done because this is not done in Dictionary.afterPropertiesSet()
		maper.getDictionary().initHrbpUsersMap();
		
		String hrbp_id = null;
		String[] srcVals = new String[]{hrbp_id,""};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
				
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"LP_EXCEPTIONS" );		
	}

	@Test
	public void fixedValueMappperTest(){
		ResultColumn jobTitile = tableMapper.getResultColumns().get(8);
		FixedValueMapper maper = (FixedValueMapper)jobTitile.getMapper();
		String[] srcVals = new String[]{""};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"defaultVal" );		
		
	}

	
	@Test
	public void notActiveMappperRetireeTest(){
		ResultColumn notActiveCol = tableMapper.getResultColumns().get(0);
		NotActiveMapper maper = (NotActiveMapper)notActiveCol.getMapper();
		String[] srcVals = new String[]{"2"};//retiree
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"Y" );		
	}


	@Test
	public void notActiveMappperUnknownStatusTest(){
		ResultColumn notActiveCol = tableMapper.getResultColumns().get(0);
		NotActiveMapper maper = (NotActiveMapper)notActiveCol.getMapper();
		String[] srcVals = new String[]{"777"};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());
		
		assertNotNull(mappedCol.getMappingException().getMessage());
		assertNull(mappedCol.getValue() );		
	}

	@Test
	public void plateauDateMapperTest(){
		ResultColumn hiredDateCol = tableMapper.getResultColumns().get(27);
		PlateauDateMapper maper = (PlateauDateMapper)hiredDateCol.getMapper();
		String[] srcVals = new String[]{"01.01.2009"};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(), "JAN-01-2009 12:00:00");		
	}
	
	
	@Test
	public void plateauDateMapperWrongDateTest(){
		
		ResultColumn hiredDateCol = tableMapper.getResultColumns().get(27);
		PlateauDateMapper maper = (PlateauDateMapper)hiredDateCol.getMapper();
		String[] srcVals = new String[]{"00.00.0000"};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());
				
		assertNotNull(mappedCol.getMappingException().getMessage());
		assertNull(mappedCol.getValue());		
	}
	
	

	@Test
	public void plateauDateMapperWrongDateFormatTest(){
		ResultColumn hiredDateCol = tableMapper.getResultColumns().get(27);
		PlateauDateMapper maper = (PlateauDateMapper)hiredDateCol.getMapper();
		String[] srcVals = new String[]{"2009-01-01"};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());
		
		assertNotNull(mappedCol.getMappingException().getMessage());
		assertNull(mappedCol.getValue());		
	}

	@Test
	public void regionIdMapperItalyTest(){
		ResultColumn regionIdCol = tableMapper.getResultColumns().get(25);
		RegionIDMapper maper = (RegionIDMapper)regionIdCol.getMapper();
		
		String countryID = "IT";
		String province = "RM";
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
				
		String[] srcVals = new String[]{countryID,province };
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
				
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"LA");		
	}		

	@Test
	public void regionIdMapperNonItalyTest(){
		ResultColumn regionIdCol = tableMapper.getResultColumns().get(25);
		RegionIDMapper maper = (RegionIDMapper)regionIdCol.getMapper();
		
		String countryID = "PL";
		String province = "";
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);

		String[] srcVals = new String[]{countryID,province };
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", sourceRow);
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"PL");		
	}

	
	@Test
	public void yesNoMapperTest(){
		ResultColumn lineManagerCust90 = tableMapper.getResultColumns().get(47);
		YesNoMapper maper = (YesNoMapper)lineManagerCust90.getMapper();
		String[] srcVals = new String[]{"no"};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"No");		
	}
	

	@Test
	public void yesNoMapperNegTest(){
		ResultColumn lineManagerCust90 = tableMapper.getResultColumns().get(47);
		YesNoMapper maper = (YesNoMapper)lineManagerCust90.getMapper();
		
		String[] srcVals = new String[]{"nooooo"};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());
				
		assertNotNull(mappedCol.getMappingException().getMessage());
		assertNull(mappedCol.getValue());		
	}

	
	@Test
	public void timeZonePosTest(){
		ResultColumn hrBusinessPartnerCol = tableMapper.getResultColumns().get(49);
		TimeZoneMapper maper = (TimeZoneMapper)hrBusinessPartnerCol.getMapper();
		String[] srcVals = new String[]{"PL"};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"Europe/Warsaw");		
	}
	
	@Test
	public void timeZoneNegTest(){
		ResultColumn hrBusinessPartnerCol = tableMapper.getResultColumns().get(49);
		TimeZoneMapper maper = (TimeZoneMapper)hrBusinessPartnerCol.getMapper();
		String[] srcVals = new String[]{"PLL"};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());
	
		assertNotNull(mappedCol.getMappingException().getMessage());
		assertNull(mappedCol.getValue());		
	}
	

	@Test
	public void countryIdMapperItalianOutboundTest(){
		ResultColumn countryIdCol = tableMapper.getResultColumns().get(24);
		CountryIDMapper countryIDMapper = (CountryIDMapper)countryIdCol.getMapper();
		String[] srcVals = new String[]{""};
				
		SourceRow sourceRow = new SourceRow();
		
		String data[] = new String[sourceFileConf.getSourceColumns().size()];
		
		//inbound expat
		data[sourceFileConf.getSrcExpatType().getSrcColIndex()] = "I";
				
		//italian org
		data[sourceFileConf.getSrcHomeLegalEntityCode().getSrcColIndex()] = "UC01";
		
		// the country is not set so it should be set IT
		
		sourceRow.setData(data);
		
		MappedColumn mappedCol = countryIDMapper.map(srcVals, "defaultVal", sourceRow);
	
		assertNull(mappedCol.getMappingException());
		
		assertEquals(mappedCol.getValue(),"defaultVal");		
		
	}
	
	@Test
	public void countryIdMapperItalianOutboundnNegTest(){
		ResultColumn countryIdCol = tableMapper.getResultColumns().get(24);
		CountryIDMapper countryIDMapper = (CountryIDMapper)countryIdCol.getMapper();
		String[] srcVals = new String[]{""};
				
		SourceRow sourceRow = new SourceRow();
		
		String data[] = new String[sourceFileConf.getSourceColumns().size()];
		
		//inbound expat
		data[sourceFileConf.getSrcExpatType().getSrcColIndex()] = "I";
				
		//italian org
		data[sourceFileConf.getSrcHomeLegalEntityCode().getSrcColIndex()] = "G022";
		
		// the country is not set so it should be set IT
		
		sourceRow.setData(data);
		
		MappedColumn mappedCol = countryIDMapper.map(srcVals, "defaultVal", sourceRow);
	
		assertNull(mappedCol.getMappingException());
		
		assertEquals(mappedCol.getValue(),"defaultVal");		
		
	}
	

	@Test
	public void countryIdMapperNoExpatTest(){
		ResultColumn countryIdCol = tableMapper.getResultColumns().get(24);
		CountryIDMapper countryIDMapper = (CountryIDMapper)countryIdCol.getMapper();
		String[] srcVals = new String[]{"PL"};
				
		SourceRow sourceRow = new SourceRow();
		
		String data[] = new String[sourceFileConf.getSourceColumns().size()];
		
		//inbound expat = NULL
						
		//italian org
		data[sourceFileConf.getSrcHomeLegalEntityCode().getSrcColIndex()] = "UC01";
		
		// the country should be passed directly
		
		sourceRow.setData(data);
		
		MappedColumn mappedCol = countryIDMapper.map(srcVals, "defaultVal", sourceRow);
	
		assertNull(mappedCol.getMappingException());
		
		assertEquals(mappedCol.getValue(),"PL");		
		
	}
	
		
	@Test
	public void roleSeniorityTest(){
		ResultColumn roleSeniorityCol = tableMapper.getResultColumns().get(61);
		RoleSeniorityMapper maper = (RoleSeniorityMapper)roleSeniorityCol.getMapper();
		
		DateTime now = new DateTime();
		
		now.minusMonths(5);
		
		DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM.yyyy");
				
		/* 0 days of being hired (hired today) */		
		String hiredDate = fmt.print(now);					
		String[] srcVals = new String[]{hiredDate};
		MappedColumn mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());	
				
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"0");
		
		/* below 5 months of being hired */
		DateTime threeMonthsAgo = now.minusMonths(3);
		hiredDate = fmt.print(threeMonthsAgo);					
		srcVals = new String[]{hiredDate};
		mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());	
	
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"0");		
		
		
		/* > 5 months of being hired */
		DateTime fiveMonthsOneDayAgo = (now.minusMonths(5)).minusDays(1);
		hiredDate = fmt.print(fiveMonthsOneDayAgo);					
		srcVals = new String[]{hiredDate};
		mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());	
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"1");	
		
		/* > 17 months of being hired */
		DateTime manyMonthsAgo = now.minusMonths(17);
		hiredDate = fmt.print(manyMonthsAgo);
				
		srcVals = new String[]{hiredDate};
		mappedCol = maper.map(srcVals, "defaultVal", new SourceRow());	
		
		assertNull(mappedCol.getMappingException());
		assertEquals(mappedCol.getValue(),"2");	
	}

		
	
	@Test
	public void processRowTest(){
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupSourceData(data);
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
		
		MappedRow mappedRow = tableMapper.processRow(sourceRow, "UV00000");
		String[] mappedRowData = mappedRow.getData();
		
		/*
		ArrayList<String> resultCols = new ArrayList<String>();
		for(ResultColumn rc : tableMapper.getResultColumns()){
			//System.out.println(rc.getResColName() + ", " + rc.getResColIndex());
			resultCols.add(rc.getResColName());
		}
		
		int j = 0;
		for(String mappedVal : mappedRowData){			
			//if (mappedVal == null || mappedVal.length() == 0){
			//	System.out.println("[" + j +  "]NULL \t[" + resultCols.get(j) + "]");
			//} else {
			//	System.out.println("[" + j + "]" + mappedVal + "\t[" + resultCols.get(j) + "]");
			//}	
			System.out.println("assertEquals(mappedRowData[" + j + "], \"" + mappedVal + "\"); //" +  resultCols.get(j) );			
			j++;
		}*/						
		
		assertEquals("N",mappedRowData[0]); //Not Active
		assertEquals("UV00000",mappedRowData[1]); //User ID
		assertEquals("Scott",mappedRowData[2]); //First Name
		
		assertEquals("Tiger", mappedRowData[3]); //Last Name
		assertEquals("", mappedRowData[4]); //Middle Initial
		assertEquals("M", mappedRowData[5]); //Gender
		
		
		//assertEquals("GlobalJobCode", mappedRowData[6]); //Job Position ID				
		//assertEquals("GlobalJobAbbr-GlobalJobDesc", mappedRowData[7]); //Job Position Description

		// GLOBAL JOB DATA IS NOT SENT FOR THE MOMENT
		assertEquals("", mappedRowData[6]); //Job Position ID				
		assertEquals("", mappedRowData[7]); //Job Position Description
		
		
		assertEquals("", mappedRowData[8]); //Job Title (not used)
		assertEquals("UCG_USER", mappedRowData[9]); //Role (fixed value)
		assertEquals("UserOfficeCode", mappedRowData[10]); //Job Location ID
		assertEquals("UserOfficeAbbr-UserOfficeDesc", mappedRowData[11]); //Job Location Description
		assertEquals("LP_UV00002", mappedRowData[12]); //Domain ID
		assertEquals("Learners Portfolio of UV00002", mappedRowData[13]); //Domain Description
		//assertEquals("HostLegalEntCode", mappedRowData[14]); //Organization ID
		//assertEquals("HostLegalEntDesc", mappedRowData[15]); //Organization Description
		assertEquals("UserOfficeCode", mappedRowData[14]); //Organization ID
		assertEquals("UserOfficeAbbr-UserOfficeDesc", mappedRowData[15]); //Organization Description
		
		assertEquals("X1", mappedRowData[16]); //Employee Type ID		
		assertEquals("Full-time Contract", mappedRowData[17]); //Employee Type Description
		assertEquals("FT", mappedRowData[18]); //Employee Status ID
		assertEquals("FULL TIME", mappedRowData[19]); //Employee Status Description
		assertEquals("OfficeAddr", mappedRowData[20]); //Main Address
		assertEquals("OfficeCity", mappedRowData[21]); //City
		assertEquals("OfficeProvince", mappedRowData[22]); //State
		assertEquals("OfficePostalCode", mappedRowData[23]); //Postal Code
		assertEquals("PL", mappedRowData[24]); //Country
		assertEquals("PL", mappedRowData[25]); //Region
		assertEquals("SCOTT.TIGER@UNICREDITGROUP.EU", mappedRowData[26]); //Email Address
		assertEquals("DEC-01-1999 12:00:00", mappedRowData[27]); //Hired Date
		assertEquals("", mappedRowData[28]); //Terminated Date
		assertEquals("UV00001", mappedRowData[29]); //Supervisor
		assertEquals("UV00000", mappedRowData[30]); //Resume
		assertEquals("", mappedRowData[31]); //Comments (not used)
		//assertEquals("CostCenterCode", mappedRowData[32]); //Account Code
		// the value is not passed to the Plateau 
		assertEquals("", mappedRowData[32]); //Account Code		
		assertEquals("OfficeMobilePhone", mappedRowData[33]); //Phone Number 1
		assertEquals("Office mobile number", mappedRowData[34]); //Phone Number Description 1
		assertEquals("OfficeFixPhone", mappedRowData[35]); //Phone Number 2
		assertEquals("Office fix number", mappedRowData[36]); //Phone Number Description 2
		assertEquals("", mappedRowData[37]); //Phone Number 3 (not used)
		assertEquals("", mappedRowData[38]); //Phone Number Description 3 (not used)
		assertEquals("FiscalCode", mappedRowData[39]); //Custom Column 10 Value
		assertEquals("Org1stLevelCode-Org1stLevelAbbr-Org1stLevelDesc", mappedRowData[40]); //Custom Column 20 Value
		assertEquals("Org2ndLevelCode-Org2ndLevelAbbr-Org2ndLevelDesc", mappedRowData[41]); //Custom Column 30 Value
		assertEquals("HomeLegalEntityCode-HomeLegalEntityDesc", mappedRowData[42]); //Custom Column 40 Value
		assertEquals("DivisionAbbr-DivisionDesc", mappedRowData[43]); //Custom Column 50 Value
		assertEquals("CompetenceLineAbbr-CompetenceLineDesc", mappedRowData[44]); //Custom Column 60 Value
		assertEquals("UV00002", mappedRowData[45]); //Custom Column 70 Value
		assertEquals("No", mappedRowData[46]); //Custom Column 80 Value
		assertEquals("No", mappedRowData[47]); //Custom Column 90 Value
		
		// GLOBAL JOB DATA IS NOT SENT FOR THE MOMENT
		//assertEquals("GlobalJobBandCode-GlobalJobBandAbbr-GlobalJobBandDesc", mappedRowData[48]); //Custom Column 100 Value
		assertEquals("", mappedRowData[48]); //Custom Column 100 Value
		
		assertEquals("Europe/Warsaw", mappedRowData[49]); //Time Zone
		assertEquals("English (UK)", mappedRowData[50]); //Locale
		assertEquals("", mappedRowData[51]); //User May Use Org Acct Code (not used)
		assertEquals("PLN", mappedRowData[52]); //Currency ID		
		//assertEquals("CostCenterDesc", mappedRowData[53]); //Account Code Description
		// the value is not passed to the Plateau 
		assertEquals("", mappedRowData[53]); //Account Code Description
		assertEquals("", mappedRowData[54]); //JobPositionChangeEffectiveDate (not used)
		assertEquals("", mappedRowData[55]); //Admin ID Mapping (not used)
		assertEquals("", mappedRowData[56]); //Related Instructor (not used)
		assertEquals("JobRoleCode-JobRoleAbbr-JobRoleDesc", mappedRowData[57]); //localJobPositionCust110
		assertEquals("PayGradeCode-PayGradeDesc", mappedRowData[58]); //joblevelCust120	payGradeCode
		assertEquals("PrevJobRoleCode-PrevJobRoleAbbr-PrevJobRoleDesc", mappedRowData[59]); //previousJobPositionCust130	prevJobRoleCode
		assertEquals("SecondPrevJobRoleCode-SecondPrevJobRoleAbbr-SecondPrevJobRoleDesc", mappedRowData[60]); //secondPreviousJobPositionCust140	secondPrevJobRoleCode
			
		assertTrue(Integer.valueOf(mappedRowData[61]) >= 3); //jobStartingDateCust150	jobRoleStartDate (role seniority in fact)
		
		assertEquals("39", mappedRowData[62]); //dateOfBirthCust160	dateOfBirth
		assertEquals("PL-Poland", mappedRowData[63]); //nationalityCust170	nationID
		assertEquals("No", mappedRowData[64]); //expatriateCust180	expatriate
		assertEquals("02-University", mappedRowData[65]);//educationTitleCust190	eduTitleID
		assertEquals("JobSeniorityCode", mappedRowData[66]); //jobSeniorityCust200	srcJobSeniorityCode
		assertEquals("ServiceLineCode", mappedRowData[67]); //serviceLineCust210	srcServiceLineCode
		assertEquals("BusinessLineCode", mappedRowData[68]); //businessLineCust220	srcBusinessLineCode
		assertEquals("75.0", mappedRowData[69]); //fteCust230	
		assertEquals("L", mappedRowData[70]); //orgUnitLocTypeCust240	
		assertEquals("", mappedRowData[71]); //cust250	
		assertEquals("OrgDstCd-OrgDistrictName-OrgDistrictDesc", mappedRowData[72]); //cust260	
		assertEquals("HostLegalEntCode", mappedRowData[73]); //cust270	
		assertEquals("ASSIGNMENT MODE VALUE", mappedRowData[74]); //cust280	
		assertEquals("", mappedRowData[75]); //cust290	
		assertEquals("", mappedRowData[76]); //custDesc10	
		assertEquals("", mappedRowData[77]); //custDesc20
		assertEquals("", mappedRowData[78]); //custDesc30
		assertEquals("", mappedRowData[79]); //custDesc40	
		assertEquals("", mappedRowData[80]); //custDesc50	
		assertEquals("", mappedRowData[81]); //custDesc60	
		assertEquals("", mappedRowData[82]); //custDesc70		
		assertEquals("", mappedRowData[83]); //custDesc80		
		assertEquals("", mappedRowData[84]); //custDesc90		
		assertEquals("", mappedRowData[85]); //custDesc100
		assertEquals("", mappedRowData[86]); //custDesc110
		assertEquals("", mappedRowData[87]); //custDesc120		
		assertEquals("", mappedRowData[88]); //custDesc120
		assertEquals("", mappedRowData[89]); //custDesc120
		assertEquals("", mappedRowData[90]); //custDesc150		
		assertEquals("", mappedRowData[91]); //custDesc160		
		assertEquals("", mappedRowData[92]); //custDesc170	
		assertEquals("", mappedRowData[93]); //custDesc180		
		assertEquals("", mappedRowData[94]); //custDesc190	
		assertEquals("JobSeniorityDesc", mappedRowData[95]); //jobSeniorityCustDesc200	srcJobSeniorityDesc	
		assertEquals("ServiceLineDesc", mappedRowData[96]); //serviceLineCustDesc210	srcServiceLineDesc	
		assertEquals("BusinessLineDesc", mappedRowData[97]); //businessLineCustDesc220	srcBusinessLineDesc	
		assertEquals("", mappedRowData[98]); //custDesc230		
		assertEquals("Local", mappedRowData[99]); //custDesc240		
		assertEquals("", mappedRowData[100]); //custDesc250		
		assertEquals("", mappedRowData[101]); //custDesc260		
		assertEquals("HostLegalEntDesc", mappedRowData[102]); //custDesc270		
		assertEquals("", mappedRowData[103]); //custDesc280		
		assertEquals("", mappedRowData[104]); //custDesc290		
		assertEquals("Y", mappedRowData[105]); //FullTime
	}
	
	
	
	/**
	 * Print out the configuration
	 * 
	 */
	@Test 
	public void outputConfig(){

		String msg;
		String src;
		for (ResultColumn rc: tableMapper.getResultColumns()){
			
			msg = rc.getResColIndex() + " - " + rc.getResColName() + " - " + rc.getResColDesc();
			
			src = "";
			if ( rc.getSrcCol()!= null ){
				for (SourceColumn sc : rc.getSrcCol()){
					
					src = src + " " + sc.getSrcColName();
								
				}
			}
			
			msg = msg + ": " +src;
			 
			
			System.out.println(msg);
			
			
		
			
		}
		
		 
		
		
	}
	
	
	@Test
	public void processGeMngrRowTest(){
		
		
		
		String[] data = new String[SAP_SRC_COLS_CNT];
		setupGeManagerSourceData(data);
		
			
		
		SourceRow sourceRow = new SourceRow();
		sourceRow.setData(data);
				
		
		MappedRow mappedRow = tableMapper.processRow(sourceRow, "UV00001");
		String[] mappedRowData = mappedRow.getData();
		
		/*
		ArrayList<String> resultCols = new ArrayList<String>();
		for(ResultColumn rc : tableMapper.getResultColumns()){
			//System.out.println(rc.getResColName() + ", " + rc.getResColIndex());
			resultCols.add(rc.getResColName());
		}
		
		int j = 0;
		for(String mappedVal : mappedRowData){			
			//if (mappedVal == null || mappedVal.length() == 0){
			//	System.out.println("[" + j +  "]NULL \t[" + resultCols.get(j) + "]");
			//} else {
			//	System.out.println("[" + j + "]" + mappedVal + "\t[" + resultCols.get(j) + "]");
			//}	
			System.out.println("assertEquals(mappedRowData[" + j + "], \"" + mappedVal + "\"); //" +  resultCols.get(j) );			
			j++;
		}*/						
		
		assertEquals("N",mappedRowData[0]); //Not Active
		assertEquals("UV00001",mappedRowData[1]); //GERMAN User ID
		assertEquals("Scott",mappedRowData[2]); //First Name
		
		assertEquals("Tiger", mappedRowData[3]); //Last Name
		assertEquals("", mappedRowData[4]); //Middle Initial
		assertEquals("M", mappedRowData[5]); //Gender
		
		
		//assertEquals("GlobalJobCode", mappedRowData[6]); //Job Position ID				
		//assertEquals("GlobalJobAbbr-GlobalJobDesc", mappedRowData[7]); //Job Position Description

		// GLOBAL JOB DATA IS NOT SENT FOR THE MOMENT
		assertEquals("", mappedRowData[6]); //Job Position ID				
		assertEquals("", mappedRowData[7]); //Job Position Description
		
		
		assertEquals("", mappedRowData[8]); //Job Title (not used)
		assertEquals("UCG_USER", mappedRowData[9]); //Role (fixed value)
		assertEquals("", mappedRowData[10]); //Job Location ID
		assertEquals("", mappedRowData[11]); //Job Location Description
		
		 
		
		assertEquals("MANAGERS_DE", mappedRowData[12]); //Domain ID
		assertEquals("Domain for German managers", mappedRowData[13]); //Domain Description
		//assertEquals("HostLegalEntCode", mappedRowData[14]); //Organization ID
		//assertEquals("HostLegalEntDesc", mappedRowData[15]); //Organization Description
		assertEquals("UserOfficeCode", mappedRowData[14]); //Organization ID
		assertEquals("UserOfficeAbbr-UserOfficeDesc", mappedRowData[15]); //Organization Description
		
		assertEquals("", mappedRowData[16]); //Employee Type ID		
		assertEquals("", mappedRowData[17]); //Employee Type Description
		assertEquals("", mappedRowData[18]); //Employee Status ID
		assertEquals("", mappedRowData[19]); //Employee Status Description
		assertEquals("", mappedRowData[20]); //Main Address
		assertEquals("", mappedRowData[21]); //City
		assertEquals("", mappedRowData[22]); //State
		assertEquals("", mappedRowData[23]); //Postal Code
		assertEquals("", mappedRowData[24]); //Country
		assertEquals("", mappedRowData[25]); //Region
		assertEquals("SCOTT.TIGER@UNICREDITGROUP.EU", mappedRowData[26]); //Email Address
		assertEquals("", mappedRowData[27]); //Hired Date
		assertEquals("", mappedRowData[28]); //Terminated Date
		assertEquals("UV00001", mappedRowData[29]); //Supervisor
		assertEquals("", mappedRowData[30]); //Resume
		assertEquals("", mappedRowData[31]); //Comments (not used)
		//assertEquals("CostCenterCode", mappedRowData[32]); //Account Code
		// the value is not passed to the Plateau 
		assertEquals("", mappedRowData[32]); //Account Code		
		assertEquals("", mappedRowData[33]); //Phone Number 1
		assertEquals("", mappedRowData[34]); //Phone Number Description 1
		assertEquals("", mappedRowData[35]); //Phone Number 2
		assertEquals("", mappedRowData[36]); //Phone Number Description 2
		assertEquals("", mappedRowData[37]); //Phone Number 3 (not used)
		assertEquals("", mappedRowData[38]); //Phone Number Description 3 (not used)
		assertEquals("", mappedRowData[39]); //Custom Column 10 Value
		assertEquals("", mappedRowData[40]); //Custom Column 20 Value
		assertEquals("", mappedRowData[41]); //Custom Column 30 Value
		assertEquals("", mappedRowData[42]); //Custom Column 40 Value
		assertEquals("", mappedRowData[43]); //Custom Column 50 Value
		assertEquals("", mappedRowData[44]); //Custom Column 60 Value
		assertEquals("", mappedRowData[45]); //Custom Column 70 Value
		assertEquals("", mappedRowData[46]); //Custom Column 80 Value
		assertEquals("No", mappedRowData[47]); //Custom Column 90 Value
		
		// GLOBAL JOB DATA IS NOT SENT FOR THE MOMENT
		//assertEquals("GlobalJobBandCode-GlobalJobBandAbbr-GlobalJobBandDesc", mappedRowData[48]); //Custom Column 100 Value
		assertEquals("", mappedRowData[48]); //Custom Column 100 Value
		
		assertEquals("Europe/Warsaw", mappedRowData[49]); //Time Zone
		assertEquals("English (UK)", mappedRowData[50]); //Locale
		assertEquals("", mappedRowData[51]); //User May Use Org Acct Code (not used)
		assertEquals("PLN", mappedRowData[52]); //Currency ID		
		//assertEquals("CostCenterDesc", mappedRowData[53]); //Account Code Description
		// the value is not passed to the Plateau 
		assertEquals("", mappedRowData[53]); //Account Code Description
		assertEquals("", mappedRowData[54]); //JobPositionChangeEffectiveDate (not used)
		assertEquals("", mappedRowData[55]); //Admin ID Mapping (not used)
		assertEquals("", mappedRowData[56]); //Related Instructor (not used)
		assertEquals("", mappedRowData[57]); //localJobPositionCust110
		assertEquals("", mappedRowData[58]); //joblevelCust120	payGradeCode
		assertEquals("", mappedRowData[59]); //previousJobPositionCust130	prevJobRoleCode
		assertEquals("", mappedRowData[60]); //secondPreviousJobPositionCust140	secondPrevJobRoleCode
			
		assertEquals("", mappedRowData[61]); //jobStartingDateCust150	jobRoleStartDate (role seniority in fact)
		
		assertEquals("", mappedRowData[62]); //dateOfBirthCust160	dateOfBirth
		assertEquals("", mappedRowData[63]); //nationalityCust170	nationID
		assertEquals("", mappedRowData[64]); //expatriateCust180	expatriate
		assertEquals("", mappedRowData[65]);//educationTitleCust190	eduTitleID
		assertEquals("", mappedRowData[66]); //jobSeniorityCust200	srcJobSeniorityCode
		assertEquals("", mappedRowData[67]); //serviceLineCust210	srcServiceLineCode
		assertEquals("", mappedRowData[68]); //businessLineCust220	srcBusinessLineCode
		assertEquals("0", mappedRowData[69]); //fteCust230	
		assertEquals("", mappedRowData[70]); //orgUnitLocTypeCust240	
		assertEquals("", mappedRowData[71]); //cust250	
		assertEquals("", mappedRowData[72]); //cust260	
		assertEquals("", mappedRowData[73]); //cust270	
		assertEquals("", mappedRowData[74]); //cust280	
		assertEquals("", mappedRowData[75]); //cust290	
		assertEquals("", mappedRowData[76]); //custDesc10	
		assertEquals("", mappedRowData[77]); //custDesc20
		assertEquals("", mappedRowData[78]); //custDesc30
		assertEquals("", mappedRowData[79]); //custDesc40	
		assertEquals("", mappedRowData[80]); //custDesc50	
		assertEquals("", mappedRowData[81]); //custDesc60	
		assertEquals("", mappedRowData[82]); //custDesc70		
		assertEquals("", mappedRowData[83]); //custDesc80		
		assertEquals("", mappedRowData[84]); //custDesc90		
		assertEquals("", mappedRowData[85]); //custDesc100
		assertEquals("", mappedRowData[86]); //custDesc110
		assertEquals("", mappedRowData[87]); //custDesc120		
		assertEquals("", mappedRowData[88]); //custDesc120
		assertEquals("", mappedRowData[89]); //custDesc120
		assertEquals("", mappedRowData[90]); //custDesc150		
		assertEquals("", mappedRowData[91]); //custDesc160		
		assertEquals("", mappedRowData[92]); //custDesc170	
		assertEquals("", mappedRowData[93]); //custDesc180		
		assertEquals("", mappedRowData[94]); //custDesc190	
		assertEquals("JobSeniorityDesc", mappedRowData[95]); //jobSeniorityCustDesc200	srcJobSeniorityDesc	
		assertEquals("ServiceLineDesc", mappedRowData[96]); //serviceLineCustDesc210	srcServiceLineDesc	
		assertEquals("BusinessLineDesc", mappedRowData[97]); //businessLineCustDesc220	srcBusinessLineDesc	
		assertEquals("", mappedRowData[98]); //custDesc230		
		assertEquals("Local", mappedRowData[99]); //custDesc240		
		assertEquals("", mappedRowData[100]); //custDesc250		
		assertEquals("", mappedRowData[101]); //custDesc260		
		assertEquals("HostLegalEntDesc", mappedRowData[102]); //custDesc270		
		assertEquals("", mappedRowData[103]); //custDesc280		
		assertEquals("", mappedRowData[104]); //custDesc290		
		assertEquals("", mappedRowData[105]); //FullTime
	}

	
}
