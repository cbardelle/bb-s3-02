package eu.unicreditgroup.test.processor;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.MappedRow;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.config.ResultColumn;
import eu.unicreditgroup.config.SourceColumn;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.mapper.AgeMapper;
import eu.unicreditgroup.mapper.CompetenceLineMapper;
import eu.unicreditgroup.mapper.CountryIDMapper;
import eu.unicreditgroup.mapper.CurrencyIDMapper;
import eu.unicreditgroup.mapper.DirectMapper;
import eu.unicreditgroup.mapper.DivisionMapper;
import eu.unicreditgroup.mapper.DomainDescMapper;
import eu.unicreditgroup.mapper.DomainIDMapper;
import eu.unicreditgroup.mapper.EmployeeStatusDescMapper;
import eu.unicreditgroup.mapper.FixedValueMapper;
import eu.unicreditgroup.mapper.IMapper;
import eu.unicreditgroup.mapper.LocaleIDMapper;
import eu.unicreditgroup.mapper.NotActiveMapper;
import eu.unicreditgroup.mapper.PlateauDateMapper;
import eu.unicreditgroup.mapper.RoleSeniorityMapper;
import eu.unicreditgroup.mapper.TimeZoneMapper;
import eu.unicreditgroup.mapper.YesNoMapper;
import eu.unicreditgroup.mapper.RegionIDMapper;
import eu.unicreditgroup.mapper.TableMapper;
import eu.unicreditgroup.processor.OrgLstFileProcessor;
import eu.unicreditgroup.test.BaseSpringTest;


public class OrgLstFileProcessorTest extends BaseSpringTest{
	
	//@Resource(name = "reportDataBean")  
	//private ReportDataBean reportDataBean;  
	
	@Resource(name = "tableMapper")
	private TableMapper tableMapper;
	
	@Resource(name = "sourceColumns")
	private List<SourceColumn> sourceColumns;
	
	
	@Resource(name = "sourceFileConf")
	private SourceFileConf sourceFileConf;
	
	
	@Resource(name = "configParams")
	private ConfigParams configParams;
	
	@Resource(name = "orgLstFileProcessor")
	private OrgLstFileProcessor orgLstFileProcessor;
	
	
	//@Test 
	public void outputConfig(){
		
		System.out.println(">>>> " + configParams.getStrSourceLocalDir());
		System.out.println(">>>> " + configParams.getStrOrgLstFileName());
		
		orgLstFileProcessor.processOrgLstFile();
		
			
	}
	
}
