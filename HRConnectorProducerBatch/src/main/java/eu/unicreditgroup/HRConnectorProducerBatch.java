//GIT01
package eu.unicreditgroup;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import eu.unicreditgroup.batch.BatchWorker;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.mailer.FtlReportGenerator;
import eu.unicreditgroup.mailer.ReportDataBean;

//import eu.unicreditgroup.batch.BatchWorker;
//import eu.unicreditgroup.mailer.FtlReportGenerator;
//import eu.unicreditgroup.mailer.ReportDataBean;

public class HRConnectorProducerBatch {

	private static String logFileName;
	
	private static String pathToLogFile = "logs/";	
	
	/**
	 * Performs log4j initialization and configuration.
	 * 
	 */
	public static void log4jInit() {		
		SimpleDateFormat sdf =	new SimpleDateFormat("'LOG_'dd-MM-yyyy__HH_mm_ss'.txt'");
		logFileName = sdf.format(new Date());
		System.setProperty("log_file_name", pathToLogFile + logFileName);
		PropertyConfigurator.configure("conf/log4j.properties");
	}		
	
	/**
	 * Initializes application context and performs all work.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		log4jInit();
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext(new String[] {"META-INF/spring/spring.xml"});
		
		BatchWorker batchWorker = appContext.getBean(BatchWorker.class);
		batchWorker.execute();
		
		ReportDataBean reportBean = appContext.getBean(ReportDataBean.class);
		FtlReportGenerator reportGenerator = new FtlReportGenerator(pathToLogFile, logFileName);
		reportGenerator.generateAndSendReport(reportBean);
		
		// send simplified version of the report 
		// basic info + changes of selected user's attributes		
		reportGenerator.generateAndSendTrackedAttrReport(reportBean);
		
		
		// After migration to the cloud version the new organization 
		// are not insert into the PA_ORG table so there's a need 
		// to manually update the PA_ORG table
		reportGenerator.generateAndSendNewOrgAlert(reportBean);
		
		
		
	}
	
}
