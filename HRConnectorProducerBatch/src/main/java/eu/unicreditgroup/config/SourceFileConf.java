package eu.unicreditgroup.config;

import java.util.List;

import eu.unicreditgroup.beans.SourceRow;

/**
 * Configuration of the source file
 * (spring configured)
 * 
 * @author UV00074
 *
 */
public class SourceFileConf {
	
	public final String EXPAT_TYPE_INBOUND = "I";	
	
	public final String EXPAT_TYPE_OUTBOUND = "O";
	
	public final String EXPATRIATE_YES = "Y";
	
	
	// external users (from the source_aaext.csv file) have some hard-coded values
	public final String EXTERNAL_USER_OFFICE_COUNTRY = "AA";
		
	public final String EXTERNAL_USER_HOST_LEGAL_ENTITY_CODE = "AA"; 
	
	public final String EXTERNAL_USER_HOME_LEGAL_ENTITY_CODE = "AA";
	
	public final String USER_STATUS_ID_WITHDRAWN = "0";
	
	public final String USER_STATUS_ID_WITHDRAWN_DESC = "Withdrawn";
	
	private final String LINE_MANAGER_YES = "Y";
	
	List<SourceColumn> sourceColumns;
	
	// list of source column which will be used for processing
	private SourceColumn srcUserStatusID;
	private SourceColumn srcUserStatusDesc;
	private SourceColumn srcUserID;	
	private SourceColumn srcFirstName;
	private SourceColumn srcLastName;	
	private SourceColumn srcHostLegalEntCode;
	private SourceColumn srcHostLegalEntDesc;		
	private SourceColumn srcOfficeCountry;
	private SourceColumn srcHomeLegalEntityCode;
	private SourceColumn srcHomeLegalEntityDesc;
	private SourceColumn srcCompetenceLineCode;	
	private SourceColumn srcHRBPID;
	private SourceColumn srcHRBP;	
	private SourceColumn srcLineManager;
	private SourceColumn srcNationID;
	private SourceColumn srcExpatriate;	
	private SourceColumn srcLocalLang;				
	private SourceColumn srcLocalLangDesc;		
	private SourceColumn srcExpatType;
	private SourceColumn srcResume;
	private SourceColumn srcOrgUnitLocType;
	
	private SourceColumn srcJobRoleCode;
	private SourceColumn srcJobRoleAbbr;
	private SourceColumn srcJobRoleDesc;
	
	
	private SourceColumn srcDivisionCode;
	private SourceColumn srcOrg1stLevelCode;
	private SourceColumn srcOrg2ndLevelCode;
	
	
	public boolean isLineManager( SourceRow srcRow ){		
		String[] row = srcRow.getData();
		
		if ( LINE_MANAGER_YES.equalsIgnoreCase( row[srcLineManager.getSrcColIndex() ] ) ){
			return true;
		} else {		
			return false;
		}
		
	}
	
	
	public boolean isExternalUser( SourceRow srcRow ){
		
		String[] row = srcRow.getData();
		
		if ( EXTERNAL_USER_OFFICE_COUNTRY.equals( row[srcOfficeCountry.getSrcColIndex() ] ) &&
			EXTERNAL_USER_HOST_LEGAL_ENTITY_CODE.equals( row[srcHostLegalEntCode.getSrcColIndex() ] ) &&
			EXTERNAL_USER_HOME_LEGAL_ENTITY_CODE.equals( row[srcHomeLegalEntityCode.getSrcColIndex() ] ) 
			)
		{
			return true;
		} else {
			return false;
		}		
	}
	
		
		
	public String getSrcExpatTypeVal(SourceRow sourceRow){		
		return sourceRow.getValue( srcExpatType.getSrcColIndex() );		
	}
	

	public String getSrcExpatatriateVal(SourceRow sourceRow){		
		return sourceRow.getValue( srcExpatriate.getSrcColIndex() );		
	}

	
	public String getSrcHostLegalEntCode(SourceRow sourceRow){
		return sourceRow.getValue( srcHostLegalEntCode.getSrcColIndex() );		
	}
	
	public String getSrcCompetenceLineCode(SourceRow sourceRow){		
		return sourceRow.getValue( srcCompetenceLineCode.getSrcColIndex() );		
	}
	public String getSrcDivisionCode(SourceRow sourceRow){		
		return sourceRow.getValue( srcDivisionCode.getSrcColIndex() );		
	}	
	public String getSrcOrg1stLevelCode(SourceRow sourceRow){
		return sourceRow.getValue( srcOrg1stLevelCode.getSrcColIndex() );		
	}
		
	public String getSrcOrg2ndLevelCode(SourceRow sourceRow){
		return sourceRow.getValue( srcOrg2ndLevelCode.getSrcColIndex() );
	}
	
		
	public List<SourceColumn> getSourceColumns() {
		return sourceColumns;
	}
	public void setSourceColumns(List<SourceColumn> sourceColumns) {
		this.sourceColumns = sourceColumns;
	}
	public SourceColumn getSrcUserStatusID() {
		return srcUserStatusID;
	}
	public void setSrcUserStatusID(SourceColumn srcUserStatusID) {
		this.srcUserStatusID = srcUserStatusID;
	}
	public SourceColumn getSrcUserID() {
		return srcUserID;
	}
	public void setSrcUserID(SourceColumn srcUserID) {
		this.srcUserID = srcUserID;
	}
	public SourceColumn getSrcFirstName() {
		return srcFirstName;
	}
	public void setSrcFirstName(SourceColumn srcFirstName) {
		this.srcFirstName = srcFirstName;
	}
	public SourceColumn getSrcLastName() {
		return srcLastName;
	}
	public void setSrcLastName(SourceColumn srcLastName) {
		this.srcLastName = srcLastName;
	}
	public SourceColumn getSrcHostLegalEntCode() {
		return srcHostLegalEntCode;
	}
	public void setSrcHostLegalEntCode(SourceColumn srcHostLegalEntCode) {
		this.srcHostLegalEntCode = srcHostLegalEntCode;
	}
	public SourceColumn getSrcHostLegalEntDesc() {
		return srcHostLegalEntDesc;
	}
	public void setSrcHostLegalEntDesc(SourceColumn srcHostLegalEntDesc) {
		this.srcHostLegalEntDesc = srcHostLegalEntDesc;
	}
	public SourceColumn getSrcOfficeCountry() {
		return srcOfficeCountry;
	}
	public void setSrcOfficeCountry(SourceColumn srcOfficeCountry) {
		this.srcOfficeCountry = srcOfficeCountry;
	}
	
	
	
	public SourceColumn getSrcHomeLegalEntityCode() {
		return srcHomeLegalEntityCode;
	}


	public void setSrcHomeLegalEntityCode(SourceColumn srcHomeLegalEntityCode) {
		this.srcHomeLegalEntityCode = srcHomeLegalEntityCode;
	}


	public SourceColumn getSrcHomeLegalEntityDesc() {
		return srcHomeLegalEntityDesc;
	}


	public void setSrcHomeLegalEntityDesc(SourceColumn srcHomeLegalEntityDesc) {
		this.srcHomeLegalEntityDesc = srcHomeLegalEntityDesc;
	}


	public SourceColumn getSrcCompetenceLineCode() {
		return srcCompetenceLineCode;
	}
	public void setSrcCompetenceLineCode(SourceColumn srcCompetenceLineCode) {
		this.srcCompetenceLineCode = srcCompetenceLineCode;
	}
	public SourceColumn getSrcHRBPID() {
		return srcHRBPID;
	}
	public void setSrcHRBPID(SourceColumn srcHRBPID) {
		this.srcHRBPID = srcHRBPID;
	}
	public SourceColumn getSrcHRBP() {
		return srcHRBP;
	}
	public void setSrcHRBP(SourceColumn srcHRBP) {
		this.srcHRBP = srcHRBP;
	}
	public SourceColumn getSrcLineManager() {
		return srcLineManager;
	}
	public void setSrcLineManager(SourceColumn srcLineManager) {
		this.srcLineManager = srcLineManager;
	}
	public SourceColumn getSrcNationID() {
		return srcNationID;
	}
	public void setSrcNationID(SourceColumn srcNationID) {
		this.srcNationID = srcNationID;
	}
	public SourceColumn getSrcExpatriate() {
		return srcExpatriate;
	}
	public void setSrcExpatriate(SourceColumn srcExpatriate) {
		this.srcExpatriate = srcExpatriate;
	}
	public SourceColumn getSrcLocalLang() {
		return srcLocalLang;
	}
	public void setSrcLocalLang(SourceColumn srcLocalLang) {
		this.srcLocalLang = srcLocalLang;
	}
	public SourceColumn getSrcLocalLangDesc() {
		return srcLocalLangDesc;
	}
	public void setSrcLocalLangDesc(SourceColumn srcLocalLangDesc) {
		this.srcLocalLangDesc = srcLocalLangDesc;
	}
	public SourceColumn getSrcExpatType() {
		return srcExpatType;
	}
	public void setSrcExpatType(SourceColumn srcExpatType) {
		this.srcExpatType = srcExpatType;
	}
	public SourceColumn getSrcResume() {
		return srcResume;
	}
	public void setSrcResume(SourceColumn srcResume) {
		this.srcResume = srcResume;
	}

	public SourceColumn getSrcOrgUnitLocType() {
		return srcOrgUnitLocType;
	}

	public void setSrcOrgUnitLocType(SourceColumn srcOrgUnitLocType) {
		this.srcOrgUnitLocType = srcOrgUnitLocType;
	}

	public SourceColumn getSrcJobRoleCode() {
		return srcJobRoleCode;
	}

	public void setSrcJobRoleCode(SourceColumn srcJobRoleCode) {
		this.srcJobRoleCode = srcJobRoleCode;
	}

	public SourceColumn getSrcJobRoleAbbr() {
		return srcJobRoleAbbr;
	}

	public void setSrcJobRoleAbbr(SourceColumn srcJobRoleAbbr) {
		this.srcJobRoleAbbr = srcJobRoleAbbr;
	}

	public SourceColumn getSrcJobRoleDesc() {
		return srcJobRoleDesc;
	}

	public void setSrcJobRoleDesc(SourceColumn srcJobRoleDesc) {
		this.srcJobRoleDesc = srcJobRoleDesc;
	}

	public SourceColumn getSrcUserStatusDesc() {
		return srcUserStatusDesc;
	}

	public void setSrcUserStatusDesc(SourceColumn srcUserStatusDesc) {
		this.srcUserStatusDesc = srcUserStatusDesc;
	}


	public SourceColumn getSrcDivisionCode() {
		return srcDivisionCode;
	}


	public void setSrcDivisionCode(SourceColumn srcDivisionCode) {
		this.srcDivisionCode = srcDivisionCode;
	}


	public SourceColumn getSrcOrg1stLevelCode() {
		return srcOrg1stLevelCode;
	}


	public void setSrcOrg1stLevelCode(SourceColumn srcOrg1stLevelCode) {
		this.srcOrg1stLevelCode = srcOrg1stLevelCode;
	}


	public SourceColumn getSrcOrg2ndLevelCode() {
		return srcOrg2ndLevelCode;
	}


	public void setSrcOrg2ndLevelCode(SourceColumn srcOrg2ndLevelCode) {
		this.srcOrg2ndLevelCode = srcOrg2ndLevelCode;
	}
	
	

}
