package eu.unicreditgroup.filter;

import eu.unicreditgroup.beans.SourceRow;

public interface AdditionalProcessor {
	
	/**
	 * Do additional processing for SAP source data
	 * ( e.g. put source row to an additional table )
	 * @param sourceRow
	 * @return
	 */
	public void process(SourceRow sourceRow);
	
	
	/**
	 * Flush data buffers etc
	 * 
	 */
	public void finishProcessing();
		
	
}
