package eu.unicreditgroup.filter;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;



/**
 * This filter is responsible for filtering out
 * all outbound expat user
 * 
 * @author UV00074
 *
 */
public class OutboundExpatFilter implements Filter {
	
	private SourceFileConf sourceFileConf;
	

	@Override
	public boolean accept(SourceRow sourceRow) {
		
		String expatType = sourceFileConf.getSrcExpatTypeVal(sourceRow);
		
		String expatriate = sourceFileConf.getSrcExpatatriateVal(sourceRow);
	
		// accept only if not outbound expat
		
		
		// 2013-01-18
		// TODO - processing of expat users is still suspended
		// For the moment Inboud and Outbound expat users won't be passed to Plateau 
		
		if (sourceFileConf.EXPAT_TYPE_OUTBOUND.equalsIgnoreCase(expatType) == true || 
				sourceFileConf.EXPAT_TYPE_INBOUND.equalsIgnoreCase(expatType) == true || 
				sourceFileConf.EXPATRIATE_YES.equalsIgnoreCase(expatriate) == true ){
			return false;
		}else {
			return true;
		}

	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}

}
