package eu.unicreditgroup.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import org.apache.log4j.Logger;

import eu.unicreditgroup.config.ResultColumn;
import eu.unicreditgroup.mapper.SupervisorIdMapper;

public class ResultFileWriter {
		
	private final String columnDelimiter;
	
	private final String lineEnd;	
	
	private File resFile = null;
	
	private BufferedWriter resFileWriter = null;
	
	private StringBuilder sb;
	
	Logger logger = Logger.getLogger(ResultFileWriter.class);
	
			
			
	public ResultFileWriter(String columnDelimiter, String lineEnd,
			String fileName) throws Exception {		
		this.columnDelimiter = columnDelimiter;
		this.lineEnd = lineEnd;					
		resFile = new File(fileName);		
		resFileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(resFile),"UTF-8"));				
	}

	public void writeHeader(List<ResultColumn> headers) throws Exception{
				
		sb = new StringBuilder();		
		//write header
		boolean once = true;
		for(ResultColumn resCol : headers)
		{	
			
			if (resCol.isIncludeInResultFile() ){
				if (once == true){
					once = false;
				} else {					
					sb.append(columnDelimiter);					
				}				
				sb.append(resCol.getResColName());			
			}
		
		}
		sb.append(lineEnd);
		resFileWriter.write(sb.toString());		
	}
	
	public void writeResultRow(String[] rowData, List<ResultColumn> resultColsConf ) throws Exception{
		sb = new StringBuilder();
		
		
		
		
		for(int i = 0 ; i< rowData.length ; i++){
			
			// check if the column should be included in the result file
			
			if (resultColsConf.get(i).isIncludeInResultFile() ){			
				if (i > 0){			
					sb.append(columnDelimiter);									
				}
				sb.append(rowData[i]);			
			}
			
		}
		sb.append(lineEnd);				
		resFileWriter.write(sb.toString());		
		
		
	}
		
	public void close() throws Exception{
		if (resFileWriter != null){
			resFileWriter.close();
		}
	}

		
	
	
	
}
