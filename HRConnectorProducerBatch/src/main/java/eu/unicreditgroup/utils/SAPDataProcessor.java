package eu.unicreditgroup.utils;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;


import eu.unicreditgroup.batch.SourceFileProducer;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceColumn;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.mapper.exception.InvalidColumnException;
import eu.unicreditgroup.mapper.exception.InvalidRowException;

/**
 * @author UV00074
 *
 */
public class SAPDataProcessor implements InitializingBean{
	// metadata information of the source table columns
	
	private static final Logger logger = Logger.getLogger(SAPDataProcessor.class);
	
	private int columnsCount;
	
	//private SourceColumn userIdColumn;
	
	//private SourceColumn firstNameColumn;	
	
	//private SourceColumn lastNameColumn;	
		
	//List<SourceColumn> sourceColumns;
	SourceFileConf sourceFileConf;
	
	private BlockingQueue<Exception> exceptionsQueue;
		
	private ReportDataBean reportDataBean;
		
	@Override
	public void afterPropertiesSet() throws Exception {
				
		columnsCount = sourceFileConf.getSourceColumns().size();
		if (columnsCount <= 0){
			throw new Exception("Configuration problem wrong number of source column : " + columnsCount + ". Check the configuration.");
		}
	}
	
	//check the proper length of a file
	boolean validateUTF8Length(String str, int length) throws UnsupportedEncodingException{		
		if (str == null) {
			return true;
		}		
		//check the leght of UTF-8 encoded string
		int colLength = str.getBytes("UTF-8").length;		
		return colLength <= length ? true : false;		
	}
	
	
	private String getUserId(String[] sourceDataRow)
	{
		if (sourceDataRow == null || sourceDataRow.length != columnsCount){
			return null;
		}
		
		if (StringUtils.isNotEmpty( sourceDataRow[sourceFileConf.getSrcUserID().getSrcColIndex()])) {
			return sourceDataRow[sourceFileConf.getSrcUserID().getSrcColIndex()];
		} else {
			return sourceDataRow[sourceFileConf.getSrcFirstName().getSrcColIndex()] + " " + sourceDataRow[sourceFileConf.getSrcLastName().getSrcColIndex()];
		}
		
	}
	
	/**
	 * Checks that all mandatory fields of source row are present and valid
	 * @param sourceDataRow
	 * @return
	 * @throws Exception
	 */
	private boolean validateSourceRow(String[] sourceDataRow) throws Exception {		
				
		if(sourceDataRow.length != columnsCount) {
			// this shouldn't happen in general and will suggest wrong file format	
			String errMsg = String.format("Wrong number of columns : %s, User ID: %s Should be: %s", sourceDataRow.length, getUserId(sourceDataRow), columnsCount );
			InvalidRowException e = new InvalidRowException(errMsg); 
			logger.error(e);
			reportDataBean.addExceptionWithLimit(e);
			return false;
		}		
				
		// check all mandatory fields are not empty and valid 
		for(SourceColumn srcCol : sourceFileConf.getSourceColumns()){			
			if (srcCol.isMandatory() == true){
				String colValue = sourceDataRow[srcCol.getSrcColIndex()];
				
				// check not empty				
				if ( StringUtils.isEmpty( colValue ) )
				{
					// this shouldn't happen in general and will suggest wrong file format
					String errMsg = String.format("Field %s is empty for User Id: %s.", srcCol.getSrcColName(), getUserId(sourceDataRow));										
					InvalidRowException e = new InvalidRowException(errMsg);
					logger.error(e);					
					reportDataBean.addExceptionWithLimit(e);
					return false;
				}
				
				// and of proper length ... 
				if ( validateUTF8Length(colValue , srcCol.getLength()) == false )
				{
					// this shouldn't happen too... 
					String errMsg = String.format("Field %s for User Id: %s is of wrong length.", srcCol.getSrcColName(), getUserId(sourceDataRow));
					InvalidRowException e = new InvalidRowException(errMsg);
					logger.error(e);
					reportDataBean.addExceptionWithLimit(e);
					return false;			
				}
			}		
		}
		return true;		 
	}	
	
	/**
	 * process source data row
	 * @param sourceFileName 
	 * @param data
	 * @return
	 */
	public SourceRow processSourceData(String[] sourceDataRow) throws Exception{
		
		SourceRow sourceRow = new SourceRow(); 
		sourceRow.setData(sourceDataRow);
			
		// do not process invalid rows because they will be rejected anyway
		if ( validateSourceRow(sourceDataRow) == true ){
			sourceRow.setValid(true);	
			
			int i = 0;
			for(SourceColumn srcCol : sourceFileConf.getSourceColumns()){
				
				// check *only* non mandatory fields because
				// mandatory fields has been already checked by validateSourceRow
				if (srcCol.isMandatory() == false){				
					if (validateUTF8Length(sourceDataRow[i], srcCol.getLength()) == false)
					{					
						reportDataBean.incSourceFileInvalidColumnsCount();
						String errMsg = String.format("Column %s has wrong length. User Id: %s", srcCol.getSrcColName(), getUserId(sourceDataRow));
						InvalidColumnException e = new InvalidColumnException(errMsg);
						logger.error(e);								
						reportDataBean.addExceptionWithLimit(e);			
						sourceDataRow[i] = null;
					}
				}
				i++;
			}
		} else {
			sourceRow.setValid(false);
		}
						
		return sourceRow;
	}

	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}

	
	
}
