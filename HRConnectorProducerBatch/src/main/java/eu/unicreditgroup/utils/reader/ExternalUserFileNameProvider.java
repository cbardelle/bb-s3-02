package eu.unicreditgroup.utils.reader;

import java.util.ArrayList;
import java.util.List;

import eu.unicreditgroup.config.ConfigParams;

/**
 * 
 * Provider of the source file name for the file with external users 
 * @author UV00074
 *
 */
public class ExternalUserFileNameProvider implements SrcFileNameProvider {
	
	private ConfigParams configParams;

	@Override
	public ArrayList<String> getSourceFilesList() {
		
		ArrayList<String> sourceFilesList = new ArrayList<String>(); 
		
		sourceFilesList.add(configParams.getStrExternalUserSourceFileName());
		
		return sourceFilesList;
	}
	
	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

}
