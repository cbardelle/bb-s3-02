package eu.unicreditgroup.utils.reader;

import eu.unicreditgroup.utils.CsvReader;

public class SapFileReader implements FlatFileReader {
	
	private CsvReader reader;
	
	private final char CSV_COL_DELIMITER = ';';
	

	@Override
	public FlatFileReader initReader(String flatFilePath) throws Exception {
		
		reader = new CsvReader(flatFilePath);
			
		reader.setDelimiter(CSV_COL_DELIMITER);
				
		return this;		
	}

	@Override
	public boolean readNextRow() throws Exception {		
		return reader.readRecord();
	}

	@Override
	public String[] getColumnsValues() throws Exception {
	
		return reader.getValues();
	}

	@Override
	public void close() {
		reader.close();
	}

}
