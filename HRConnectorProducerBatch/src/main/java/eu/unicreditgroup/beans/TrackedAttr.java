package eu.unicreditgroup.beans;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class TrackedAttr implements Comparable<TrackedAttr>{

	//private String attrName;
	// refactoring use index 
	private Integer attrIdx;
	private String attrDesc;
	private String attrCode;
	private String mappedValue;
	private String dictValue;		
	private List<String> assgnProfiles;
	
	@Override public String  toString() {
		
		StringBuffer sb = new StringBuffer();
		sb.append(this.attrIdx).append(" ").append(this.attrCode).append(" ").append(this.mappedValue).append(" ").append(this.dictValue);
		return sb.toString();		
	}

	@Override public boolean equals(Object obj) {
	    //check for self-comparison
	    if ( this == obj ) return true;
	    
	    TrackedAttr attr = (TrackedAttr)obj;

	    //now a proper field-by-field evaluation can be made
	    return	    
	    	this.attrIdx == attr.attrIdx &&
	    	StringUtils.equals(this.attrCode, attr.attrCode);// &&
	    	/*StringUtils.equals(this.attrDesc, attr.attrDesc) &&*/
	    	/*StringUtils.equals(this.mappedValue, attr.mappedValue);*/ 
	    	/*StringUtils.equals(this.dictValue, attr.dictValue);*/
	}

	
	
	public Integer getAttrIdx() {
		return attrIdx;
	}

	public void setAttrIdx(Integer attrIdx) {
		this.attrIdx = attrIdx;
	}

	public String getAttrCode() {
		return attrCode;
	}

	public void setAttrCode(String attrCode) {
		this.attrCode = attrCode;
	}

	public String getMappedValue() {
		return mappedValue;
	}

	public void setMappedValue(String mappedValue) {
		this.mappedValue = mappedValue;
	}

	public String getDictValue() {
		return dictValue;
	}

	public void setDictValue(String dictValue) {
		this.dictValue = dictValue;
	}

	@Override
	public int compareTo(TrackedAttr o) {
		
		return this.toString().compareTo(o.toString());
	}

	public String getAttrDesc() {
		return attrDesc;
	}

	public void setAttrDesc(String attrDesc) {
		this.attrDesc = attrDesc;
	}

	public List<String> getAssgnProfiles() {
		return assgnProfiles;
	}

	public void setAssgnProfiles(List<String> assgnProfiles) {
		this.assgnProfiles = assgnProfiles;
	}


	public String getAssgnProfilesAsString(){
		StringBuffer sb = new StringBuffer();
		if (this.getAssgnProfiles() == null )
		{
			return "";			
		} else {
			Collections.sort(assgnProfiles);
			boolean not1stItem = false;
			for (String ap : assgnProfiles){
				if (not1stItem) {
					sb.append(", ");
				} else {
					not1stItem = true;
				}
				sb.append(ap);
			}
		}
		return sb.toString();
		
	}
	


}
