package eu.unicreditgroup.beans;

public class UserAttrExcp {
	
	private String srcAttrCode;
	
	//private String resAttrName;
	
	private Integer resAttrIdx;
		
	

	public String getSrcAttrCode() {
		return srcAttrCode;
	}

	public void setSrcAttrCode(String srcAttrCode) {
		this.srcAttrCode = srcAttrCode;
	}

	public Integer getResAttrIdx() {
		return resAttrIdx;
	}

	public void setResAttrIdx(Integer resAttrIdx) {
		this.resAttrIdx = resAttrIdx;
	}

		
	

}
