package eu.unicreditgroup.beans;

import eu.unicreditgroup.mapper.IMapper;

public class MappedColumn {
	
	private String value;
	
	//private String errorMsg = null;
	
	private MappingException mappingException;
	
	
	public String getValue() {
		return value;
	}
	
	public MappedColumn(){}
	
	public MappedColumn(String value){
		this.value = value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public MappingException getMappingException() {
		return mappingException;
	}

	public void createMappingException(String srcValue, String message, String mapperName, String userId) {
		
		MappingException me = new MappingException(message);
		
		me.setSrcValue(srcValue);		
		me.setMapperName(mapperName);
		me.setUserId(userId);		 
		
		this.mappingException = me;
	}

	/*
	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}*/
	
	
	
		
}
