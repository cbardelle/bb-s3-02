package eu.unicreditgroup.beans;

public class Locale {
		
	private String sourceNationId;
	
	private String  plateauLocaleId;

	public String getSourceNationId() {
		return sourceNationId;
	}

	public void setSourceNationId(String sourceNationId) {
		this.sourceNationId = sourceNationId;
	}

	public String getPlateauLocaleId() {
		return plateauLocaleId;
	}

	public void setPlateauLocaleId(String plateauLocaleId) {
		this.plateauLocaleId = plateauLocaleId;
	}	

}
