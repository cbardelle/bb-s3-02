package eu.unicreditgroup.beans;

public class TimeZone {
	
	String countryID;
	
	String plateauTimeZoneID;
	
	String plateauTimeZoneDesc;

	
	public String getCountryID() {
		return countryID;
	}

	public void setCountryID(String countryID) {
		this.countryID = countryID;
	}

	public String getPlateauTimeZoneID() {
		return plateauTimeZoneID;
	}

	public void setPlateauTimeZoneID(String plateauTimeZoneID) {
		this.plateauTimeZoneID = plateauTimeZoneID;
	}

	public String getPlateauTimeZoneDesc() {
		return plateauTimeZoneDesc;
	}

	public void setPlateauTimeZoneDesc(String plateauTimeZoneDesc) {
		this.plateauTimeZoneDesc = plateauTimeZoneDesc;
	}
	
}
