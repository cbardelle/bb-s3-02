package eu.unicreditgroup.beans;

public class ResultUserAttr {

	private Integer resAttrIdx;
	
	private String resAttrName;
	
	private String resAttrDesc;
	
	private String plateauAttrId;

	public Integer getResAttrIdx() {
		return resAttrIdx;
	}

	public void setResAttrIdx(Integer resAttrIdx) {
		this.resAttrIdx = resAttrIdx;
	}

	public String getResAttrName() {
		return resAttrName;
	}

	public void setResAttrName(String resAttrName) {
		this.resAttrName = resAttrName;
	}

	public String getResAttrDesc() {
		return resAttrDesc;
	}

	public void setResAttrDesc(String resAttrDesc) {
		this.resAttrDesc = resAttrDesc;
	}

	public String getPlateauAttrId() {
		return plateauAttrId;
	}

	public void setPlateauAttrId(String plateauAttrId) {
		this.plateauAttrId = plateauAttrId;
	}


	
	
	
	
}
