package eu.unicreditgroup.beans;

import org.apache.commons.lang.StringUtils;

public class NewTrackedAttr implements Comparable<NewTrackedAttr>{

	//private String attrName;
	// refactoring use index 
	private Integer attrIdx;
	private String attrDesc;
	private String attrCode;
	private String mappedValue;
	//private String dictValue;
	
	private final Integer HOST_LEGAL_ENT_CC_270_CODE = 74;
	
	
	
	
	@Override public String  toString() {
		
		StringBuffer sb = new StringBuffer();
		sb.append(this.attrIdx).append(" ").append(this.attrCode).append(" ").append(this.mappedValue).append(" ")/*.append(this.dictValue)*/;
		return sb.toString();		
	}

	@Override public boolean equals(Object obj) {
	    //check for self-comparison
	    if ( this == obj ) return true;
	    
	    NewTrackedAttr attr = (NewTrackedAttr)obj;

	    //now a proper field-by-field evaluation can be made
	    return	    
	    	this.attrIdx == attr.attrIdx &&
	    	StringUtils.equals(this.attrCode, attr.attrCode); 
	    	/*StringUtils.equals(this.attrDesc, attr.attrDesc) &&*/
	    	/*StringUtils.equals(this.mappedValue, attr.mappedValue) &&*/
	    	/*StringUtils.equals(this.dictValue, attr.dictValue);*/
	}

	
	public boolean isHostLegalEntAttr(){		
		if (attrIdx == HOST_LEGAL_ENT_CC_270_CODE){
			return true;
		} else {
			return false;
		}
	}
	
	
	public Integer getAttrIdx() {
		return attrIdx;
	}

	public void setAttrIdx(Integer attrIdx) {
		this.attrIdx = attrIdx;
	}

	public String getAttrCode() {
		return attrCode;
	}

	public void setAttrCode(String attrCode) {
		this.attrCode = attrCode;
	}

	public String getMappedValue() {
		return mappedValue;
	}

	public void setMappedValue(String mappedValue) {
		this.mappedValue = mappedValue;
	}

	@Override
	public int compareTo(NewTrackedAttr o) {
		
		return this.toString().compareTo(o.toString());
	}

	public String getAttrDesc() {
		return attrDesc;
	}

	public void setAttrDesc(String attrDesc) {
		this.attrDesc = attrDesc;
	}

	

}
