package eu.unicreditgroup.beans;

import org.apache.commons.lang.StringUtils;


public class MappingException extends Exception {
	
	
	private String srcValue;
	
	
	private String mapperName;
	
	private String userId; 
	
	
	public MappingException(String message) {
		
		super(message);
		
	}

	public String getSrcValue() {
		return srcValue;
	}

	public void setSrcValue(String srcValue) {
		this.srcValue = srcValue;
	}

	public String getMapperName() {
		return mapperName;
	}

	public void setMapperName(String mapperName) {
		this.mapperName = mapperName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public boolean equals(Object other){
	    if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof MappingException))return false;
	    
	    MappingException me = (MappingException)other;
	    
	    return (
	    StringUtils.equals( me.getSrcValue() , this.srcValue) && 
		StringUtils.equals( me.getMessage() , this.getMessage()) && 
		StringUtils.equals( me.getMapperName() , this.mapperName)
		);
	    
	    
	}
	

}
