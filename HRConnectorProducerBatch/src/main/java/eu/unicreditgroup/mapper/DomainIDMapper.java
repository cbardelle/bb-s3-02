package eu.unicreditgroup.mapper;

import java.util.concurrent.BlockingQueue;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.beans.HRBPUser;
import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.utils.Dictionary;

public class DomainIDMapper implements IMapper {
	
	Logger logger = Logger.getLogger(DomainIDMapper.class);
	
	private Dictionary dictionary;
	
	private  BlockingQueue<String> invalidHRBPQueue;
	
	private final String DOMAIN_ID_PREFIX = "LP_";
	
	private final String DOMAIN_ID_EXCEPTIONS = "LP_EXCEPTIONS";
	
	private final String DEFAULT_DOMAIN_4_EXTERNAL_USERS = "LP_ALLEATI";
	
	private final String DEFAULT_DOMAIN_4_GE_MANAGERS = "MANAGERS_DE";
		
	private final String DEFAULT_DOMAIN_4_AUDIT_ACADEMY = "LP_AUDIT";
	
	private final String AUDIT_ACADEMY_DOMAIN = "AA2";
		
	private SourceFileConf sourceFileConf;
	

	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		String hostLe = srcVal[1];
		
		if ( AUDIT_ACADEMY_DOMAIN.equals(hostLe) ) {
			
			return new MappedColumn( DEFAULT_DOMAIN_4_AUDIT_ACADEMY );		
			
		} else if (sourceFileConf.isExternalUser(sourceRow) ){
			
			return new MappedColumn(DEFAULT_DOMAIN_4_EXTERNAL_USERS);
		
		} else if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){
			
			return new MappedColumn(DEFAULT_DOMAIN_4_GE_MANAGERS);
		
		} else {

			// user ID of the HRBP user
			String hrbpId = srcVal[0];

			if (StringUtils.isEmpty(hrbpId)){
				// 2010-11-23 request : users without HRBPID columns won't be rejected
				// and bind to the LP_EXCEPTIONS domain 	
				hrbpId = DOMAIN_ID_EXCEPTIONS;
				
			} else {
				
				// check the HRBP is present (and active)
				HRBPUser hrbpUser = dictionary.findHRBPUser(hrbpId);
				
				if (hrbpUser == null){
					// the HRBP user cannot be found however the value will be not rejected
					// and it's stored only for reporting
					try {
						
						// this is not synchronized, as a result there are duplicates on the list
						// (duplicates will be filtered when getting result for the report)
						// consider using synchronized list container					
						//if ( !invalidHRBPQueue.contains(hrbpId)){
						
							invalidHRBPQueue.put(hrbpId);
							
						//}
					} catch (Exception e){
						logger.error(e);
					}
				}			
				// add prefix due to Stefano Prest request
				// add prefix regardless the HRBP user is present or not
				hrbpId = DOMAIN_ID_PREFIX + hrbpId;			
			}	
			return new MappedColumn(hrbpId);
		}		
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

	public BlockingQueue<String> getInvalidHRBPQueue() {
		return invalidHRBPQueue;
	}

	public void setInvalidHRBPQueue(BlockingQueue<String> invalidHRBPQueue) {
		this.invalidHRBPQueue = invalidHRBPQueue;
	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}
	
	
	
}
