package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.batch.BatchWorker;
import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.utils.Dictionary;

/**
 * This mapper class is responsible for setting country ID 
 * for Italian inbound expats with no country.
 * For the rest of users this value is passed directly
 * 
 * 
 * @author UV00074
 *
 */
public class CountryIDMapper implements IMapper {
	
	private SourceFileConf sourceFileConf;
	
	private Dictionary dictionary;
	
	private final String COUNTRY_ITALY = "IT";	
	
	Logger logger = Logger.getLogger(CountryIDMapper.class);
	

	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn();
				
		String expatType = sourceRow.getData()[sourceFileConf.getSrcExpatType().getSrcColIndex()];
		
		String country = sourceRow.getData()[sourceFileConf.getSrcOfficeCountry().getSrcColIndex()];
		
		String homeLegalEntityCode = sourceRow.getData()[sourceFileConf.getSrcHomeLegalEntityCode().getSrcColIndex()];
		
		
		// The default value for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			
			mappedCol.setValue(null);
			return mappedCol;
			
		
		/* As agreed with P.Corda this logic won't be used anyway 
		} else if (sourceFileConf.EXPAT_TYPE_INBOUND.equalsIgnoreCase(expatType) // inbound expat?
			&& 	StringUtils.isEmpty(country) == true 
			&& dictionary.isItalianOrg(homeLegalEntityCode)	){									
			// if the country is not set and home company is italian then set italian as user country			
			mappedCol.setValue(COUNTRY_ITALY);			
			logger.debug("Setting country: " + COUNTRY_ITALY + " for user: " + sourceRow.getIdOrUserName() + ", home legal ent code: " + homeLegalEntityCode);
		*/
			
		} else {
			// for the rest (outbound or no expat) retuen the default value or pass the value directly
			mappedCol.setValue(concatValues(srcVal, "-" ,defaultVal));			
		}		
		return mappedCol;
		
	}
	
	
	private String concatValues(String[] srcVal, String separator ,String defaultVal ){
		if (srcVal == null || srcVal.length == 0) {
			return defaultVal;
		} else {	
			StringBuilder sb = new StringBuilder();
			boolean allNull = true;
			for (int i = 0 ; i < srcVal.length ; i++ ){
				if  (StringUtils.isNotEmpty(srcVal[i])){
					allNull = false;
				}
			}
			if ( allNull == true ) {				
				return defaultVal;				
			}
			for (int i = 0 ; i < srcVal.length ; i++ ){			
				sb.append(srcVal[i]);				
				if (i < srcVal.length - 1){				
					sb.append(separator);				
				}
			}			
			return sb.toString();
		}
	}


	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}


	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}


	public Dictionary getDictionary() {
		return dictionary;
	}


	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
	
	
}
