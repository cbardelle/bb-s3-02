package eu.unicreditgroup.mapper;

import org.apache.log4j.Logger;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;

public class FullDepartmentCodeMapper implements IMapper {

	Dictionary dictionary;
	
	Logger logger = Logger.getLogger(FullDepartmentCodeMapper.class);
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {

		MappedColumn mappedCol = new MappedColumn(); 
		
		// The default value for all German managers
		if ( dictionary.checkForGermanManager( sourceRow.getUserId() )){			
			mappedCol.setValue(null);
			return mappedCol; 
		}
		
		
		String userOfficeCode = srcVal[0];
		
		String userId = sourceRow.getUserId();
		
		String fullDepCode = dictionary.getFullDepartmentCode( userOfficeCode );		
		
		if ( fullDepCode != null ){			
			mappedCol.setValue(fullDepCode);						
		} else {
			logger.debug("Full department code not available for user: " + userId + ", user office code: " +  userOfficeCode);					
		}
		
		return mappedCol;
	}
	
	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

}
