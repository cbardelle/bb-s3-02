package eu.unicreditgroup.mapper.exception;

public class InvalidColumnException extends Exception{
	
	public InvalidColumnException(String errMsg){
		super(errMsg);
	}
		
}
