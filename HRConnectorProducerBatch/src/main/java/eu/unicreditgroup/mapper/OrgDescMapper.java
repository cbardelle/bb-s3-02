package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;


/**
 * This purpose of this class is to assign Audit Acadaemy and Anagrafe Alleati users 
 * with a default Organization Unit description   
 *  
 * @author UV00074
 *
 */
public class OrgDescMapper implements IMapper {
	private final static String HOST_LE_CODE_ANAGRAFE = "AA";
	
	private final static String HOST_LE_CODE_AUDIT = "AA2";
	
	private final static String AUDIT_ACADEMY_OU_DESC = "Default OU for Audit Academy users";
	
	private final static String ANAGRAFE_ALLEATI_OU_DESC = "Default OU for Anagrafe Alleati users";
	
	
	/**
	 * 	The  mapper will return passed values separated by '-'
	 *  or the default value when source table is null or empty 
	 * 
	 */	
	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn();		
				
		if (srcVal == null || srcVal.length == 0) {
			mappedCol.setValue(defaultVal);
			return mappedCol; 
		} else {
			
			String hostLe = srcVal[2];
			
			// assign Audit and Anagrafe to predefined OUs 
			if (HOST_LE_CODE_ANAGRAFE.equals( hostLe ) ){
				mappedCol.setValue(ANAGRAFE_ALLEATI_OU_DESC);
				return mappedCol;
				
			} else if (HOST_LE_CODE_AUDIT.equals( hostLe ) ) {
				mappedCol.setValue(AUDIT_ACADEMY_OU_DESC);
				return mappedCol;
				
			} else { 
		
				StringBuilder sb = new StringBuilder();
				boolean allNull = true;
				
				String[] srcVal2 = new String[2];
				srcVal2[0] = srcVal[0];
				srcVal2[1] = srcVal[1];
				
				for (int i = 0 ; i < srcVal2.length ; i++ ){
					if  (StringUtils.isNotEmpty(srcVal2[i])){
						allNull = false;
					}
				}
				if ( allNull == true ) {				
					mappedCol.setValue(defaultVal);
					return mappedCol;
				}
				for (int i = 0 ; i < srcVal2.length ; i++ ){			
					sb.append(srcVal2[i]);				
					if (i < srcVal2.length - 1){				
						sb.append("-");				
					}
				}			
				mappedCol.setValue(sb.toString());
				return mappedCol;		
			}			
		}
	}
}
