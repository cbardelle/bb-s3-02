package eu.unicreditgroup.mapper.factory;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.mapper.DirectMapper;
import eu.unicreditgroup.mapper.EmailMapper;
import eu.unicreditgroup.mapper.FixedEmailMapper;
import eu.unicreditgroup.mapper.IMapper;
import eu.unicreditgroup.utils.Dictionary;

public class EmailMapperFactory {
	
	EmailMapper standardEmailMapper;
	
	private boolean useCustomEmailForAll;
	
	private String customEmailForAllValue = "";
	
	
	public EmailMapperFactory(String _useCustomEmailForAll, String _customEmailForAllValue){
		
		useCustomEmailForAll = processBooleanParameter(_useCustomEmailForAll);
		
		if( useCustomEmailForAll == true ) {
			if (StringUtils.isEmpty(_customEmailForAllValue) ){
				customEmailForAllValue = "";
			} else {
				customEmailForAllValue = _customEmailForAllValue;
			}
		}		
	}	

	public IMapper getMapperInstance()
	{	
		if( useCustomEmailForAll == true ) {
			return new FixedEmailMapper(customEmailForAllValue);
		} else {			
			
			return standardEmailMapper;
		}
	}	
	
	public boolean processBooleanParameter(String parameter){
		if (StringUtils.equalsIgnoreCase(parameter, "y")){
			parameter = "true";
		}

		if (StringUtils.equalsIgnoreCase(parameter, "1")){
			parameter = "true";
		}

		if (StringUtils.equalsIgnoreCase(parameter, "tak")){
			parameter = "true";
		}
		
		if (StringUtils.equalsIgnoreCase(parameter, "si")){
			parameter = "true";
		}
		
		return BooleanUtils.toBoolean(parameter);
	}


	public EmailMapper getStandardEmailMapper() {
		return standardEmailMapper;
	}


	public void setStandardEmailMapper(EmailMapper standardEmailMapper) {
		this.standardEmailMapper = standardEmailMapper;
	}
	
	
	
	
}
