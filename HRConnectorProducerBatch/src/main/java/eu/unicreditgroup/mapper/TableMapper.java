package eu.unicreditgroup.mapper;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import eu.unicreditgroup.batch.BatchWorker;
import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.MappedRow;
import eu.unicreditgroup.beans.MappingException;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.ResultColumn;
import eu.unicreditgroup.config.SourceColumn;
import eu.unicreditgroup.dao.DictionaryDao;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.mapper.exception.InvalidColumnException;
import eu.unicreditgroup.mapper.exception.InvalidRowException;
import eu.unicreditgroup.validator.IValidator;

public class TableMapper implements InitializingBean{

	Logger logger = Logger.getLogger(TableMapper.class);
	
	
	private int resultRowSize = 66;  
	
	/**
	 * metadata of all columns of the result table
	 */
	private List<ResultColumn> resultColumns;
	
	private ReportDataBean reportDataBean;
	
	private BlockingQueue<Exception> exceptionsQueue;
	
	private ArrayBlockingQueue<MappingException> mappingExceptions = new ArrayBlockingQueue<MappingException>(1024);
	
		
	@Override
	public void afterPropertiesSet() throws Exception {
		resultRowSize = resultColumns.size();
		if ( resultRowSize <= 0 ){
			throw new Exception("Configuratrion problem: result columns list is empty.");
		}
	}

	public MappedRow processRow(SourceRow sourceRow, String srcUserId){
		
		MappedRow mappedRow = new MappedRow();
		mappedRow.setData(new String[resultRowSize]);
				
		mappedRow.setValid(true);
		
		// map the result row
		int colIdx = 0;
		for(ResultColumn resCol : resultColumns){
			
			IMapper columnMapper = resCol.getMapper();		
			String defaultValue = resCol.getDefaultValue();			
			String mappedValue = null;			
			
			String[] srcValues = getSourceValues(resCol.getSrcCol(), sourceRow); 
					
			MappedColumn mappedCol = null;
			mappedCol = columnMapper.map(srcValues, defaultValue, sourceRow);
				
			if (mappedCol.getMappingException() != null) {
				
				// if failed to map mandatory column reject the record
				if (resCol.isMandatory() == true){
					mappedRow.setValid(false);
										
					InvalidRowException e = new InvalidRowException( String.format("Failed to map mandatory column %s, User : %s. %s", resCol.getResColName(), sourceRow.getIdOrUserName(), mappedCol.getMappingException().getMessage()) );
					logger.error(e);					
					reportDataBean.addExceptionWithLimit(e);
					//do not process rejected row
					break;
				} else {
					// for non mandatory column just report the exception and use its default value
					reportDataBean.incSourceFileInvalidColumnsCount();
					
										 
					//InvalidColumnException e = new InvalidColumnException(String.format("Failed to map column %s, User : %s. %s.", resCol.getResColName(), sourceRow.getIdOrUserName(), mappedCol.getErrorMsg() ));
														
					
					// do not CurrencyIDMapper exceptions because there are loads of them 
					if (! ( columnMapper instanceof  CurrencyIDMapper ) ){
												
						logger.error( String.format("Failed to map column %s, User : %s. %s.", resCol.getResColName(), sourceRow.getIdOrUserName(), mappedCol.getMappingException().getMessage() ) );
					}
					
					// try to limit the number of exceptions
					if ( !mappingExceptions.contains( mappedCol.getMappingException() ) ) {
						
						logger.debug(" Adding mapping exception: " + mappedCol.getMappingException().getMessage() );
						
						mappingExceptions.add( mappedCol.getMappingException() );						
						reportDataBean.addExceptionWithLimit( mappedCol.getMappingException() );
					}
					
					mappedValue = defaultValue;						
				}
			} else {
				mappedValue = mappedCol.getValue();
			}
			
			// run rejectable validators
			// for external users (from file source_aaext.csv) there are more mandatory columns (firsName, lastName, email)
			// external users without their mandatory columns will be rejected
			if (runAllValidators(resCol.getRejectableValidators(), mappedValue, srcValues, srcUserId, sourceRow) == false){
				// the row will be rejected
				mappedRow.setValid(false);
				
				
				InvalidRowException e = new InvalidRowException( String.format("Failed to map column %s, User : %s. %s", resCol.getResColName(), sourceRow.getIdOrUserName(), "Attribute value is not present in the dictionary!" ));
				
				
				logger.error(e);					
				reportDataBean.addExceptionWithLimit(e);										
				
			}
			
			
			// run non rejectable validator
			if (runAllValidators(resCol.getValidators(), mappedValue, srcValues, srcUserId, sourceRow) == false){
				mappedValue = defaultValue;
				logger.error("Validation for column " + resCol.getResColName() + " has failed and default value will be used: '" + defaultValue + "'");
				reportDataBean.incSourceFileInvalidColumnsCount();				
			}
			
			
			// if defined run the dictionary validator
			if (resCol.getDictionaryValidator() != null){				
				
				IValidator dictValidator = resCol.getDictionaryValidator();  
				
				
				// the validator will perform the logic necessary
				// to track new and changed attributes values
				boolean result = dictValidator.validate(mappedValue, srcValues, resCol.getResColIndex(), sourceRow);
				if (result == false){
					if (dictValidator.isRowRejectable() == true){
						// the row will be rejected
						mappedRow.setValid(false);
						InvalidRowException e = new InvalidRowException( String.format("Failed to map column %s, User : %s. %s", resCol.getResColName(), sourceRow.getIdOrUserName(), "Attribute value is not present in the dictionary!" ));
						logger.error(e);					
						reportDataBean.addExceptionWithLimit(e);										
					}
				}				
			}
			
			
			if (mappedValue == null){
				mappedValue = "";			
			}			
			
			mappedRow.setValue(colIdx, mappedValue);						
			colIdx++;						
		}
	
		return mappedRow;
	}

	private String[] getSourceValues(List<SourceColumn> srcCols,
			SourceRow sourceRow) {
		
		if(srcCols == null || srcCols.size() == 0){
			return null;
		}		
		String[] sourceValues = new String[srcCols.size()];
		int i = 0;
		for(SourceColumn srcCol : srcCols)
		{	
			String value = sourceRow.getValue(srcCol.getSrcColIndex());
			sourceValues[i] = value;						
			i++;
		}		
		return sourceValues;
		
	}


	public boolean runAllValidators(List<IValidator> validators, String value, String[] srcValues, String srcUserId, SourceRow sourceRow)
	{
		if (validators == null){
			return true;
		}
		boolean validRow = true;
		boolean result;
		for (IValidator validator : validators){
			result = validator.validate(value, srcValues, 0 /* not used */, sourceRow);
			if ( validRow == true && result == false){
				validRow = result;
			}
			
			// do not stop after first failure
			// run all validators to send back information 
			if (result == false){
				Exception e = new Exception("Validator of type " + validator.getClass().getName() + " has failed for value: '" + value + "' and User ID: " + srcUserId );
				logger.error(e);
				exceptionsQueue.add(e);
			}			
		}
		
		return validRow;
	}
	
	
	

	public List<ResultColumn> getResultColumns() {
		return resultColumns;
	}

	public void setResultColumns(List<ResultColumn> resultColumns) {
		this.resultColumns = resultColumns;
	}

	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	
	
}
