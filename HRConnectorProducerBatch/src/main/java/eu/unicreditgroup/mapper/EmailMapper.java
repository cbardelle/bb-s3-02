package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.utils.Dictionary;


/**
 * Email mapping 
 *  
 * @author UV00074
 *
 */
public class EmailMapper implements IMapper {
	
	Dictionary dictionary;
	
	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		
		MappedColumn mappedCol = new MappedColumn();
		
		String email = srcVal[0];
		if ( StringUtils.isEmpty(email) ){
			mappedCol.setValue(defaultVal);
			return mappedCol;	
		}
		
		String customEmail= dictionary.findCutomsEmail(sourceRow.getUserId());
								
		if ( StringUtils.isEmpty(customEmail)){
			// no custom email
			mappedCol.setValue(email);
		} else {
			// found a custom email
			mappedCol.setValue(customEmail);
			
		}		
		return mappedCol;		
	}
	
	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
}
