package eu.unicreditgroup.mapper;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.batch.BatchWorker;
import eu.unicreditgroup.beans.MappedColumn;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;


/**
 * This mapper had to be introduced to prevent  reseting description 
 * of the custom column 240 (org unit location type).
 * (Custom column description is reseted when null value is passed in the source file.)
 *  
 * The description of this column is determined using the value of the 
 * custom column 240. 
 * 
 * @author UV00074
 *
 */
public class OrgUnitLocTypeDescMapper implements IMapper {
	 
	private final String localItalian = "I";	
	private final String localItalianDesc = "Local Italian";	   
	
	private final String global = "G";
	private final String globalDesc = "Global";
	
	private final String local = "L";
	private final String localDesc = "Local";
	
	private final String globalGroup = "H";
	private final String globalGroupDesc = "Global Group";
	
	private final String globalOther = "O";
	private final String globalOtherDesc = "Global Other";
	   
	
	Logger logger = Logger.getLogger(OrgUnitLocTypeDescMapper.class);
	

	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		
				
		String orgUnitLocType = StringUtils.upperCase( StringUtils.trim(srcVal[0]) );
		
		
		MappedColumn mappedCol = new MappedColumn();

		
		if ( StringUtils.isEmpty(orgUnitLocType) )
		{
			return mappedCol; 
		}
		
		
		
		
		// set appropriate descriptions 
		if (StringUtils.equalsIgnoreCase(localItalian, orgUnitLocType)){
			mappedCol.setValue(localItalianDesc);	
		} else if (StringUtils.equalsIgnoreCase(global, orgUnitLocType)) {
			mappedCol.setValue(globalDesc);
		} else if (StringUtils.equalsIgnoreCase(local, orgUnitLocType)) {
			mappedCol.setValue(localDesc);
		} else if (StringUtils.equalsIgnoreCase(globalGroup, orgUnitLocType)) {
			mappedCol.setValue(globalGroupDesc);			
		} else if (StringUtils.equalsIgnoreCase(globalOther, orgUnitLocType)) {
			mappedCol.setValue(globalOtherDesc);			
		} else {
			logger.debug("Unknown value for org unit location type: " + orgUnitLocType);
		}
		
		return mappedCol;		
	}
	
}
