package eu.unicreditgroup.batch;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import eu.unicreditgroup.config.ConfigParams;



public class SourceFileProcessor {
	
	private ConfigParams configParams;
	
	private BlockingQueue<Exception> exceptionsQueue;

	Logger logger = Logger.getLogger(SourceFileProcessor.class);

	public void processSourceFile(SourceDataProducer sourceDataProd, ApplicationContext ctx) {
		
		
		Thread sourceFileProducerThread = new Thread(sourceDataProd);
		
		/* Read all source file */
		sourceFileProducerThread.start();
		
		String srcFiles = sourceDataProd.getSourceFilesAsString();
		
		logger.debug("Source file " + srcFiles + " producer thread started.");
				
		logger.debug("Start of source file(s) " + srcFiles + " queue consumers creation.");
		List<Thread> sourceFileConsumerThreads = new ArrayList<Thread>();
		
		int consumerBufSize =  configParams.getSourceBufferSize() / configParams.getSourceRowProcessThreadsNumber();
		for (int i = 0; i < configParams.getSourceRowProcessThreadsNumber(); i++) {
			logger.debug(String.format("Creating source file " + srcFiles + " queue consumer number %d.", i));
			SourceFileConsumer consumer = ctx.getBean(SourceFileConsumer.class);			
						
			consumer.setConsumerNumber(i);
			
			logger.debug("Setting source file(s) " + srcFiles + "  consumer buffer size : " + consumerBufSize);
			
			consumer.setBufferSize(consumerBufSize);
						
			Thread consumerThread = new Thread(consumer);
			sourceFileConsumerThreads.add(consumerThread);
			consumerThread.start();
		} 
		logger.debug("Source file(s) " + srcFiles + " queue consumers created.");
		
		for (Thread thread : sourceFileConsumerThreads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				logger.error("Error during waiting for end of created source file(s) " + srcFiles + " consumer threads.");
				exceptionsQueue.add(e);
			}
		}
		
		try {
			sourceFileProducerThread.join();
		} catch (InterruptedException e) {
			logger.error("Error during waiting for end of source file(s) " + srcFiles + " producer thread.");
			exceptionsQueue.add(e);
		}
		
	}


	public ConfigParams getConfigParams() {
		return configParams;
	}


	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}


	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}


	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	
	
}
