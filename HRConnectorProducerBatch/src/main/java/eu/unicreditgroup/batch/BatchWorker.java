//DEV01
package eu.unicreditgroup.batch;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import eu.unicreditgroup.batch.step.LoadOrgLstTableStep;
import eu.unicreditgroup.beans.ExpatUser;
import eu.unicreditgroup.beans.HRBPUser;
import eu.unicreditgroup.beans.MappedRow;
import eu.unicreditgroup.beans.NewTrackedAttr;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.beans.TrackedAttr;
import eu.unicreditgroup.beans.User;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.dao.DictionaryDao;
import eu.unicreditgroup.dao.ExpatUserDao;
import eu.unicreditgroup.dao.HRBPUserDao;
import eu.unicreditgroup.dao.HRBPUserStatus;
import eu.unicreditgroup.dao.SourceTableDao;
import eu.unicreditgroup.filter.AdditionalProcessor;
import eu.unicreditgroup.ftp.FTPUploader;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.processor.ExternalUserProcessor;
import eu.unicreditgroup.utils.AppContext;
import eu.unicreditgroup.utils.Dictionary;
import eu.unicreditgroup.utils.MappedRowsQueueUtil;
import eu.unicreditgroup.utils.PlateauAttrLogger;
import eu.unicreditgroup.utils.SourceFileUtil;
import eu.unicreditgroup.utils.SourceRowQueueUtil;


public class BatchWorker {
	
	Logger logger = Logger.getLogger(BatchWorker.class);
	
	private ConfigParams configParams;
	
	private SourceDataProducer sourceFileProducer;
	
	private SourceDataProducer additionalSourceFileProducer;
	
	private SourceDataProducer externalUsersFileProducer;
	
	private SourceDataProducer sourceManualDataProducer;	
	
	private SourceTableProducer sourceTableProducer;
		
	private MappedRowConsumer mappedRowsConsumer;
	
	private SourceTableDao sourceTableDao;
	
	private HRBPUserDao hrbpUserDao;
	
	private ExpatUserDao expatUserDao;
		
	private BlockingQueue<Exception> exceptionsQueue;
	
	private BlockingQueue<SourceRow> sourceRowsQueue;
	
	private BlockingQueue<MappedRow> mappedRowsQueue;
	
	private Dictionary dictionary;
	
	private ReportDataBean reportDataBean;
	
	//private SourceTableReader sourceTableReader;
	
	private FTPUploader sourceDownloader;
	
	private FTPUploader externalUserSourceDownloader;
	
	private FTPUploader resultUploader;
		
	private DictionaryDao dictionaryDao;
	
	private SourceFileProcessor sourceFileProcessor;
	
	private PlateauAttrLogger  plateauAttrLogger; 
	
	private LoadOrgLstTableStep loadOrgLstTableStep; 
	
	//private ExclusionProcessor exclusionProcessor;
	
	private AdditionalProcessor additionalProcessor;
	
	private AdditionalProcessor additionalProcessor2;
	
	private AdditionalProcessor additionalProcessor3; 
	
	
	private SourceFileUtil sourceFileUtil; 
	
	private ExternalUserProcessor externalUserProcessor;
	
		
	/**
	 * Main method here. Executes steps as set in config file.
	 * 
	 */	
	public void execute(){		
		reportDataBean.setBatchStartDate(new Date());
		

		
		
			// Step load organizations list into dictionary table
			loadOrgLstTableStep.execute();
			
			
			// Step load source file into the source table
			if (configParams.isStepLoadSourceTableEnabled()){
				try {				
					reportDataBean.setStepLoadSourceTableStartDate(new Date());				
					executeLoadSourceTableStep();
									
					reportDataBean.setStepLoadSourceTableEndDate(new Date());				
				} catch (Exception e){
					logger.error("Exception during step load source table", e);
					exceptionsQueue.add(e);
				}
			} else {
				logger.info("Step 'load source table' is disabled. Nothing to do here.");
			}
			
			
			// 2015-06-12 additional check to prevent sending
			// users without custom column 250 (Full dep code) being set
			if ( !checkFullDepCodeDataPresent() ){			
				reportDataBean.setFailedFastException(new Exception("The data in the column FULL_DEPARTMENT_CODE of the table HR_ORG_STRUCTURE seems to be incomplete or not present!"));			
					
			} else {
			
				//generate the source file
				if (configParams.isStepGenerateResultFileEnabled()){
					
					// check the result file can be generated
					if( checkGenerateResultFilePrereq() == false ){
										
						logger.error("Prerequisites for generating result file are not met. The result file can not be generated.");
						
					} else {
					
						try {				
							reportDataBean.setStepGenerateResultFileStartDate(new Date());				
							executeGenerateResultFileStep();
							reportDataBean.setStepGenerateResultFileEndDate(new Date());				
						} catch (Exception e){
							logger.error("Exception during step generate result file", e);
							exceptionsQueue.add(e);
						}
					}
				}
		
			}
		
		reportDataBean.setBatchEndDate(new Date());	
		
		long diff = reportDataBean.getBatchEndDate().getTime() - reportDataBean.getBatchStartDate().getTime(); 
		
		long diffMinutes = diff / (60 * 1000);
			 
		System.out.println("--------------------------------------------------");
		System.out.println("Batch executed in " + diffMinutes);
	}
	

	private void removeDuplicatedUserId() {
 		
		Map duplicates = reportDataBean.getDuplicatedUserId();
		
		if (duplicates == null || duplicates.size() == 0){
			// nothing to do
			return;
		}
		
		try {			
			
			logger.info("Deleting duplicated user Id...");
			int deletedCount = sourceTableDao.removeDuplicatedUserId(duplicates.keySet());
			logger.info(String.format("The number of deleted user Id dupclicates was %d", deletedCount));
			
			//remove deleted duplicates
			reportDataBean.removeTempSourceTableRowsInserted(deletedCount);
			
			// add duplicated
			reportDataBean.addSourceFileInvalidRowsCount(reportDataBean.getSourceFileDuplicatedUserIdCount());
			
			
		} catch (Exception e){
			Exception ex = new Exception("Field to delete duplicated users. ", e);
			logger.error(ex);
			exceptionsQueue.add(ex);						
		}		
	}
	
	private void processOutboundExpatUsers() {
		
		try{
		
		// 1. find all new expats
			List<ExpatUser> newOutboundExpats = expatUserDao.getNewOutboundExpatUsers();
			
			
			if (newOutboundExpats != null && !newOutboundExpats.isEmpty() ){
				// - insert them into the hr_conn_expat_users table
				expatUserDao.addNewOutboundExpatUsers(newOutboundExpats);
			
				// - include them in the report
				reportDataBean.addOutboundExpatUsers(newOutboundExpats);
			}
			
			// 2. find expats removed in the past but present in the current version of the file
			List<ExpatUser> reappearedOutboundExpats = expatUserDao.getReappearedOutboundExpatUsers();
			
			logger.info("NUMBER OF REAPPERED EXPATS : " + reappearedOutboundExpats.size());
			
			if (reappearedOutboundExpats != null && !reappearedOutboundExpats.isEmpty() ){
				
				// - reset remove_date for this users
				expatUserDao.resetRemoveDateForExpat(reappearedOutboundExpats);
				
				// - include them in the report			
				reportDataBean.addOutboundExpatUsers(reappearedOutboundExpats);
			}
			
			// find related inbounds (by first name, last name and date of birth) 
			if (reportDataBean.getOutboundExpatsUsers() != null && !reportDataBean.getOutboundExpatsUsers().isEmpty()){
	
				logger.info("Number of new outbound expats: " + reportDataBean.getOutboundExpatsUsers().size());
				
				expatUserDao.findAndSetRelatedInboundsExpats(reportDataBean.getOutboundExpatsUsers());
				
				for (ExpatUser eu : reportDataBean.getOutboundExpatsUsers()){				
					logger.info("Outbound expat user: " + eu.getExpatUserId() + " , related inbound expat: " + eu.getRelatedInboudExpatUserId());
				}		
			}
			
			// find outbound expats removed from current version of the source file and set them remove date 		
			expatUserDao.setRemoveDate( expatUserDao.findRemovedOutboundExpats(), new Date() );
			
			 
		} catch (Exception e){
			Exception ex = new Exception("Exception when processing outbound expat users.", e);
			logger.error(ex);
			exceptionsQueue.add(ex);
			
		}		
	}

	private int processHrbpUsers() {
		int hrbpUsersCount = this.hrbpUserDao.writeHRBPUsersTempTable();		
		
		
		List<HRBPUser> newHRBPUsers = hrbpUserDao.getNewHRBPUsers();
		List<HRBPUser> removedHRBPUsers = hrbpUserDao.getRemovedHRBPUsers();
		
		// check users with empty domains
		
		// this is not possible now because only HRBP with filled domains are selected
		//List<HRBPUser> newHRBPUsersWithEmptyDomain = hrbpUserDao.checkForEmptyDomains(newHRBPUsers);  
		//reportDataBean.setNewHRBPUsersWithEmptyDomain(newHRBPUsersWithEmptyDomain);	
		
		reportDataBean.setNewHRBPUsers(newHRBPUsers);		
		reportDataBean.setRemovedHRBPUsers(removedHRBPUsers);
		
		hrbpUserDao.writeUsersToLogTable(newHRBPUsers, HRBPUserStatus.NEW_HRBP_USER);
		hrbpUserDao.writeUsersToLogTable(removedHRBPUsers, HRBPUserStatus.REMOVED_HRBP_USER);
				
		
		// this will be left to process current HRBP with empty domains.
		// but at some moment in the feature won't be necessary 
		
		// before writing information about HRBP with empty domain
		// check existing HRBPs with empty domains if they have been filled
		List<HRBPUser> usersWithFilledDomain = hrbpUserDao.getHrbpWithFilledDomain();
		reportDataBean.setUsersWithFilledDomain(usersWithFilledDomain);
		hrbpUserDao.writeUsersToLogTable(usersWithFilledDomain, HRBPUserStatus.HRBP_USER_FILLED_DOMAIN);
		
		// delete log info about empty domains for HRBPs users which domains has been filled
		// (so they are not processed next time) 
		hrbpUserDao.deleteLogInfoForFilledDomain(usersWithFilledDomain);
				
		
		// in the end write new hrbp users with empty domain
		// this is not possible now because only HRBP with filled domains are selected
		//hrbpUserDao.writeUsersToLogTable(newHRBPUsersWithEmptyDomain, HRBPUserStatus.NEW_HRBP_USER_EMPTY_DOMAIN);
				
		
		hrbpUserDao.refreshMainTable();
		return hrbpUsersCount;
	}
	
	
	/**
	 * Check prerequisites for creating backup of the main source table 
	 * @return
	 */
	private boolean checkBakupMainSourceTablePrereq(){
				
		int mapped = reportDataBean.getMappedRowsCount();
		
		int allRows = reportDataBean.getSourceFileRowsCount();
		
		int validRows = allRows - reportDataBean.getSourceFileInvalidRowsCount();
		
		float validRowsFactor = (float) ((float)validRows * 100.0 / (float)allRows);
		float insertedRowsFactor = (float) ((float)reportDataBean.getMainSourceTableRowsInserted() * 100.0 / (float)validRows);

		if (validRowsFactor < configParams.getValidSourceRowsPercentage()){
			logger.error("Too few valid rows : " + (int)validRows);
			return false;
		}
		
		if (insertedRowsFactor < configParams.getValidInsertedRowsPercentage()){			
			logger.error("Too few valid rows inserted into the main table");			
			return false;
		}
		
		return true;
	
	}
	
	
	/**
	 * Check if the organization data from the table HR_ORG_STRUCTURE 
	 * is present
	 * The full department code is very important because it's used for
	 * Assignment Profiles in LMS 
	 * 
	 * In case the FDC would get not generated users would loose their assignments
	 * (As it has happened once, so it can happen twice ;))
	 * 
	 * @return
	 */
	private boolean checkFullDepCodeDataPresent()
	{
		if ( dictionaryDao.checkDistinctFullDepartmentCodes() < 1000){			
			return false;
		} else {
			return true;
		}
	}
	
	
	/**
	 * Check prerequisites for cleaning  the table HR_CONN_SAP_SRC 
	 * @return
	 */
	private boolean checkCleanSapDataTablePrereq()
	{
		
		// check no problem when reading the source
		if (reportDataBean.getFailedFastException() != null ){
			logger.error("The batch won't continue due to fatal exception", reportDataBean.getFailedFastException());
			return false;
		}		

		int allRows = reportDataBean.getSourceFileRowsCount();
		int validRows = allRows - reportDataBean.getSourceFileInvalidRowsCount(); /*- reportDataBean.getExcludedTableRowsInserted();*/ /*- reportDataBean.getSourceFileDuplicatedUserIdCount();*/		
					
		
		// check source file is not empty
		if (reportDataBean.getSourceFileRowsCount() == 0 ){
			String errMsg = "The source file is empty!"; 
			logger.error(errMsg);
			reportDataBean.setFailedFastException(new Exception(errMsg));
			return false;
		}
		
		//no valid or too few valid source users records				
		// calculate valid rows factor
		float validRowsFactor = (float) ((float)validRows * 100.0 / (float)allRows);
		
		float insertedRowsFactor = (float) ((float)reportDataBean.getTempSourceTableRowsInserted() * 100.0 / (float)validRows);
		
		logger.info("VALID ROWS :" + validRows);
		logger.info("VALID ROWS INSERTED INTO TEMP SOURCE TABLE :" + reportDataBean.getTempSourceTableRowsInserted());
		
		
		if (validRowsFactor < configParams.getValidSourceRowsPercentage()){
			String errMsg = "There is too few valid rows : " + (int)validRows; 
			logger.error(errMsg);
			reportDataBean.setFailedFastException(new Exception(errMsg));
			return false;
		}
		
		if (insertedRowsFactor < configParams.getValidInsertedRowsPercentage()){
			
			String errMsg = "Not all source data was loaded into database."; 
			logger.error(errMsg);
			reportDataBean.setFailedFastException(new Exception(errMsg));
			return false;
		}
		
		
		
		return true;
	}

	
	

	private boolean checkGenerateResultFilePrereq()
	{
		
		// check no problem when reading the source
		if (reportDataBean.getFailedFastException() != null ){
			logger.error("The batch won't continue due to fatal exception", reportDataBean.getFailedFastException());
			return false;
		}
				
		if (configParams.isStepLoadSourceTableEnabled()){
			
			int allRows = reportDataBean.getSourceFileRowsCount();
			int validRows = allRows - reportDataBean.getSourceFileInvalidRowsCount(); /*- reportDataBean.getExcludedTableRowsInserted();*/ /*- reportDataBean.getSourceFileDuplicatedUserIdCount();*/		
						
			
			// check source file is not empty
			if (reportDataBean.getSourceFileRowsCount() == 0 ){
				String errMsg = "The source file is empty!"; 
				logger.error(errMsg);
				reportDataBean.setFailedFastException(new Exception(errMsg));
				return false;
			}
			
			//no valid or too few valid source users records				
			// calculate valid rows factor
			float validRowsFactor = (float) ((float)validRows * 100.0 / (float)allRows);
			
			float insertedRowsFactor = (float) ((float)reportDataBean.getMainSourceTableRowsInserted() * 100.0 / (float)validRows);
			
			logger.info("Checking result file generation prerequisities...");
			logger.info("ALL ROWS IN THE SOURCE FILES: " + allRows);
			logger.info("INSERTED INTO MAIN TABLE: " +  reportDataBean.getMainSourceTableRowsInserted());
			logger.info("VALID ROWS (ALL - INVALID): " + validRows);
			logger.info("VALID ROWS FACTOR: " + validRowsFactor + "%");
			logger.info("VALID AND INSERTED INTO SRC TABLE ROWS FACTOR: " + insertedRowsFactor + "%");
			logger.info("INVALID: " + reportDataBean.getSourceFileInvalidRowsCount());
			logger.info("DUPLICATED: " + reportDataBean.getSourceFileDuplicatedUserIdCount());
			//logger.info("INSERTED INTO EXCLUDED TABLE: " + reportDataBean.getExcludedTableRowsInserted());
			
			
			if (validRowsFactor < configParams.getValidSourceRowsPercentage()){
				String errMsg = "There is too few valid rows : " + (int)validRows ; 
				logger.error(errMsg);
				reportDataBean.setFailedFastException(new Exception(errMsg));
				return false;
			}
			
			// check all data in the source table
			
			// give up strict checking becasuse it failes in some rare situations...
			//if (reportDataBean.getMainSourceTableRowsInserted()  != validRows ){
			
			if (insertedRowsFactor < configParams.getValidInsertedRowsPercentage()){
							
				String errMsg = "Not all source data was loaded into database."; 
				logger.error(errMsg);
				reportDataBean.setFailedFastException(new Exception(errMsg));
				return false;
			}
		}
		
		return true;
	}
		
	
	/**
	 * Multithreaded implementation of step responsible for loading data 
	 * from source flat file into source table
	 */
	public void executeLoadSourceTableStep(){
		logger.info("Start of execution for step load source table");
		ApplicationContext ctx = AppContext.getApplicationContext();
		
		if (configParams.isDownloadSourceFileEnabled() ){			
			try {	
				
				String sourceFileName = "";
				sourceFileUtil.backupAllSourceFiles();		
				
				
				for (String  srcFileName: configParams.getSourceFilesList() ){
					
					try {
						logger.info("Starting download of source file: " + srcFileName);
						sourceFileName = srcFileName;
						sourceDownloader.downloadFile( srcFileName, configParams.getStrSourceLocalDir() + "\\" + srcFileName );
						
						logger.info("Source file : " + srcFileName + " downloaded.");
						
						// check the primary src file is not of  zero - length
						if (StringUtils.equalsIgnoreCase(sourceFileName, configParams.getStrPrimarySourceFileName())){							
							File primarySrcFile = new File(configParams.getStrSourceLocalDir() + "\\" + srcFileName);														
							if (primarySrcFile.length() == 0){
								Exception e = new Exception("The primary source file is empty!");
								reportDataBean.setFailedFastException(e);
								throw e;
							}
						}
						
					} catch (Exception e){
						 if (StringUtils.equalsIgnoreCase(sourceFileName, configParams.getStrPrimarySourceFileName())){
							 throw e;
						 } else {
							 logger.info("Failed to download non-primary source file: " + sourceFileName);
						 }
					}
				}
				
			} catch (Exception e) {
				reportDataBean.setFailedFastException(new Exception("Failed to download the primary source file",e));					
			}
		}
				
		
		// check there are no fatal errors like for example empty primary source file
		// (this will prevent from deleting HR_CONN_SAP_SRC and HR_CONN_SAP_SRC_TMP tables) 
		if (reportDataBean.getFailedFastException() != null){
			logger.info("Load source table step will end due to fatal error: " + reportDataBean.getFailedFastExceptionMsg());
			return;
		}
				 
		if (configParams.isDownloadExternalUsersFileEnabled() ){
			
			String extUsersFile = configParams.getStrExternalUserSourceFileName();
			try{
				sourceFileUtil.backupFile(configParams.getExternalUserSourceFullFilePath());			
			} catch (Exception e){				
				logger.error("Failed to backup external users source file: " + configParams.getExternalUserSourceFullFilePath() , e);				
			}
									
			try {				
				logger.info("Starting download of external users source file: " + extUsersFile);
				
				// please note that the external users file is not in the same directory on the ftp
				// as the SAP source files
				externalUserSourceDownloader.downloadFile( extUsersFile, configParams.getStrSourceLocalDir() + "\\" + extUsersFile );				
				logger.info("External users source file : " + extUsersFile + " downloaded.");		
				
			} catch (Exception e){				 
				logger.error("Failed to download external users source file: " + extUsersFile, e);				 
			}
		}
								
		logger.info("Cleaning temp table with source rows");
		sourceTableDao.cleanTempTable();	
				
		if (configParams.isProcessHRBPUsers() == true){
			logger.info("Cleaning temp table with HRBP users");
			hrbpUserDao.cleanTempTable();
		} else {
			logger.info("Loading of HRBP users is disabled. Nothing to do here.");
		}
		
			
		sourceFileProcessor.processSourceFile(sourceFileProducer, ctx);
		
		//TODO refactor it
		// this is temporary solution to flush German managers  
		// to the HR_CONN_SAP_EXCLD table
		//exclusionProcessor.finishProcessing();
		
		//additionalProcessor.finishProcessing();
		
		//additionalProcessor2.finishProcessing();
		
		//additionalProcessor3.finishProcessing();
			
		
		// process source file with external users (Anagrafe Alleati)
		if (configParams.isProcessExternalUsersFileEnabled() ){
			logger.info("Processing source file with external users.");
			
			// remove the end of queue mark!
			sourceRowsQueue.clear();
			
			sourceFileProcessor.processSourceFile(externalUsersFileProducer, ctx);
			
			// set in the report list of reactivated external users
			reportDataBean.setListOfReactivatedExternalUsers(externalUserProcessor.getListOfReactivatedExternalUsers());
			
			
		}  else {
			logger.info("Processing source file with external disabled.");
		}
		
		
		// at this point the main source file is loaded into the temp source table
		// load additional source file if any
		
		if (StringUtils.isNotEmpty((configParams.getStrAdditionalSourceFileName()))){
			
			logger.info("Loading additional source file: " + configParams.getStrAdditionalSourceFileName());
			
			// remove the end of queue mark!
			sourceRowsQueue.clear();
			
			sourceFileProcessor.processSourceFile(additionalSourceFileProducer, ctx);
		} else {
			logger.info("Additional source file not specified, nothing to do");
			
		}
		

		// process table HR_CONN_MNL_SRC (with manually inserted users data) 
		if (configParams.isProcessManualSourceTable() ){
			logger.info("Processing table HR_CONN_MNL_SRC (with manually inserted users data).");
			
			// remove the end of queue mark!
			sourceRowsQueue.clear();
						
			sourceFileProcessor.processSourceFile(sourceManualDataProducer, ctx);
			
			
		}  else {
			logger.info("Processing table HR_CONN_MNL_SRC (with manually inserted users data) disabled.");
		}
		
		
		
		
		// process duplicated userId if any
		removeDuplicatedUserId();

		
		
		// and just before copying tmp table to the main table...
		// perform some additional cheks...
		if(configParams.isStepCheckUsersWithChangedId()){
			try{
				executStepCheckUsersWithChangedId();				
			} catch (Exception e){
				logger.error("Exception during step check users with changed id", e);
				exceptionsQueue.add(e);				
			}				
		}else{
			logger.info("Step 'check users with changed id' is disabled. Nothing to do here.");
		}
		
		if(configParams.isStepCheckUsersWithEmptyResume()){
			try{
				executeStepCheckUsersWithEmptyResume();				
			} catch (Exception e){
				logger.error("Exception during step check users with empty resume", e);
				exceptionsQueue.add(e);			
			}			
		}else{
			logger.info("Step 'check users with empty resume' is disabled. Nothing to do here.");	
		}
		
		 
		logger.info("Copying source rows to the main table.");
		
		
		if (checkCleanSapDataTablePrereq() == true) {
			int refreshedRowsCount = sourceTableDao.refreshMainTable();		
			logger.info("Number of records copied to the main SAP source table : " + refreshedRowsCount);
			reportDataBean.setMainSourceTableRowsInserted(refreshedRowsCount);								
		} else {
			logger.info("The SAP data will be not updated with the new data becasue it seems to be incomplete.");
			
		}
		
		// process the HRBP users
		if (configParams.isProcessHRBPUsers() == true ){
			if ( reportDataBean.getFailedFastException() == null){
				logger.info("Processing HRBP users...");
				reportDataBean.setValidHRBPUsersCount(processHrbpUsers());
			} else {
				logger.info("HRBP users will be not processed becasue of the fatal exception: " + reportDataBean.getFailedFastExceptionMsg());
				
			}			
		} else {
			logger.info("Processing of HRBP users is disabled. Nothing to do here.");
		}
		
		// process Expat users
		if (configParams.isProcessExpatUsers() == true){
			if ( reportDataBean.getFailedFastException() == null){
				logger.info("Processing Expat users...");				
				processOutboundExpatUsers();
			} else {
				logger.info("Expat users will be not processed becasue of the fatal exception: " + reportDataBean.getFailedFastExceptionMsg());
				
			}
			
		} else {
			logger.info("Processing of Expat users is disabled. Nothing to do here.");
		}
		
		
		//TODO consider to add parameter to config.ini 
		// get count of users with empty email
		if ( reportDataBean.getFailedFastException() == null){
			logger.info("Looking for count of users with empty email.");				
			reportDataBean.setUsersWithEmptyEmailCount(sourceTableDao.getUsersWithEmptyEmailCount());
		} else {
			logger.info("Users with empty email won't be found becasue of the fatal exception: " + reportDataBean.getFailedFastExceptionMsg());			
		}
		
		
		logger.info("End of step load source table");
	}
	
	
	public void executeGenerateResultFileStep(){
		logger.info("Start of execution for step generate result file");
		ApplicationContext ctx = AppContext.getApplicationContext();
		
		// backup result file if any
		try{
			sourceFileUtil.backupFile(configParams.getPlateauFullFilePath());			
		} catch (Exception e){
			exceptionsQueue.add(e);
		}
		
		
		// clean up source queue		
				
		if ( sourceRowsQueue.size() > 1 ||
			sourceRowsQueue.size() == 1 && 
			!SourceRowQueueUtil.isShutdownRow(sourceRowsQueue.peek()))
		{
			logger.error("Definitly not good and shouldn't get here becasue the source queue should be epmty!!!");
		}
		
		sourceRowsQueue.clear();

		// init HRBP users data!
		dictionary.initHrbpUsersMap();
			
		// read data from database
		Thread sourceTableProducerThread = new Thread(sourceTableProducer);
		sourceTableProducerThread.start();
		logger.info("Source table producer thread started.");

				
		
		/*
		 this doesn't work - think it over
		int sourceTableProducerThreadCount = 4;
		logger.info("Start of source table queue producers creation.");
		List<Thread> sourceTableProducerThreads = new ArrayList<Thread>();
		for (int i = 0; i < sourceTableProducerThreadCount; i++) {
			logger.info(String.format("Creating source table queue producer number %d.", i));
			SourceTableProducer producer = ctx.getBean(SourceTableProducer.class);
			producer.setConsumerNumber(i);	
			Thread producerThread = new Thread(producer);
			producerThread.setName("Source table producer " + i);
			sourceTableProducerThreads.add(producerThread);
			producerThread.start();
		} 
		logger.info("Source table queue producers created.");
		*/
		

		logger.info("Start of source table queue consumers creation.");
		List<Thread> sourceTableConsumerThreads = new ArrayList<Thread>();
		for (int i = 0; i < configParams.getSourceRowProcessThreadsNumber(); i++) {
			logger.info(String.format("Creating source table queue consumer number %d.", i));
			SourceTableConsumer consumer = ctx.getBean(SourceTableConsumer.class);			
			consumer.setConsumerNumber(i);												
			Thread consumerThread = new Thread(consumer);
			sourceTableConsumerThreads.add(consumerThread);
			consumerThread.start();
		} 
		logger.info("Source table queue consumers created.");
		
		
		
		Thread mappedRowsConsumerThread = new Thread(mappedRowsConsumer);
		mappedRowsConsumerThread.start();
		logger.info("Mapped rows consumer thread started.");
	
		
		for (Thread thread : sourceTableConsumerThreads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				logger.error("Error during waiting for end of created source table consumer threads.");
				exceptionsQueue.add(e);
			}
		}		
		
		
		try {
			if (!MappedRowsQueueUtil.isShutdownRow(mappedRowsQueue.peek())){
				mappedRowsQueue.put(MappedRowsQueueUtil.generateShutdownRow());
			}
		} catch (InterruptedException e1) {
			logger.error("Exception occured : " + e1);
		}
		
		/*
		for (Thread thread2 : sourceTableProducerThreads) {
			try {
				thread2.join();
			} catch (InterruptedException e) {
				logger.error("Error during waiting for end of created source table producer threads.");
				exceptionsQueue.add(e);
			}
		}		
		*/
	
		
		try {
			sourceTableProducerThread.join();
		} catch (InterruptedException e) {
			logger.error("Error during waiting for end of source table producer thread.");
			exceptionsQueue.add(e);
		}

		
		try {
			mappedRowsConsumerThread.join();
		} catch (InterruptedException e) {
			logger.error("Error during waiting for end of mapped rows consumer thread.");
			exceptionsQueue.add(e);
		}
				
		
		if (configParams.isUploadGeneratedFileEnabled() ){			
			
			if (reportDataBean.getFailedFastException() == null ){
				try {
					
					
					logger.info("Starting upload of result file ...");
					resultUploader.uploadFile(new File(configParams.getPlateauFullFilePath()));
					logger.info("Result file uploaded.");
				} catch (Exception e) {
					reportDataBean.setFailedFastException(new Exception("Failed to upload the result file",e));					
				}
			}
			
		} else {
			logger.info("Upload of result file was skipped.");
		}
		
		// move processed SAP source file to the directory with processed files.
		
		if (configParams.isMoveProcessedSapFileEnabled()){		
			
			sourceFileUtil.moveProcessedSapFiles();
			
		} else {
			logger.info("Moving of processed source file was skipped.");
		}
		
		// move processed source file with external users to the directory with processed files
		if (configParams.isMoveProcessedExternalUsersFileEnabled()){		
			
			sourceFileUtil.moveProcessedExtrnalUsersFile();
			
		} else {
			logger.info("Moving of processed source file with external users was skipped.");
		}
		
		
		// additional task : create new users attributes
		try {
			List<NewTrackedAttr> newAttr = new ArrayList<NewTrackedAttr>(reportDataBean.getNewTrackedAttrQueue());
			
			List<TrackedAttr> changedAttr = new ArrayList<TrackedAttr>(reportDataBean.getChangedTrackedAttrQueue());						
			
			if ( !newAttr.isEmpty() || !changedAttr.isEmpty() ){				
				logger.info("Updating new and changed users attributes...");
				
				dictionaryDao.updateUserAttrDict(newAttr, changedAttr);
				
				logger.info("Updating new and changed users attributes done.");
			}			
			
		} catch (Exception e){
			logger.error("Failed to insert new user attributes values into the dictionary table",e);			
		} 
		
				
		// additional task : log information about changed users attributes
		try {
			plateauAttrLogger.logInformationAboutChangedAttr(reportDataBean.getChangedTrackedAttrWithAsgnPrfList(), new Date());
		} catch (Exception e) {
			logger.error("Failed to update information about changed assignment profiles attributes",e);
		} 	
		
		// addditional task : create backup of the HR_CONN_SAP_SRC table if possible
		
		// check the file was generated 
		
		
		
		// create backup of the main source table if the prerqusities are met
		// the backup will contain the SAP data userd for the last successful creation 
		// of the file for the HR Connector
		
		if (checkBakupMainSourceTablePrereq() == true ) {		
			logger.info("Backing up the main source table.");
			try {
				long cnt = sourceTableDao.bakupMainSourceTable();
				
				logger.info("The number of copied rows into backup table : " + cnt);
				
			} catch (Exception e){
				logger.error("Failed to backup the main source table.",e);
			}
		} else {
			logger.error("Backup of the main source table will be not created, becasue the prerequsites are not met.");
		}
		
		
		// update the data in the table HR_CONN_MNL_SRC 
		// mark related rows as sent
		
		if ( configParams.isProcessManualSourceTable() ){
			logger.info("Updating the table HR_CONN_MNL_SRC.");
			
			try {
				
				int res = ((SourceManualDataProducer)sourceManualDataProducer).getManualSourceTableDao().updateSentRows();
				logger.info("Updated " + res + " records in the table HR_CONN_MNL_SRC.");
				
				
			} catch (Exception e){				
				logger.error("Failed to update the table HR_CONN_MNL_SRC");
				exceptionsQueue.add(e);				
			}
			
		} else {
			
			logger.error("Processing of the HR_CONN_MNL_SRC table is disabled.");
		}
						
		
		
		logger.info("End of step generate result file");		
	}
	

	
	private void executeStepCheckUsersWithEmptyResume() {
		List<User> emptyResumeUsers = sourceTableDao.getUsersWithEmptyResume();
		reportDataBean.setUsersWithEmptyResume(emptyResumeUsers);		
	}


	private void executStepCheckUsersWithChangedId() {
		List<User> usersWithChangedId = sourceTableDao.getUsersWithChangedId();
		reportDataBean.setUsersWithChangedId(usersWithChangedId);
	}
	

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public SourceDataProducer getSourceFileProducer() {
		return sourceFileProducer;
	}

	public void setSourceFileProducer(SourceDataProducer sourceFileProducer) {
		this.sourceFileProducer = sourceFileProducer;
	}
	
	
	public SourceTableDao getSourceTableDao() {
		return sourceTableDao;
	}
	
	public void setSourceTableDao(SourceTableDao sourceTableDao) {
		this.sourceTableDao = sourceTableDao;
	}
	
	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public HRBPUserDao getHrbpUserDao() {
		return hrbpUserDao;
	}

	public void setHrbpUserDao(HRBPUserDao hrbpUserDao) {
		this.hrbpUserDao = hrbpUserDao;
	}

	public SourceTableProducer getSourceTableProducer() {
		return sourceTableProducer;
	}

	public void setSourceTableProducer(SourceTableProducer sourceTableProducer) {
		this.sourceTableProducer = sourceTableProducer;
	}

	public MappedRowConsumer getMappedRowsConsumer() {
		return mappedRowsConsumer;
	}

	public void setMappedRowsConsumer(MappedRowConsumer mappedRowsConsumer) {
		this.mappedRowsConsumer = mappedRowsConsumer;
	}

	/*
	public SourceTableReader getSourceTableReader() {
		return sourceTableReader;
	}

	public void setSourceTableReader(SourceTableReader sourceTableReader) {
		this.sourceTableReader = sourceTableReader;
	}*/

	public BlockingQueue<SourceRow> getSourceRowsQueue() {
		return sourceRowsQueue;
	}

	public void setSourceRowsQueue(BlockingQueue<SourceRow> sourceRowsQueue) {
		this.sourceRowsQueue = sourceRowsQueue;
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}


	public FTPUploader getSourceDownloader() {
		return sourceDownloader;
	}


	public void setSourceDownloader(FTPUploader sourceDownloader) {
		this.sourceDownloader = sourceDownloader;
	}


	public FTPUploader getResultUploader() {
		return resultUploader;
	}


	public void setResultUploader(FTPUploader resultUploader) {
		this.resultUploader = resultUploader;
	}

	
	 
	 //private String stripFileExtension(String fileName) {
	//	 int dotInd = fileName.lastIndexOf('.');
	 //    return (dotInd > 0) ? fileName.substring(0, dotInd) : fileName;
	 //}
	
	 
	/*
	 private void backupSourceFiles() throws Exception 
	 {
		 String localSrcPath = configParams.getStrSourceLocalDir();
		 
		 String sourceFileName= "";
		 
		 for (String  srcFileName: configParams.getSourceFilesList() ){	
			 try {
				 sourceFileName = srcFileName; 
				 backupFile(localSrcPath + "\\" + srcFileName);
				 
			 } catch (Exception e){
				 if (StringUtils.equalsIgnoreCase(sourceFileName, configParams.getStrPrimarySourceFileName())){
					 throw e;
				 } else {
					 logger.info("Failed to backup non-primary source file: " + sourceFileName);
				 }
			 }
		 }
	 }*/
		 
	 
	/*
	private void backupFile(String strSourceFilePath) throws Exception {
		
		byte[] buf = new byte[1024];
		
		File file = new File(strSourceFilePath);
		
		if (!file.exists()){
			return;
		}
				
		FileInputStream in = new FileInputStream(file);
				
		String fileName = stripFileExtension(file.getName());
		
		String dir = file.getParent();
		
		String zipPattern = "'" + fileName + "_'dd-MM-yyyy__HH_mm_ss'.zip'";
		
		SimpleDateFormat sdf =	new SimpleDateFormat(zipPattern);
		
		String zipFileName = sdf.format(new Date());
				  
		 // Create the ZIP file
		
        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(dir + "\\" + zipFileName));
    
        // Compress the files
        // Add ZIP entry to output stream.
        out.putNextEntry(new ZipEntry(file.getName()));

        // Transfer bytes from the file to the ZIP file
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }

        // Complete the entry
        out.closeEntry();
        in.close();        
    
        // Complete the ZIP file
        out.close();
		
        //delete the origin
        file.delete();
	}*/
	
	 
	/*
	private void moveProcessedSapFiles(){
		if (reportDataBean.getFailedFastException() == null ){
			
			
			String processedSapFilePath = "";
			String sourceFileName = "";
			try {
				logger.info("Starting moving of source files to processed dir ...");
				
				for(String srcFileName : configParams.getSourceFilesListToMove()){
					
					
					SimpleDateFormat sdf =	new SimpleDateFormat("'_'dd-MM-yyyy__HH_mm_ss'.csv'");					
					processedSapFilePath = configParams.getStrProcessedSapFileFtpDir() + "/" 
					+ configParams.getFileNameWithoutExt(srcFileName)									
					+ sdf.format(new Date());
					
					String sapFilePath = configParams.getStrSourceRemoteDir() + "/" + srcFileName; 
					sourceFileName = srcFileName;
					try {					
						resultUploader.moveFile(sapFilePath, processedSapFilePath);					
					} catch (Exception e){
						if (StringUtils.equalsIgnoreCase(sourceFileName, configParams.getStrPrimarySourceFileName())){
							 throw e;
						} else {
							 logger.info("Failed to move non-primary source file: " + sourceFileName);
						}
					}
				}
				logger.info("Sources file moved.");
			} catch (Exception e) {
				logger.info("Failed to move primary sap source file to: " + processedSapFilePath ,e);					
			}
		}
	}*/


	public SourceDataProducer getAdditionalSourceFileProducer() {
		return additionalSourceFileProducer;
	}


	public void setAdditionalSourceFileProducer(
			SourceDataProducer additionalSourceFileProducer) {
		this.additionalSourceFileProducer = additionalSourceFileProducer;
	}


	public BlockingQueue<MappedRow> getMappedRowsQueue() {
		return mappedRowsQueue;
	}


	public void setMappedRowsQueue(BlockingQueue<MappedRow> mappedRowsQueue) {
		this.mappedRowsQueue = mappedRowsQueue;
	}


	public DictionaryDao getDictionaryDao() {
		return dictionaryDao;
	}


	public void setDictionaryDao(DictionaryDao dictionaryDao) {
		this.dictionaryDao = dictionaryDao;
	}


	public SourceFileProcessor getSourceFileProcessor() {
		return sourceFileProcessor;
	}


	public void setSourceFileProcessor(SourceFileProcessor sourceFileProcessor) {
		this.sourceFileProcessor = sourceFileProcessor;
	}


	public PlateauAttrLogger getPlateauAttrLogger() {
		return plateauAttrLogger;
	}

	public void setPlateauAttrLogger(PlateauAttrLogger plateauAttrLogger) {
		this.plateauAttrLogger = plateauAttrLogger;
	}


	public ExpatUserDao getExpatUserDao() {
		return expatUserDao;
	}


	public void setExpatUserDao(ExpatUserDao expatUserDao) {
		this.expatUserDao = expatUserDao;
	}


	public LoadOrgLstTableStep getLoadOrgLstTableStep() {
		return loadOrgLstTableStep;
	}


	public void setLoadOrgLstTableStep(LoadOrgLstTableStep loadOrgLstTableStep) {
		this.loadOrgLstTableStep = loadOrgLstTableStep;
	}

	/*
	public ExclusionProcessor getExclusionProcessor() {
		return exclusionProcessor;
	}

	public void setExclusionProcessor(ExclusionProcessor exclusionProcessor) {
		this.exclusionProcessor = exclusionProcessor;
	}*/
	
	public SourceDataProducer getExternalUsersFileProducer() {
		return externalUsersFileProducer;
	}

	public void setExternalUsersFileProducer(
			SourceDataProducer externalUsersFileProducer) {
		this.externalUsersFileProducer = externalUsersFileProducer;
	}

	public SourceFileUtil getSourceFileUtil() {
		return sourceFileUtil;
	}

	public void setSourceFileUtil(SourceFileUtil sourceFileUtil) {
		this.sourceFileUtil = sourceFileUtil;
	}

	public FTPUploader getExternalUserSourceDownloader() {
		return externalUserSourceDownloader;
	}

	public void setExternalUserSourceDownloader(
			FTPUploader externalUserSourceDownloader) {
		this.externalUserSourceDownloader = externalUserSourceDownloader;
	}

	
	public ExternalUserProcessor getExternalUserProcessor() {
		return externalUserProcessor;
	}

	public void setExternalUserProcessor(ExternalUserProcessor externalUserProcessor) {
		this.externalUserProcessor = externalUserProcessor;
	}


	public AdditionalProcessor getAdditionalProcessor() {
		return additionalProcessor;
	}


	public void setAdditionalProcessor(AdditionalProcessor additionalProcessor) {
		this.additionalProcessor = additionalProcessor;
	}


	public SourceDataProducer getSourceManualDataProducer() {
		return sourceManualDataProducer;
	}


	public void setSourceManualDataProducer(
			SourceDataProducer sourceManualDataProducer) {
		this.sourceManualDataProducer = sourceManualDataProducer;
	}


	public AdditionalProcessor getAdditionalProcessor2() {
		return additionalProcessor2;
	}


	public void setAdditionalProcessor2(AdditionalProcessor additionalProcessor2) {
		this.additionalProcessor2 = additionalProcessor2;
	}


	public AdditionalProcessor getAdditionalProcessor3() {
		return additionalProcessor3;
	}


	public void setAdditionalProcessor3(AdditionalProcessor additionalProcessor3) {
		this.additionalProcessor3 = additionalProcessor3;
	}
	
	
}
