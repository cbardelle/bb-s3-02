package eu.unicreditgroup.batch;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.config.SourceFileConf;
import eu.unicreditgroup.filter.AustrianUsersProcessor;
import eu.unicreditgroup.filter.ExcludeGermanManagersFilter;
import eu.unicreditgroup.filter.GermanUsersProcessor;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.mapper.HRBPUserProcessor;
import eu.unicreditgroup.utils.CsvReader;
import eu.unicreditgroup.utils.SAPDataProcessor;
import eu.unicreditgroup.utils.SourceRowQueueUtil;
import eu.unicreditgroup.utils.reader.FlatFileReader;
import eu.unicreditgroup.utils.reader.SrcFileNameProvider;

/**
 * This class is responsible for reading data rows from the source file. Each
 * row is parsed and put into the sourceUsersQueue. (When sourceUsersQueue is
 * full the put method will wait until the queue will be emptied by consumer
 * threads.) When EOF is reached a false user is put to the queue to stop
 * processing.
 * 
 * 
 * @author UV00074
 * 
 */
public class SourceFileProducer implements Runnable, SourceDataProducer {

	private static final Logger logger = Logger
			.getLogger(SourceFileProducer.class);

	private ConfigParams configParams;

	private SAPDataProcessor sapDataProcessor;

	private BlockingQueue<SourceRow> sourceRowsQueue;

	private BlockingQueue<Exception> exceptionsQueue;

	private ReportDataBean reportDataBean;

	private HRBPUserProcessor hrbpUserProcessor;

	//private final char CSV_COL_DELIMITER = ';';

	//private SourceColumn srcResume;
	
	private SourceFileConf sourceFileConf;	

	// private String additionalSourceFileName;

	private boolean primarySourceFileProducer = false;
	
	private FlatFileReader flatFileReader;
	
	
	private SrcFileNameProvider  srcFileNameProvider;

	/* (non-Javadoc)
	 * @see eu.unicreditgroup.batch.SourceDataProducer#getSourceFilesAsString()
	 */
	@Override
	public String getSourceFilesAsString() {
		
		
		return StringUtils.join(srcFileNameProvider.getSourceFilesList(), ",");
		
		/*
		if (isPrimarySourceFileProducer()) {
			return StringUtils.join(configParams.getSourceFilesList(), ",");
		} else {
			return configParams.getStrAdditionalSourceFileName();
		}*/
	}

	private boolean isPrimarySrcFile(String fileName) {
		
		/*
		return isPrimarySourceFileProducer()
				&& StringUtils.equalsIgnoreCase(fileName, configParams
						.getStrPrimarySourceFileName());
		*/
		
		if ( !isPrimarySourceFileProducer() ) {
			
			return false;
			
		} else {
			
			return configParams.getMandatorySourceFilesList().contains( fileName.toLowerCase() );			
			
		}
		

	}

	@Override
	public void run() {

		FlatFileReader reader = null;
		String srcFileName = "";
		String sourceLocalDir = configParams.getStrSourceLocalDir();

		try {

			// reader = new CsvReader(configParams.getStrSourceLocalFilePath());
			//ArrayList<String> sourceFilesList = new ArrayList<String>();

			ArrayList<String> sourceFilesList = srcFileNameProvider.getSourceFilesList();
			
			/*
			if (isPrimarySourceFileProducer()) {
				// for primary source file producer will be used the list of
				// source files
				sourceFilesList.addAll(configParams.getSourceFilesList());

			} else {
				// for additional source file the additional file will be used
				sourceFilesList.add(configParams.getStrAdditionalSourceFileName());
			}*/
			
						
			

			for (String sourceFileName : sourceFilesList) {

				try {
					String sourceFileLocalPath = sourceLocalDir + "\\"
							+ sourceFileName;
					srcFileName = sourceFileName;

					File checkFile = new File(sourceFileLocalPath);

					if (checkFile.exists() == false) {
						if (isPrimarySrcFile(srcFileName)) {
							throw new Exception("The primary sorce file " + srcFileName + " doesn't exist!");
						} else {
							logger.error("The non-primary sorce file " + srcFileName + " doesn't exist.");
							continue;
						}
					}

					if (checkFile.length() == 0) {
						if (isPrimarySrcFile(srcFileName)) {
							throw new Exception("The primary sorce file " + srcFileName + " is of zero length!");
						} else {
							logger.error("The non-primary sorce file " + srcFileName + " is of zero length.");
							
							try{
							checkFile.delete();
							} catch (Exception e){
								logger.error("Failed to delete empty source file " +  srcFileName, e);
							}
							continue;
						}
					}

					//reader = new CsvReader(sourceFileLocalPath);
					reader = flatFileReader.initReader(sourceFileLocalPath);					
					
					logger.info("PROCESSING SOURCE FILE: " + sourceFileName);

					//reader.setDelimiter(CSV_COL_DELIMITER);

					//while (reader.readRecord()) {
					while (reader.readNextRow()) {
						String[] sourceDataRow = null;
						//sourceDataRow = reader.getValues();
						sourceDataRow = reader.getColumnsValues();
						
						// we are going to include the Germans too 
						//if( ExcludeGermanManagersFilter.isGermanManagerSourceFile(sourceFileName) == false ){
						
						if ( AustrianUsersProcessor.isAustrianUsersSourceFile( sourceFileName ) == false &&
								GermanUsersProcessor.isGermanUsersSourceFile( sourceFileName ) == false ){
												
							reportDataBean.incSourceFileRowsCount();
						
						}
							
						//}
						
						
						SourceRow sourceRow = sapDataProcessor
								.processSourceData(sourceDataRow);
						
						// set source file name for the purpose of 
						// filtering out data from selected files 
						sourceRow.setSourceFileName(sourceFileName);

						if (sourceRow.getValid() == false) {
							// invalid rows will be rejected e.g. wrong number
							// of cols or no userId
							
							// we are going to include the Germans too
							//if( ExcludeGermanManagersFilter.isGermanManagerSourceFile(sourceFileName) == false ){
							
							logger.error("Source row nr : "
								+ reportDataBean.getSourceFileRowsCount()
								+ " is invalid, employee id : "
								+ sourceRow.getValue(sourceFileConf.getSrcResume().getSrcColIndex()));
						
							reportDataBean.incSourceFileInvalidRowsCount();
							
							//}
						} else {
							sourceRowsQueue.put(sourceRow);
							// HRBP users won't be identified by the column HRBP
							// == Y/N
							/*
							 * // process HRBP partner if
							 * (hrbpUserProcessor.isHBRPUser(sourceRow)){
							 * reportDataBean.incSourceFileHRBPUsersCount();
							 * hrbpUserProcessor.process(sourceRow); }
							 */
						}
					}
					
					// if the file has been successfully red add to the list					
					reportDataBean.addProcessedSourceFiles(srcFileName);

				} catch (Exception e) {
					// handle the io exceptions of non primary source files
					if (isPrimarySrcFile(srcFileName)) {
						throw (e);
					} else {
						logger.error("Exception when reading non-primary source file", e);
					}
				}
			}
			
		} catch (Exception e) {

			if (isPrimarySrcFile(srcFileName)) {
				reportDataBean.setFailedFastException(e);
				logger.error("Exception when reading primary source file!", e);
			} else {
				logger.error("Exception when reading source file", e);
			}

			exceptionsQueue.add(e);

			// exception at this point will be fatal exception and result file
			// will be not generated

			// if (isPrimarySourceFile() &&
			// StringUtils.equalsIgnoreCase(srcFileName,
			// configParams.getStrPrimarySourceFileName())){

			// }

		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
				// put end of the queue marker
				sourceRowsQueue.put(SourceRowQueueUtil.generateShutdownRow());

			} catch (Exception e) {
				// logger.error("Failed to close file : " +
				// configParams.getStrSourceLocalFilePath());
				logger.error("Failed to close file : " + srcFileName);

			}
		}
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public BlockingQueue<SourceRow> getSourceRowsQueue() {
		return sourceRowsQueue;
	}

	public void setSourceRowsQueue(BlockingQueue<SourceRow> sourceRowsQueue) {
		this.sourceRowsQueue = sourceRowsQueue;
	}

	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public HRBPUserProcessor getHrbpUserProcessor() {
		return hrbpUserProcessor;
	}

	public void setHrbpUserProcessor(HRBPUserProcessor hrbpUserProcessor) {
		this.hrbpUserProcessor = hrbpUserProcessor;
	}

	public SAPDataProcessor getSapDataProcessor() {
		return sapDataProcessor;
	}

	public void setSapDataProcessor(SAPDataProcessor sapDataProcessor) {
		this.sapDataProcessor = sapDataProcessor;
	}

	/*
	 * public String getSourceLocalDir() { return sourceLocalDir; }
	 * 
	 * public void setSourceLocalDir(String sourceLocalDir) {
	 * this.sourceLocalDir = sourceLocalDir; }
	 */

	/*
	 * public String getSourceFileName() { return sourceFileName; }
	 * 
	 * public void setSourceFileName(String sourceFileName) {
	 * this.sourceFileName = sourceFileName; }
	 */

	
	public boolean isPrimarySourceFileProducer() {
		return primarySourceFileProducer;
	}

	public boolean getPrimarySourceFileProducer() {
		return primarySourceFileProducer;
	}

	public void setPrimarySourceFileProducer(boolean primarySourceFileProducer) {
		this.primarySourceFileProducer = primarySourceFileProducer;
	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}

	public FlatFileReader getFlatFileReader() {
		return flatFileReader;
	}

	public void setFlatFileReader(FlatFileReader flatFileReader) {
		this.flatFileReader = flatFileReader;
	}

	public SrcFileNameProvider getSrcFileNameProvider() {
		return srcFileNameProvider;
	}

	public void setSrcFileNameProvider(SrcFileNameProvider srcFileNameProvider) {
		this.srcFileNameProvider = srcFileNameProvider;
	}

	
	
}
