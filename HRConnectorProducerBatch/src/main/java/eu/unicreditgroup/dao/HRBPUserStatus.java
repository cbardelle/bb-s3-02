//GIT01
package eu.unicreditgroup.dao;

public class HRBPUserStatus {
	
	/**
	 * New HRBP user with *not* empty users list 
	 */
	public final static String NEW_HRBP_USER = "NEW";
	
	/**
	 * New HRBP user with empty users list
	 */
	public final static String NEW_HRBP_USER_EMPTY_DOMAIN = "NEW_WITH_EMPTY_DOMAIN";
		
	
	/**
	 * Existing HRBP user with filled domain
	 * (a domain which was empty in the past and has been filled.) 
	 */
	public final static String HRBP_USER_FILLED_DOMAIN = "FILLED_DOMAIN";
		
	
	/**
	 * Removed HRBP user
	 */
	public final static String REMOVED_HRBP_USER = "REMOVED";
	

}
