package eu.unicreditgroup.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;


public class ManualSourceTableDao  {
	
	
	SourceFileConf sourceFileConf;	
	
	private SimpleJdbcTemplate simpleJdbcTemplate;
	
	private DataSource dataSource;

	
	private class SourceRowMapper implements RowMapper<SourceRow> {
		@Override
		public SourceRow mapRow(ResultSet rs, int rowNum) throws SQLException {
			SourceRow srcRow = new SourceRow();
			
			int sourceColsCount = sourceFileConf.getSourceColumns().size(); 
					
			String[] row = new String[sourceColsCount];					
			
			for ( int i = 0; i < sourceColsCount ; i++ ){
				row[i] = rs.getString(i+1);
			}
			srcRow.setData(row);			
			return srcRow;
		}
	}

	
	public List<SourceRow> getUsersToSend(){
		
		String sql = "SELECT * FROM HR_CONN_MNL_SRC WHERE SEND_STATUS = 'TO_SEND'";
		
		return simpleJdbcTemplate.query(sql, new SourceRowMapper());
	
	}
	

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}


	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}

	
	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        this.dataSource = dataSource;
    }


	/**
	 * Update the rows which were sent to the LMS
	 * (at least we hope that were)
	 * 
	 */
	public int updateSentRows() {
		String sql = " UPDATE HR_CONN_MNL_SRC SET SEND_STATUS='SENT', LAST_SEND_DATE=SYSDATE WHERE SEND_STATUS = 'TO_SEND'";
		
		return simpleJdbcTemplate.update(sql);
				
	}	
	
	
}
