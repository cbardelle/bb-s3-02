package eu.unicreditgroup.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.SourceFileConf;

public class SourceTableReader {
	
	private static final Logger logger = Logger.getLogger(SourceTableReader.class);
	
	SourceFileConf sourceFileConf;	
		
	private class SourceRowMapper implements RowMapper<SourceRow> {
		@Override
		public SourceRow mapRow(ResultSet rs, int rowNum) throws SQLException {
			SourceRow srcRow = new SourceRow();
			
			int sourceColsCount = sourceFileConf.getSourceColumns().size(); 
					
			String[] row = new String[sourceColsCount];					
			
			for ( int i = 0; i < sourceColsCount ; i++ ){
				row[i] = rs.getString(i+1);
			}
			srcRow.setData(row);			
			return srcRow;
		}
	}
		
	private SimpleJdbcTemplate simpleJdbcTemplate;
		
	private DataSource dataSource;
	
	private boolean runOnce = true;

	private static int bunchSize = 1000;
	
	private static int bunchRowMin = 1;
	
	private static int bunchRowMax;
	
	//private AtomicBoolean atomicEmpty = new AtomicBoolean(false);
	
	private static  AtomicBoolean atomicEmpty = new AtomicBoolean(false);
	private static  boolean empty = false;
		
	
	// count of all rows in a table
	private int rowsCount = -1;
	
	private String SQL_ROWS_COUNT = "SELECT COUNT(*) FROM hr_conn_sap_src";
	
	private String SQL_GET_NEXT_BUNCH = 
	"	SELECT * FROM															" +	
	"	  ( SELECT /*+ FIRST_ROWS(%d) */ src.*, ROWNUM rnum  FROM	" +
	"	    ( SELECT * FROM hr_conn_sap_src ORDER BY user_id ) src				" +
	"	    WHERE ROWNUM <= %d  )								" +
	"	WHERE rnum >= %d											";
	
	
	public synchronized String getNextBunchSql(){

		bunchRowMax = bunchRowMin + bunchSize - 1;        
        if (bunchRowMax > getRowsCount()) {            
            bunchRowMax = getRowsCount();        }        
                
        logger.debug(String.format("Getting rows %d to %d", bunchRowMin , bunchRowMax ));                
        String sql = String.format(SQL_GET_NEXT_BUNCH, bunchSize,  bunchRowMax , bunchRowMin);        
		bunchRowMin += bunchSize;		
		if ( bunchRowMin > getRowsCount() ) {			
			
			atomicEmpty.set(true);
			empty = true;
		}					

		return sql;
	}
	
	public List<SourceRow> getNextBunch(){
		if ( atomicEmpty.get() == true ) {			
			return null;
		}
		
		return simpleJdbcTemplate.query(getNextBunchSql(), new SourceRowMapper());
	
	}

	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        this.dataSource = dataSource;
    }	

	private int getRowsCount(){
		if (rowsCount < 0){			
			rowsCount = simpleJdbcTemplate.queryForInt(SQL_ROWS_COUNT);			 
		}		
		return rowsCount;		
	}
			
	/*
	public int getSourceColCount() {
		return sourceColCount;
	}

	public void setSourceColCount(int sourceColCount) {
		this.sourceColCount = sourceColCount;
	}
	*/

	public long getBunchSize() {
		return bunchSize;
	}

	public void setBunchSize(int bunchSize) {
		this.bunchSize = bunchSize;
	}
	
	public void setEmpty(boolean empty) {
		this.empty = empty;
	}

	public boolean isEmpty() {
		//return atomicEmpty.get();
		return empty;
	}

	public SourceFileConf getSourceFileConf() {
		return sourceFileConf;
	}

	public void setSourceFileConf(SourceFileConf sourceFileConf) {
		this.sourceFileConf = sourceFileConf;
	}

		
}
