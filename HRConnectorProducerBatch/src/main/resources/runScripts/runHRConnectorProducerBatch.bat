rem -----------------------
rem Prepare date and time variables
For /f "tokens=1-7 delims=:/-, " %%i in ('echo exit^|cmd /q /k"prompt $D $T"') do (
	For /f "tokens=2-4 delims=/-,() skip=1" %%a in ('echo.^|date') do (
		set dow=%%i
		set %%a=%%j
		set %%b=%%k
		set %%c=%%l
		set hh=%%m
		set min=%%n
		set ss=%%o
	)
)

for %%i in (dow dd mm yy hh min ss) do set %%i
set seconds=%ss:~0,2%
set name=%yy%-%mm%-%dd%__%hh%_%min%_%seconds%
rem -----------------------

java -jar ${pom.build.finalName}.jar > "sysouts\sysout_%name%.txt" 2>&1



