<html>

<head>
	
	<style>
		body {
			font-family: verdana, sans-serif;
			font-size: 10pt;
			padding: 6px;
		}
		
		.headInfoDoneWithoutErrors {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #3db00b;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		.headInfoDoneWithErrors {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #FF8C00;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		.headInfoFailedFast {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #d8001d;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		
		
		
		.resultTable {
			width: 100%;
			border: 1px #6a8546 solid;
			text-align: center;
		}
		
		.resultTableName {
			background-color: #6a8546;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
			padding: 3px;
			width: 280px;
			margin-top: 10px;
		}
		
		.resultName {
			font-weight: bolder;
			width: 300px;
			text-align: left;
			vertical-align: top;
			font-size: 10pt;
		}
		
		.resultValue {
			text-align: left;
			padding-left: 5px;
			font-size: 10pt;
		}
		
		.errorValue {
			color: #d8001d;
			font-weight: bolder;
		}

		.warningValue {
			color: #ed872c;
			font-weight: bolder;
		}

	</style>

</head>

<body>
	
	<#-- General exit code  -->
	<#if (rb.failedFastException??)>
		<div class="headInfoFailedFast"> 
		 	Batch failed due to fatal exception: ${rb.failedFastExceptionMsg}.
		</div>
	<#else>	
		<#if (rb.exceptionsQueue?size == 0)>
			<div class="headInfoDoneWithoutErrors"> 
				&radic; Batch finished successfully.
			</div>				
		<#else>	
			<div class="headInfoDoneWithErrors"> 
				Batch finished but there were exceptions during execution of the batch.
			</div>
		</#if>
	</#if>
	
	
	<#-- Generic execution info  -->
	<div class="resultTableName">Execution</div>
	<table class="resultTable">
		<tr>
			<td class="resultName">Start</td>
			<td class="resultValue">
				<#if rb.batchStartDate??>
					${rb.batchStartDate?string("yyyy-MM-dd HH:mm:ss")}
				<#else> 
					Undefined 
				</#if>
			</td>
		</tr>
		<tr>
			<td class="resultName">End</td>
			<td  class="resultValue">
				<#if rb.batchEndDate?? >
					${rb.batchEndDate?string("yyyy-MM-dd HH:mm:ss")}
				<#else> 
					Undefined 
				</#if>
			</td>
		</tr>
	</table>

	<#-- Load org list table -->
	<div class="resultTableName">Load organization list table</div>
	<table class="resultTable">
		<#if (rb.configParams.stepLoadOrgLstTableEnabled == true)>
			<tr>
				<td class="resultName">Number of imported organizations</td>
				<td class="resultValue">
					${rb.processedOrgCount}
				</td>
			</tr>			
		<#else>
			<tr>
				<td class="resultName" colspan="2">Step disabled</td>
			</tr>
		</#if>
	</table>

	<#-- Load source table -->
	<div class="resultTableName">Load source table</div>
	<table class="resultTable">
		<#if (rb.configParams.stepLoadSourceTableEnabled == true)>
			<tr>
				<td class="resultName">Start</td>
				<td class="resultValue">
					<#if rb.stepLoadSourceTableStartDate?? >
						${rb.stepLoadSourceTableStartDate?string("yyyy-MM-dd HH:mm:ss")}
					<#else> 
						Undefined 
					</#if>
				</td>
			</tr>
			<tr>
				<td class="resultName">End</td>				
				<td  class="resultValue">
					<#if rb.stepLoadSourceTableEndDate?? >
						${rb.stepLoadSourceTableEndDate?string("yyyy-MM-dd HH:mm:ss")}
					<#else> 
						Undefined 
					</#if>
				</td>
			</tr>
			
			<tr>
				<td class="resultName">Loaded source file(s)</td>
				<td  class="resultValue">
					${rb.processedSourceFilesList}
				</td>
			</tr>		
			<#if (rb.presentAnagrafeAlleatiSourceFile == false)>
				<tr>
					<td class="resultName">Anagrafe alleati source file</td>
					<td  class="errorValue">The source file for Anagrafe Alleati users is not present!</td>
				</tr>			
			</#if>							
			<tr>
				<td class="resultName">Number of rows in source file(s)</td>
				<td  class="resultValue">
					${rb.sourceFileRowsCount}
				</td>
			</tr>
			
			<tr>
				<td class="resultName">Number of invalid columns in source file(s)</td>
				<td  class="resultValue">
					${rb.sourceFileInvalidColumnsCount}
				</td>
			</tr>
			
			<tr>
				<td class="resultName">Number of rows inserted into the source table</td>
				<td  class="resultValue">
					${rb.mainSourceTableRowsInserted}
				</td>
			</tr>
			
			
			<tr>
				<td class="resultName">Number of rows with duplicated User Id column</td>
				<td  class="resultValue">
					${rb.sourceFileDuplicatedUserIdCount}
				</td>
			</tr>
			
			
			<#--tr>
				<td class="resultName">Number of identified HRBP users in source file</td>
				<td  class="resultValue">
					${rb.sourceFileHRBPUsersCount}
				</td>
			</tr-->
			
			<tr>
				<td class="resultName">Number of valid HRBP users in source file (active and present)</td>
				<td  class="resultValue">
					${rb.validHRBPUsersCount}
				</td>
			</tr>

			<tr>
				<td class="resultName">New HRBP users</td>
				<td  class="resultValue">
					${rb.newHRBPUsersList}
				</td>
			</tr>
			
			<#--tr>
				<td class="resultName">New HRBP users with empty domains</td>
				<td  class="resultValue">
					${rb.newHRBPUsersWithEmptyDomainList}
				</td>
			</tr-->
			
			<tr>
				<td class="resultName">HRBP users whose domains have been filled</td>
				<td  class="resultValue">
					${rb.usersWithFilledDomainList}
				</td>
			</tr>
			
			<tr>
				<td class="resultName">Removed HRBP users</td>
				<td  class="resultValue">
					${rb.removedHRBPUsersList}
				</td>
			</tr>
			
			<tr>
				<td class="resultName">Users with empty Resume column </td>
				<td  class="resultValue">
					${rb.usersWithEmptyResumeList}
				</td>
			</tr>
			
			<tr>
				<td class="resultName">Users with changed UserId</td>
				<td  class="resultValue">
					${rb.usersWithChangedIdList}
				</td>
			</tr>
							
			<td class="resultName">Invalid HRBP users count (inactive or not found)</td>
				<td  class="resultValue">
					${rb.invalidHRBPQueue?size}
				</td>
			</tr>

			
			<tr>
				<td class="resultName">Invalid HRBP users (inactive or not found)</td>
				<td  class="resultValue">
					${rb.invalidHRBPUsersList}
				</td>
			</tr>
			
			<tr>
				<td class="resultName">Number of invalid rows in source file</td>
				<td  class="resultValue">
					${rb.sourceFileInvalidRowsCount}
				</td>
			</tr>	
			
			<tr>
				<td class="resultName">Invalid rows list</td>
				<td  class="resultValue">
					${rb.rejectedRowsList}
				</td>
			</tr>
			<tr>
				<td class="resultName">Number of users with empty email in the source file</td>
				<td  class="resultValue">
					${rb.usersWithEmptyEmailCount}
				</td>
			</tr>
		<#else>		
			<tr>
				<td class="resultName" colspan="2">Step disabled</td>
			</tr>
		</#if>
	</table>
	
	<#if (rb.configParams.stepLoadSourceTableEnabled == true && rb.configParams.processExpatUsers == true)>
		<div class="resultTableName">New outboud expat users and related inbounds (if any)</div>
		<table class="resultTable">		
			<tr>
				<th class="resultName">Outbound expat ID</th>
				<th class="resultName">Related inbound expat ID</th>
			</tr>					
			<#list rb.outboundExpatsUsers as rowb>
				<tr>
					<td class="resultValue">${rowb.expatUserId}</td>			
					<td class="resultValue">${rowb.relatedInboudExpatUserId}</td>			
				</tr>
			</#list>
		</table>			
	</#if>
	
	<#if (rb.configParams.processExternalUsersFileEnabled == true)>
		<div class="resultTableName">Processing of external users (Anagrafe Alleati)</div>
		<table class="resultTable">
			<tr>
				<td class="resultName">Number of invalid rows </td>
				<td  class="resultValue">
					${rb.externalUsersInvalidRowsCount}
				</td>
			</tr>
			<tr>
				<td class="resultName">Number of valid rows </td>
				<td  class="resultValue">
					${rb.externalUsersValidRowsCount}
				</td>
			</tr>
			<tr>
				<td class="resultName">External users to deactivate </td>
				<td  class="resultValue">
					${rb.externalUsersToDeactivateList}
				</td>
			</tr>			
			<tr>
				<td class="resultName">Reactivated external users</td>
				<td  class="resultValue">
					${rb.listOfReactivatedExternalUsers}
				</td>
			</tr>			
			<#if (rb.externalUsersMaxCountExceeded == true) >
				<tr>
					<td class="resultName">Warning</td>
					<td  class="resultValue">The maximium count of rows is exceeded, file was not processed.</td>
				</tr>			
			</#if>
			
		</table>			
	</#if>
		
	<#-- Generate result file -->
	<div class="resultTableName">Generate result file</div>
	<table class="resultTable">
		<#if (rb.configParams.stepGenerateResultFileEnabled == true)>
			<tr>
				<td class="resultName">Start</td>
				<td class="resultValue">
					<#if rb.stepGenerateResultFileStartDate?? >
						${rb.stepGenerateResultFileStartDate?string("yyyy-MM-dd HH:mm:ss")}
					<#else> 
						Undefined 
					</#if>
				</td>
			</tr>
			<tr>
				<td class="resultName">End</td>
				<td  class="resultValue">
					<#if rb.stepGenerateResultFileEndDate?? >
						${rb.stepGenerateResultFileEndDate?string("yyyy-MM-dd HH:mm:ss")}
					<#else> 
						Undefined 
					</#if>
				</td>
			</tr>
			<tr>			
				<td class="resultName">Number of successfully mapped source rows</td>
				<td  class="resultValue">
					${rb.mappedRowsCount}
				</td>
			</tr>
			<tr>			
				<td class="resultName">Number of excluded OUTBOUND AND INBOUND expat users</td>
				<td  class="resultValue">
					${rb.excludedOutboundExpatCount}
				</td>
			</tr>
			
			<tr>			
				<td class="resultName">Number of excluded Austrian users</td>
				<td  class="resultValue">
					${rb.excludedAustrianUsersCount}
				</td>
			</tr>
						
			<tr>			
				<td class="resultName">Number of excluded German users</td>
				<td  class="resultValue">
					${rb.excludedGermanUsersCount}
				</td>
			</tr>
			
						
		<#else>
			<tr>
				<td class="resultName" colspan="2">Step disabled</td>
			</tr>
		</#if>
	</table>

	<#-- Tracking of selected user attributes -->
	<#if (rb.configParams.stepGenerateResultFileEnabled == true)>
		
		<div class="resultTableName">Number of new (selected) attributes in the source file</div>
		<table class="resultTable">		
			<tr>
				<td class="resultName">New attributes count</td>
				<td class="resultValue">${rb.newTrackedAttrCount}</td>				
			</tr>
		</table>
		
		<div class="resultTableName">Changed attributes used in assignment profiles</div>
		<table class="resultTable">		
			<tr>
				<th class="resultName">Plateau attr desc</th>
				<th class="resultName">Attr SAP code</th>
				<th class="resultName">New value</th>
				<th class="resultName">Old value</th>
				<th class="resultName">List of assignment profiles</th>
			</tr>					
			<#list rb.changedTrackedAttrWithAsgnPrfList as rowb>
			<tr>
				<td class="resultValue">${rowb.attrDesc}</td>			
				<td class="resultValue">${rowb.attrCode}</td>			
				<td class="resultValue">${rowb.mappedValue}</td>			
				<td class="resultValue">${rowb.dictValue}</td>			
				<td class="resultValue">${rowb.assgnProfilesAsString}</td>
			</tr>
			</#list>
		</table>
		
		<div class="resultTableName">Changed attributes not used in assignment profile</div>
		<table class="resultTable">		
			<tr>
				<th class="resultName">Plateau attr desc</th>
				<th class="resultName">Attr SAP code</th>
				<th class="resultName">New value</th>
				<th class="resultName">Old value</th>				
			</tr>					
			<#list rb.changedTrackedAttrNoAsgnPrfList as rowb>
			<tr>
				<td class="resultValue">${rowb.attrDesc}</td>			
				<td class="resultValue">${rowb.attrCode}</td>			
				<td class="resultValue">${rowb.mappedValue}</td>			
				<td class="resultValue">${rowb.dictValue}</td>
			</tr>
			</#list>
		</table>
	</#if>

</body>

</html>
