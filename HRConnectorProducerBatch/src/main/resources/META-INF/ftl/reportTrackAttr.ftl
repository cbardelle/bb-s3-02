<html>

<head>
	
	<style>
		body {
			font-family: verdana, sans-serif;
			font-size: 10pt;
			padding: 6px;
		}
		
		.headInfoDoneWithoutErrors {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #3db00b;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		.headInfoDoneWithErrors {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #FF8C00;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		.headInfoFailedFast {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #d8001d;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		
		
		
		.resultTable {
			width: 100%;
			border: 1px #6a8546 solid;
			text-align: center;
		}
		
		.resultTableName {
			background-color: #6a8546;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
			padding: 3px;
			width: 280px;
			margin-top: 10px;
		}
		
		.resultName {
			font-weight: bolder;
			width: 300px;
			text-align: left;
			vertical-align: top;
			font-size: 10pt;
		}
		
		.resultValue {
			text-align: left;
			padding-left: 5px;
			font-size: 10pt;
		}
		
		.errorValue {
			color: #d8001d;
			font-weight: bolder;
		}

		.warningValue {
			color: #ed872c;
			font-weight: bolder;
		}

	</style>

</head>

<body>
	
	<#-- General exit code  -->
	<#if (rb.failedFastException??)>
		<div class="headInfoFailedFast"> 
		 	Batch failed due to fatal exception: ${rb.failedFastExceptionMsg}.
		</div>
	<#else>	
		
		<div class="headInfoDoneWithoutErrors"> 
			&radic; Batch finished successfully.
		</div>
	</#if>
	
	
	<#-- Generic execution info  -->
	<div class="resultTableName">Execution</div>
	<table class="resultTable">
		<tr>
			<td class="resultName">Start</td>
			<td class="resultValue">
				<#if rb.batchStartDate??>
					${rb.batchStartDate?string("yyyy-MM-dd HH:mm:ss")}
				<#else> 
					Undefined 
				</#if>
			</td>
		</tr>
		<tr>
			<td class="resultName">End</td>
			<td  class="resultValue">
				<#if rb.batchEndDate?? >
					${rb.batchEndDate?string("yyyy-MM-dd HH:mm:ss")}
				<#else> 
					Undefined 
				</#if>
			</td>
		</tr>
	</table>

	<#-- Load source table -->
	<div class="resultTableName">Load source table</div>
	<table class="resultTable">
		<#if (rb.configParams.stepLoadSourceTableEnabled == true)>
			<tr>
				<td class="resultName">Start</td>
				<td class="resultValue">
					<#if rb.stepLoadSourceTableStartDate?? >
						${rb.stepLoadSourceTableStartDate?string("yyyy-MM-dd HH:mm:ss")}
					<#else> 
						Undefined 
					</#if>
				</td>
			</tr>
			<tr>
				<td class="resultName">End</td>
				<td  class="resultValue">
					<#if rb.stepLoadSourceTableEndDate?? >
						${rb.stepLoadSourceTableEndDate?string("yyyy-MM-dd HH:mm:ss")}
					<#else> 
						Undefined 
					</#if>
				</td>
			</tr>
			<tr>
				<td class="resultName">Number of rows in source file</td>
				<td  class="resultValue">
					${rb.sourceFileRowsCount}
				</td>
			</tr>	
		<#else>
			<tr>
				<td class="resultName" colspan="2">Step disabled</td>
			</tr>
		</#if>
	</table>
	
	<#-- Generate result file -->
	<div class="resultTableName">Generate result file</div>
	<table class="resultTable">
		<#if (rb.configParams.stepGenerateResultFileEnabled == true)>
			<tr>
				<td class="resultName">Start</td>
				<td class="resultValue">
					<#if rb.stepGenerateResultFileStartDate?? >
						${rb.stepGenerateResultFileStartDate?string("yyyy-MM-dd HH:mm:ss")}
					<#else> 
						Undefined 
					</#if>
				</td>
			</tr>
			<tr>
				<td class="resultName">End</td>
				<td  class="resultValue">
					<#if rb.stepGenerateResultFileEndDate?? >
						${rb.stepGenerateResultFileEndDate?string("yyyy-MM-dd HH:mm:ss")}
					<#else> 
						Undefined 
					</#if>
				</td>
			</tr>
			<tr>
			
				<td class="resultName">Number of successfully mapped source rows</td>
				<td  class="resultValue">
					${rb.mappedRowsCount}
				</td>
			</tr>

		<#else>
			<tr>
				<td class="resultName" colspan="2">Step disabled</td>
			</tr>
		</#if>
	</table>
	
		<#-- Tracking of selected user attributes -->
	<#if (rb.configParams.stepGenerateResultFileEnabled == true)>
		
		<div class="resultTableName">Number of new (selected) attributes in the source file</div>
		<table class="resultTable">		
			<tr>
				<td class="resultName">New attributes count</td>
				<td class="resultValue">${rb.newTrackedAttrCount}</td>				
			</tr>
		</table>
		
		<div class="resultTableName">Changed attributes used in assignment profiles</div>
		<table class="resultTable">		
			<tr>
				<th class="resultName">Plateau attr desc</th>
				<th class="resultName">Attr SAP code</th>
				<th class="resultName">New value</th>
				<th class="resultName">Old value</th>
				<th class="resultName">List of assignment profiles</th>
			</tr>					
			<#list rb.changedTrackedAttrWithAsgnPrfList as rowb>
			<tr>
				<td class="resultValue">${rowb.attrDesc}</td>			
				<td class="resultValue">${rowb.attrCode}</td>			
				<td class="resultValue">${rowb.mappedValue}</td>			
				<td class="resultValue">${rowb.dictValue}</td>			
				<td class="resultValue">${rowb.assgnProfilesAsString}</td>
			</tr>
			</#list>
		</table>
	</#if>
	
</body>

</html>
