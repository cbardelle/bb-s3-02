package pl.ugis.batch.utility;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pl.ugis.batch.bo.Department;
import pl.ugis.batch.bo.Enrollment;
import pl.ugis.batch.bo.User;
import pl.ugis.batch.dao.BatchService;

public class MngmUtilTest {
	Connection conn;
	
	@Before
	public void setUp() throws Exception {
		conn = new DBManager().getConnection();
	}

	@After
	public void tearDown() throws Exception {
		
		if (conn != null)
			conn.close();
	}
	
	
	@Test
	public void testSearchDomainDepartment() {
		String data= "uid=UV00022,ou=UGG7O5,ou=UGG7OG,ou=UGG7,ou=UGG,ou=UGIM,ou=UGI,o=US,dc=unicredito,dc=it";
		
		Enrollment expected = new Enrollment();
		expected.setDISTINGUISHED_NAME(data);
		ArrayList<Department> userDepartmentsEx = new ArrayList<Department>();
		userDepartmentsEx.add(new Department("UGG","Global Enterprise Services","DOMAIN","US00516"));
		userDepartmentsEx.add(new Department("UGG7","Intranet & Knowledge Platform","COMPETENCE AREA","US01541"));
		userDepartmentsEx.add(new Department("UGG7O5","LCC Learning&Training&Internet Appl.","COMPETENCE CENTER","US00520, US01341"));
		userDepartmentsEx.add(new Department("UGG7OG","B2E Onegate&Glob. Intranet Solutions","UNITA'","US00264"));
		userDepartmentsEx.add(new Department("UGI","Chairman UGIS","CHIEF EXECUTIVE OFFICE",null));
		userDepartmentsEx.add(new Department("UGIM","C E O","CHIEF EXECUTIVE OFFICE","UI41292"));
		expected.setUserDepartments( userDepartmentsEx);
		
		
		expected.setDomainCode("UGG");
		expected.setDomainName("Global Enterprise Services");
		expected.setDomainManager("US00516");
		ArrayList<User> managers = new ArrayList<User>();
		managers.add(new User("US00516","Diego Donisi"));
		expected.setDomainManagers(managers);
		
		Enrollment result = new Enrollment();
		result.setDISTINGUISHED_NAME(data);
		
		HashMap<String,Department> allDepartments = null;
		try {
			allDepartments = BatchService.getDepartments(conn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		MngmUtil m = new MngmUtil();
		m.digDomainDepartment(result,allDepartments);
		
		
//		assertEquals(expected.getDomainCode(),result.getDomainCode());
//		assertEquals(expected.getDomainName(),result.getDomainName());
		
//		assertEquals(expected.getDomainManager(),result.getDomainManager());
//		assertEquals(expected.getDomainManagers().get(0).getCommonName(),result.getDomainManagers().get(0).getCommonName());
		
		
		
	}

}
