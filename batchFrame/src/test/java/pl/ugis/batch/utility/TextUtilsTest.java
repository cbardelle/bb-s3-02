package pl.ugis.batch.utility;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class TextUtilsTest {

	@Test
	public void testDigDepartmentsFromDistinguishedName() {
		
		String[] expected ={"UGG7O5","UGG7OG","UGG7","UGG","UGIM","UGI"};
		String[] result= null;
		
		String data= "uid=UV00022,ou=UGG7O5,ou=UGG7OG,ou=UGG7,ou=UGG,ou=UGIM,ou=UGI,o=US,dc=unicredito,dc=it";
		
		
		result =TextUtils.digDepartmentsFromDistinguieshedName(data);
		
		
		
		assertArrayEquals(expected,result);
		
		
		//A
		
	}

}
