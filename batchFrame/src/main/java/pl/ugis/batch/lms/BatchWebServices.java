package pl.ugis.batch.lms;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.output.DOMOutputter;

import pl.ugis.batch.utility.PropertyFileReader;
import pl.ugis.batch.utility.XMLUtils;


public class BatchWebServices {
	
	private static final Logger logger = Logger.getLogger(BatchWebServices.class);
	
	public static URL endpoint;
	
	private static String endUrl = PropertyFileReader.getInstance().getProperty("LMS.webservices.anon.url");
	private static String username = PropertyFileReader.getInstance().getProperty("LMS.webservices.user");
	private static String password = PropertyFileReader.getInstance().getProperty("LMS.webservices.password");
	
	private Call call;
	
	
	public BatchWebServices() throws Exception {
		
		try{
		endpoint = new URL(endUrl);
		Service service = new Service();
		call = (Call) service.createCall();
		call.setTargetEndpointAddress(endpoint);
		
		if (username != null) {
			call.setUsername(username);
			call.setPassword(password);
		}
		
		} catch ( ServiceException se ) {
	        logger.info( "SException:" + se.getMessage() );
		} catch (  MalformedURLException me ) {
            logger.info("MException:" + me.getMessage() );
		}
		
	}
	
	public void SetResult(String Enrollment_oid,String score,String stato){//String StartDate,String endDate) {

        try { 

              int index = 0;

             // StartDate= StartDate.substring(0,10) + "T" + "07:00:00"  + ".000Z";
             // endDate= endDate.substring(0,10) + "T" + "15:00:00"  + ".000Z";
             
              DOMOutputter mDomOut = new DOMOutputter();
              call.setOperationName( new QName( "LMSResultsAPI", "setActivityResults"));
              call.setTimeout( new Integer(30000));
            
              Element activity = new Element("ActivityResults");
              activity.setAttribute("Index", Integer.toString(index) );
              activity.addContent(new Element("CompletionAmount").addContent("1"));
              activity.addContent(new Element("RawScore").addContent(score));
             // activity.addContent(new Element("StartTime").addContent(StartDate));
             // activity.addContent(new Element("EndTime").addContent(endDate));
              
              if(stato!=null && stato.equals("P")){
            	  activity.addContent(new Element("PassFail").addContent("Passed"));
              }else {
            	  activity.addContent(new Element("PassFail").addContent("Failed"));
              }
              org.w3c.dom.Element resultWS = (org.w3c.dom.Element) call.invoke(new Object [] {Enrollment_oid ,mDomOut.output(activity)} );
              logger.info("sent call for "+Enrollment_oid);
              if(resultWS!=null) logger.debug(XMLUtils.serializeDOM(resultWS));
              
              
        } catch ( RemoteException re ) {
        	logger.info( "RException:" + re.getMessage() );
        } catch (  JDOMException je ) {
        	logger.info( "JException:" + je.getMessage() );
        } catch (  IOException ie ) {
        	logger.info( "IException:" + ie.getMessage() );
        } 

        

  }

	
	
	/*public void SetCompletionAmount(String enrollmentId, String result, String completion, String score, String startDate, String endDate) throws Exception {
		logger.debug("function SetCompletionAmount[init]");
		Element mysearch = new Element("ActivityResults");
		mysearch.setAttribute("Index","0");
		
		if (completion.equals("toComplete")) {
			mysearch.addContent(new Element("CompletionAmount").addContent("1.0"));
			logger.debug("Enrolment oid: " + enrollmentId + ", completion amount for 1");
			int timeZone = DateUtility.calcuateTimeZoneOffset(startDate.substring(0,10));
			
			if (timeZone==2) {
				startDate= startDate.substring(0,10) + "T" + "06:30:00"  + ".000Z";
			} else if (timeZone==1) {
				startDate= startDate.substring(0,10) + "T" + "07:30:00"  + ".000Z";
			}
			
			if (timeZone==2) {
				endDate= endDate.substring(0,10) + "T" + "15:00:00"  + ".000Z";
			} else if (timeZone==1) {
				endDate= endDate.substring(0,10) + "T" + "16:00:00"  + ".000Z";
			}
			
			mysearch.addContent(new Element("StartTime").addContent(startDate));
			mysearch.addContent(new Element("EndTime").addContent(endDate));
		} else if (completion.equals("toIncomplete")) {
			mysearch.addContent(new Element("CompletionAmount").addContent("0"));
			logger.debug("Enrolment oid: " + enrollmentId + ", completion amount for 0");
		}
			 
		if (result.equals("Passed") || result.equals("Failed") ||result.equals("") ) {
			logger.debug("RESULT:"+ result+"|"); 
			mysearch.addContent(new Element("PassFail").addContent(result));
			logger.debug("Enrolment oid: " + enrollmentId + ", set result for " + result);
		}	
	
		DOMOutputter mDomOut = new DOMOutputter();
		call.setOperationName(new QName("LMSResultsAPI", "setActivityResults") );
		
		HammeringUtils.hammeringWSInvoke(call, new Object[] {enrollmentId,mDomOut.output(mysearch)}, reportBean);
		
		logger.debug("function SetCompletionAmount[exit]");
	}*/

	

	
	
	

}

