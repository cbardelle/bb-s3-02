package pl.ugis.batch.utility;

import java.util.HashMap;

public class LanguageTranslator {
	
	private static HashMap<String, String> courseType = new HashMap<String, String>();
	private static HashMap<String, String> courseMethod = new HashMap<String, String>();
	
	static
	{
		courseType.put("linguistico", "Language");
		courseType.put("manageriale", "Managerial");
		courseType.put("tecnico", "Technical");
		
		courseMethod.put("aula", "Classroom");
		courseMethod.put("blended", "Blended");
		courseMethod.put("cd-rom", "CD-ROM");
		courseMethod.put("coaching on-line", "Coaching on-line");
		courseMethod.put("insegnante telefonico", "Teacher telephone");
		courseMethod.put("meeting on-line", "Meeting on-line");
		courseMethod.put("meeting telefonico", "Phone meeting");
		courseMethod.put("on-line", "On-line");
		courseMethod.put("testo cartaceo", "Paper text ");
		courseMethod.put("training on the job", "Training on the job");
		courseMethod.put("web esterno", "Web external");
	}
	
	public static String translateMethod(String italianMethod)
	{
		if(italianMethod ==  null) return "";
		return courseType.get(italianMethod.trim().toLowerCase());	
	}
	
	public static String translateCourseType(String italianCourseType)
	{
		if(italianCourseType ==  null) return "";
		return courseMethod.get(italianCourseType.trim().toLowerCase());		
	}

}
