package pl.ugis.batch.utility;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

public class FtpClient {
	
	static Logger log = Logger.getLogger(FtpClient.class);
	
	private org.apache.commons.net.ftp.FTPClient ftpc = null;
	
	String hostName = null;
	String userid = null;
	String pw = null;
	String numMilliSeconds = null;
	String ftpFolder = "";
	
	public FtpClient(String hostName, String userid, String pw, String numMilliSeconds, String ftpFolder) {
		this.hostName = hostName;
		this.userid = userid;
		this.pw = pw;
		this.numMilliSeconds = numMilliSeconds;
		this.ftpFolder = ftpFolder;
	}
	
	
	public void sendFile(File file) throws Exception, IllegalStateException{
		
//		csv file containing step enrolments are copied via ftp 
		try {
		
		  int reply;
		 
		  
		  if(hostName== null || hostName.isEmpty())
			throw new IllegalStateException("Given hostName : '"+hostName+"' is not valid.");
		  
		  if(userid == null || userid.isEmpty() )
			  throw new IllegalStateException("Given userid : '"+userid+"' is not valid.");
		  
		  if(pw== null || pw.isEmpty() )
			  throw new IllegalStateException("Given pw : '"+pw+"' is not valid.");
		  
		  //open the connection
		  log.info("opening the connection to " + hostName + " via ftp");
		  ftpc = new org.apache.commons.net.ftp.FTPClient();
		  ftpc.connect(hostName);
		  log.info("Connected to " + hostName + ".");
		  log.info(ftpc.getReplyString());
		  //login
		  ftpc.login(userid, pw);
		  ftpc.changeWorkingDirectory(ftpFolder);
					
		  // After connection attempt, you should check the reply code to verify
		  // success.
		  reply = ftpc.getReplyCode();

		  if(!org.apache.commons.net.ftp.FTPReply.isPositiveCompletion(reply)) {
			ftpc.disconnect();
			log.error("FTP server refused connection.");
			System.exit(1);
		  }
		  
		  // transfer files
		  log.info("file are being transfered via ftp to "+hostName+"/"+ftpFolder);
		  log.info("Local file:"+file.toString());
		  log.info("Remote file:"+ftpFolder+file.getName());
		  
		  ftpc.setFileType(org.apache.commons.net.ftp.FTPClient.BINARY_FILE_TYPE);
		  
		  BufferedOutputStream bufPut2 =  new BufferedOutputStream(ftpc.storeFileStream(file.getName()) );
		  BufferedInputStream bufIn2 = new BufferedInputStream(new FileInputStream(file));

		  int j2;
		  byte b2[] = new byte[1024];
		  while ((j2 = bufIn2.read(b2)) != -1) {
			  bufPut2.write(b2, 0, j2);
		  }
		  bufIn2.close();
		  bufPut2.flush();
		  bufPut2.close();
		  log.info("file was copied to:"+ftpFolder+file.getName());
		  ftpc.logout();
		} catch(IOException e) {
			log.error("Problem with sending file.",e);
			throw e;
	
		} finally {
		  if(ftpc!=null && ftpc.isConnected()) {
			try {
			  ftpc.disconnect();
			} catch(IOException ioe) {
			  throw ioe;
			}
		  }
		
		}
		
	}
}
