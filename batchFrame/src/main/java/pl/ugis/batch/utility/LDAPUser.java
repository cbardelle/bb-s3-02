package pl.ugis.batch.utility;

public class LDAPUser 
{
	private String uid;
	private String sn;
	private String givenName;
	private String usmanager;
	private String managerName;      //generated
	private String usdepartment;
	private String usdepartmenttype = "";
	private String usismanager;
	private String usgenericqualificationcode;
	private String usorganization;
	private String usorganizationcode;
	private String usfulldepartmentcode;
	private String usdepartmentcode;
	private String usparentdepartmentcode;
	private String mail;
	private String usdepartmenttypecode;
	private String usdepartmenttypemngr;
	private String compAreacode;
	private String compArea;
	private String compAreaMngr;
	private String unitCode;
	private String unit;
	private String unitMngr;
	private String compCenterCode;
	private String compCenter;
	private String compCenterMngr;
	private String usagency;
	private String uscountry;
	
	private boolean managerSet = false;
	private boolean omitUser = false;
	
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getUsmanager() {
		return usmanager;
	}
	public void setUsmanager(String usmanager) {
		this.usmanager = usmanager;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public String getUsdepartment() {
		return usdepartment;
	}
	public void setUsdepartment(String usdepartment) {
		this.usdepartment = usdepartment;
	}
	public String getUsdepartmenttype() {
		return usdepartmenttype;
	}
	public void setUsdepartmenttype(String usdepartmenttype) {
		this.usdepartmenttype = usdepartmenttype;
	}
	public String getUsismanager() {
		return usismanager;
	}
	public void setUsismanager(String usismanager) {
		this.usismanager = usismanager;
	}
	public String getUsgenericqualificationcode() {
		return usgenericqualificationcode;
	}
	public void setUsgenericqualificationcode(String usgenericqualificationcode) {
		this.usgenericqualificationcode = usgenericqualificationcode;
	}
	public String getUsorganization() {
		return usorganization;
	}
	public void setUsorganization(String usorganization) {
		this.usorganization = usorganization;
	}
	public String getUsorganizationcode() {
		return usorganizationcode;
	}
	public void setUsorganizationcode(String usorganizationcode) {
		this.usorganizationcode = usorganizationcode;
	}
	
	public String getUsfulldepartmentcode() {
		return usfulldepartmentcode;
	}
	public void setUsfulldepartmentcode(String usfulldepartmentcode) {
		this.usfulldepartmentcode = usfulldepartmentcode;
	}
	public String getUsdepartmentcode() {
		return usdepartmentcode;
	}
	public void setUsdepartmentcode(String usdepartmentcode) {
		this.usdepartmentcode  = usdepartmentcode;
	}
	
	public String getUsparentdepartmentcode() {
		return usparentdepartmentcode;
	}
	public void setUsparentdepartmentcode(String usparentdepartmentcode) {
		this.usparentdepartmentcode = usparentdepartmentcode;
	}

	public boolean isManagerSet() {
		return managerSet;
	}
	public void setManagerSet(boolean managerSet) {
		this.managerSet = managerSet;
	}
	public boolean isOmitUser() {
		return omitUser;
	}
	public void setOmitUser(boolean omitUser) {
		this.omitUser = omitUser;
	}
	public String getUsdepartmenttypecode() {
		return usdepartmenttypecode;
	}
	public void setUsdepartmenttypecode(String usdepartmenttypecode) {
		this.usdepartmenttypecode = usdepartmenttypecode;
	}
	public String getCompAreacode() {
		return compAreacode;
	}
	public void setCompAreacode(String compAreacode) {
		this.compAreacode = compAreacode;
	}
	public String getCompArea() {
		return compArea;
	}
	public void setCompArea(String compArea) {
		this.compArea = compArea;
	}
	public String getUnitCode() {
		return unitCode;
	}
	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getCompCenterCode() {
		return compCenterCode;
	}
	public void setCompCenterCode(String compCenterCode) {
		this.compCenterCode = compCenterCode;
	}
	public String getCompCenter() {
		return compCenter;
	}
	public void setCompCenter(String compCenter) {
		this.compCenter = compCenter;
	}

	public void setCompAreaMngr(String compAreaMngr) {
		this.compAreaMngr = compAreaMngr;
	}
	public String getCompAreaMngr() {
		return compAreaMngr;
	}
	public void setUnitMngr(String unitMngr) {
		this.unitMngr = unitMngr;
	}
	public String getUnitMngr() {
		return unitMngr;
	}
	public void setCompCenterMngr(String compCenterMngr) {
		this.compCenterMngr = compCenterMngr;
	}
	public String getCompCenterMngr() {
		return compCenterMngr;
	}
	public void setUsdepartmenttypemngr(String usdepartmenttypemngr) {
		this.usdepartmenttypemngr = usdepartmenttypemngr;
	}
	public String getUsdepartmenttypemngr() {
		return usdepartmenttypemngr;
	}
	public void setUsagency(String usagency) {
		this.usagency = usagency;
	}
	public String getUsagency() {
		return usagency;
	}
	public void setUscountry(String uscountry) {
		this.uscountry = uscountry;
	}
	public String getUscountry() {
		return uscountry;
	}
}