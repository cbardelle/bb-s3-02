package pl.ugis.batch.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.mail.ByteArrayDataSource;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;


/**
 * Simplifies configuration for sending email with usage of
 * Jakarta commons-email.
 *
 */
public class EmailSender {
	
	//just for private methods
	private static final int TO  = 1;
	private static final int CC  = 2;
	private static final int BCC = 3;

	
	private static final String EMAIL_LIST_DELIMITERS = " ;,";
	
	private static final String EMAIL_VALIDATION_REGEXP = "[a-zA-Z0-9_\\.\\-\\*\\+]+@[a-zA-Z0-9-]+\\.([a-zA-Z0-9-]+\\.)*[a-zA-Z0-9]{1,4}";
	
	private static Pattern emailValidationPattern;
	static {
		emailValidationPattern = Pattern.compile(EMAIL_VALIDATION_REGEXP);
	}
	
	private String hostName;
	
	private String fromAddress;
	
	private String subject;
	
	private String message;
	
	private Map<String, Object> attachmentsMap;
	
	private Set<String> toAddresses;
	
	private Set<String> ccAddresses;
	
	private Set<String> bccAddresses;

	
	public EmailSender() {
		attachmentsMap = new HashMap<String, Object>();
		toAddresses = new HashSet<String>();
		ccAddresses = new HashSet<String>();
		bccAddresses = new HashSet<String>();
	}
	

	
	/**
	 * Checks if given string contains valid
	 * email address. 
	 * 
	 * @param email
	 * @return
	 */
	public static boolean isValidateEmail(String email){
		Matcher matcher = emailValidationPattern.matcher(email);
		return matcher.matches();
	}

	
	/**
	 * Adds file to be attached in the email.
	 * 
	 * @param filePath path to the file
	 * @param attachmentName name of attachment representing given file
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void addAttachmentFromFile(String filePath, String attachmentName) throws FileNotFoundException, IOException{
		attachmentsMap.put(attachmentName, new ByteArrayDataSource(new FileInputStream(filePath), null));
	}
	
	
	/**
	 * Adds attachment with content from
	 * <code>attachmentContent</code>
	 * and name 
	 * <code>attachmentName</code>
	 * 
	 * @param attachmentContent
	 * @param attachmentName
	 * @throws IOException 
	 */
	public void addAttachmentFromString(String attachmentContent, String attachmentName) throws IOException{
		attachmentsMap.put(attachmentName, new ByteArrayDataSource(attachmentContent, null));
	}
	


	/**
	 * Adds content from stream
	 * <code>dataStream</code>
	 * to be attached in the email under name
	 * <code>attachmentName</code>.
	 * 
	 * @param dataStream
	 * @param attachmentName
	 * @throws IOException
	 */
	public void addAttachmentFromInputStream(InputStream dataStream, String attachmentName) throws IOException{
		attachmentsMap.put(attachmentName, new ByteArrayDataSource(dataStream, null));
	}


	
	/**
	 * Sends email.
	 * 
	 * @throws EmailException
	 */
	public void sendEmail() throws EmailException{
		
		//TODO: add possibility of sending other mail types
		HtmlEmail email = new HtmlEmail();
	
		if(StringUtils.isNotBlank(hostName)){
			email.setHostName(hostName);
		}
		
		if(StringUtils.isNotBlank(fromAddress)){
			email.setFrom(fromAddress);
		}
		
		if(StringUtils.isNotBlank(subject)){
			email.setSubject(subject);
		}
		
		if(StringUtils.isNotBlank(message)){
			email.setHtmlMsg(message);
		}
		
		// TO addresses
		for (Iterator<String> it = toAddresses.iterator(); it.hasNext();) {
			email.addTo(it.next());
		}
		
		// CC addresses
		for (Iterator<String> it = ccAddresses.iterator(); it.hasNext();) {
			email.addCc(it.next());
		}
		
		// BCC addresses
		for (Iterator<String> it = bccAddresses.iterator(); it.hasNext();) {
			email.addBcc(it.next());
		}
		
		// attachments
		Set<Map.Entry<String, Object>> attEntrSet = attachmentsMap.entrySet();
		for (Iterator<Map.Entry<String, Object>> it = attEntrSet.iterator(); it.hasNext();) {
			Map.Entry<String, Object> entry = it.next();
			email.attach((DataSource)entry.getValue(), entry.getKey(), entry.getKey());
		}
		
		email.send();
		
	}
	
	
	
	
	/**
	 * Parses list of email addresses (separated with space, coma or semicolon)
	 * and adds every valid address to "TO" addresses collection.
	 * 
	 * @param toAddrList
	 */
	public void parseToAddresses(String toAddrList) {
		parseAddresses(toAddrList, TO);		
	}



	/**
	 * Parses list of email addresses (separated with space, coma or semicolon)
	 * and adds every valid address to "Cc" addresses collection.
	 * 
	 * @param toAddrList
	 */
	public void parseCcAddresses(String toAddrList) {
		parseAddresses(toAddrList, CC);		
	}



	/**
	 * Parses list of email addresses (separated with space, coma or semicolon)
	 * and adds every valid address to "Bcc" addresses collection.
	 * 
	 * @param toAddrList
	 */
	public void parseBccAddresses(String toAddrList) {
		parseAddresses(toAddrList, BCC);		
	}
	
	
	
	/**
	 * Parses string with list of email addresses.
	 * If extracted string contains a valid email address
	 * it would be added to one of the addresses collection(To, Cc, Bcc).
	 * 
	 * 
	 * @param addrList
	 * @param addrType
	 */
	private void parseAddresses(String addrList, int addrType){
		if (StringUtils.isBlank(addrList)) {
			return;
		}
		
		StringTokenizer st = new StringTokenizer(addrList, EMAIL_LIST_DELIMITERS);
		while (st.hasMoreTokens()) {
			String email = StringUtils.trim(st.nextToken());
			if (isValidateEmail(email)) {
				
				switch (addrType) {
					case TO:
						toAddresses.add(email);
						break;

					case CC:
						ccAddresses.add(email);
						break;

					case BCC:
						bccAddresses.add(email);
						break;
				}
			}
		}
	}
	
	
	/**
	 * Sets TO address to given value. All previously addeed 
	 * addresses will be deleted.
	 * 
	 * @param address
	 */
	public void setToAddress(String address){
		if (isValidateEmail(address)){
			toAddresses.clear();
			toAddresses.add(address);
		} else {
			throw new IllegalStateException("Given email address: '" + address + "' is not valid.");
		}
	}
	
	
	/**
	 * Sets CC address to given value. All previously addeed 
	 * addresses will be deleted.
	 * 
	 * @param address
	 */
	public void setCcAddress(String address){
		if (isValidateEmail(address)){
			ccAddresses.clear();
			ccAddresses.add(address);
		} else {
			throw new IllegalStateException("Given email address: '" + address + "' is not valid.");
		}
	}
	
	
	/**
	 * Sets BCC address to given value. All previously addeed 
	 * addresses will be deleted.
	 * 
	 * @param address
	 */
	public void setBccAddress(String address){
		if (isValidateEmail(address)){
			bccAddresses.clear();
			bccAddresses.add(address);
		} else {
			throw new IllegalStateException("Given email address: '" + address + "' is not valid.");
		}
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}


	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		if (isValidateEmail(fromAddress)){
			this.fromAddress = fromAddress;
		} else {
			throw new IllegalStateException("Given email address: '" + fromAddress + "' is not valid.");
		}
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public Map<String, Object> getAttachmentsMap() {
		return attachmentsMap;
	}

	public void setAttachmentsMap(Map<String, Object> attachmentsMap) {
		this.attachmentsMap = attachmentsMap;
	}

	public Set<String> getToAddresses() {
		return toAddresses;
	}

	public void setToAddresses(Set<String> toAddresses) {
		this.toAddresses = toAddresses;
	}

	public Set<String> getCcAddresses() {
		return ccAddresses;
	}

	public void setCcAddresses(Set<String> ccAddresses) {
		this.ccAddresses = ccAddresses;
	}

	public Set<String> getBccAddresses() {
		return bccAddresses;
	}

	public void setBccAddresses(Set<String> bccAddresses) {
		this.bccAddresses = bccAddresses;
	}
	
	
	
}
