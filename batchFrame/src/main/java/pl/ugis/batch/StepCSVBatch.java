/*
 * Created on Mar 2, 2009
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package pl.ugis.batch;

import it.webegg.util.EncryptGoodbuy;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import pl.ugis.batch.bo.Enrollment;
import pl.ugis.batch.core.Command;
import pl.ugis.batch.dao.BatchService;
import pl.ugis.batch.utility.FtpClient;
import pl.ugis.batch.utility.PropertyFileReader;

public class StepCSVBatch implements Command {

	static private PropertyFileReader pr = PropertyFileReader.getInstance();
	static Logger log = Logger.getLogger(StepCSVBatch.class);
	static SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
	
	
	/* (non-Javadoc)
	 * @see pl.ugis.batch.utility.Command#execute()
	 */
	
	public void execute() throws Exception, IllegalStateException {
		
		try{
		
		
		List<Enrollment> enrollments = BatchService.getStepEnrolments(pr.getProperty("START_DATE"));
		
		Iterator<Enrollment> it = enrollments.iterator();
		
		File file = new File(pr.getProperty("resources-path")+pr.getProperty("fileName_StepCSV")+".csv");
		FileWriter  fileWriter = new FileWriter(file);
		
		String tempString = null;
		String ENROLLDATE = null;
		tempString="Matricola,Course_code,Enrolldate\r\n";
		
		if(it.hasNext()) fileWriter.write(tempString);
		
	    while(it.hasNext()) {
			Enrollment enrolment = (Enrollment)it.next();
			
			
			ENROLLDATE = formater.format(enrolment.getENROLLDATE());
				
			tempString = enrolment.getUSER_ID()+","+enrolment.getCATALOG_CODE()+","+ENROLLDATE+"\r\n";
	 		fileWriter.write(tempString);
		}

		fileWriter.close(); 
		
		String hostName = pr.getProperty("ftpHostname");
		String userId = pr.getProperty("ftpUserid");
		String pw = pr.getProperty("ftpPassword");
		
		if(pw!=null)
			pw = EncryptGoodbuy.dencrypt(pw);
		
		String numMilliSeconds = pr.getProperty("ftpMilliWait");
		String ftpFolder = pr.getProperty("ftpFolder");
		  
		FtpClient ftpc = new FtpClient(hostName,userId,pw,numMilliSeconds,ftpFolder);
		ftpc.sendFile(file);
		
		}catch (IllegalStateException ie) {
			log.error(ie.getMessage(),ie);
			throw ie;
		}
		catch (Exception e) {
			log.error(e.getMessage(),e);
			throw e;
		}
		
		
	}



	
}
