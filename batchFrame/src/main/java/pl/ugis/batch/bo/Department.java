package pl.ugis.batch.bo;

import java.util.ArrayList;

public class Department {

	private String usDepartmentCode;
	private String usDepartment;
	private String usDepartmentType;
	private String usManger;
	private ArrayList<User> managers;
	
	
	public Department(String usDepartmentCode, String usDepartment, String usDepartmentType, String usManger) {
		this.usDepartmentCode = usDepartmentCode;
		this.usDepartment = usDepartment;
		this.usDepartmentType = usDepartmentType;
		this.usManger = usManger;
	}
	
	public String getUsDepartment() {
		return usDepartment;
	}
	public void setUsDepartment(String usDepartment) {
		this.usDepartment = usDepartment;
	}
	public String getUsDepartmentCode() {
		return usDepartmentCode;
	}
	public void setUsDepartmentCode(String usDepartmentCode) {
		this.usDepartmentCode = usDepartmentCode;
	}
	public String getUsDepartmentType() {
		return usDepartmentType;
	}
	public void setUsDepartmentType(String usDepartmentType) {
		this.usDepartmentType = usDepartmentType;
	}
	public String getUsManger() {
		return usManger;
	}
	public void setUsManger(String usManger) {
		this.usManger = usManger;
	}

	public ArrayList<User> getManagers() {
		return managers;
	}

	public void setManagers(ArrayList<User> managers) {
		this.managers = managers;
	}
	
	
}
