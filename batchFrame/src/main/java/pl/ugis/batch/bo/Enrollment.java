/*
 * Created on Jan 3, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package pl.ugis.batch.bo;

import java.sql.Date;
import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class Enrollment {
	
	private String ENROLLMENT_OID = "";
	private String OID ="";
	private String USER_ID = "";
	private String USER_EMAIL = "";
	private String NUM_ID;
	private String CATALOG_CODE = "";
	private Date ENROLLDATE = null;
	private Date REQUEST_DATE = null;
	private String STATUS = "";
	private Date START_DATE = null;
	private Date END_DATE = null;
	private String FIRST_NAME = "";
	private String LAST_NAME = "";
	private String FILIALE = "";
	private String MANAGER_ID = "";
	private String MANAGER_EMAIL = "";
	private String MANAGER_FIRSTNAME = "";
	private String MANAGER_LASTNAME = "";
	private String TYPE = "";
	private String OFFERING_OID = null;
	private String COURSE_TITLE = "";
	private String COURSE_DESCRIPTION = "";
	private String CITY = "";
	private int ALLOW_SELF_ENROLLMENT = 0;
	private String GENERALE = "";
	private String CATALOGENTRY_OID = null;
	private Date SEND_TO_METAMORFOSI = null;
	private double DAYS = 0;
	private String DIDATTICA = "";
	private String CODE_STATE = "";
	private int REQUIRES_MANAGER_APPROVAL = 0;
	private int REQUIRES_APPROVER_APPROVAL = 0;
	private String AREA_TEMATICA_CODE = null;
	
	private String competenceAreaCode = null;
	private String competanceArea = null;
	
	private String unitCode = null;
	private String unit = null;
	
	private String competenceCenterCode = null;
	private String competenceCenter = null;
	private String competenceCenterResp = null;
	
	private String competanceAreaResp = null;
	
	private String unitResp = null;
	private String state = null;
	private String note = null;
	private double realDays = 0;
	private String score = "";
	private String cf = "";
	private int ROWNUM = 0;
	private String DISTINGUISHED_NAME="";
	private ArrayList<Department> userDepartments;
	private String domainCode="";
	private String domainName="";
	private String domainManager= "";
	private String company = "";
	private String requester = "";
	private String requesterName = "";
	private String requesterSName = "";
	private String country = "";
	private String CATEGORY = "";
	private String GROUP = "";
	private String SKILL = "";
	private Date apprRejDate = null;

	private ArrayList<User> domainManagers;

	private CustomField customField = null;
	
	private String requestPriority = "";
	
	public String getNUM_ID() {
		return NUM_ID;
		//return TextUtils.padZeroLeft(NUM_ID,5);
	}

	public void setNUM_ID(String num_id) {
		NUM_ID = num_id;
	}
	
	public String getCATALOG_CODE() {
		return CATALOG_CODE;
	}

	public void setCATALOG_CODE(String catalog_code) {
		CATALOG_CODE = catalog_code;
	}

	public Date getENROLLDATE() {
		return ENROLLDATE;
	}

	public void setENROLLDATE(Date enrolldate) {
		ENROLLDATE = enrolldate;
	}

	public String getFILIALE() {
		return FILIALE;
	}

	public void setFILIALE(String filiale) {
		FILIALE = filiale;
	}

	public String getFIRST_NAME() {
		return FIRST_NAME;
	}

	public void setFIRST_NAME(String first_name) {
		FIRST_NAME = first_name;
	}

	public String getLAST_NAME() {
		return LAST_NAME;
	}

	public void setLAST_NAME(String last_name) {
		LAST_NAME = last_name;
	}

	public String getMANAGER_FIRSTNAME() {
		return MANAGER_FIRSTNAME;
	}

	public void setMANAGER_FIRSTNAME(String manager_firstname) {
		MANAGER_FIRSTNAME = manager_firstname;
	}

	public String getMANAGER_LASTNAME() {
		return MANAGER_LASTNAME;
	}

	public void setMANAGER_LASTNAME(String manager_lastname) {
		MANAGER_LASTNAME = manager_lastname;
	}

	public Date getSTART_DATE() {
		return START_DATE;
	}

	public void setSTART_DATE(Date start_date) {
		START_DATE = start_date;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String status) {
		STATUS = status;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String type) {
		TYPE = type;
	}

	public String getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(String user_id) {
		USER_ID = user_id;
	}

	public CustomField getCustomField() {
		return customField;
	}

	public void setCustomField(CustomField customField) {
		this.customField = customField;
	}

	public String getOFFERING_OID() {
		return OFFERING_OID;
	}

	public void setOFFERING_OID(String offering_oid) {
		OFFERING_OID = offering_oid;
	}

	public String getCOURSE_DESCRIPTION() {
		return COURSE_DESCRIPTION;
	}

	public void setCOURSE_DESCRIPTION(String course_description) {
		COURSE_DESCRIPTION = course_description;
	}

	public String getCOURSE_TITLE() {
		return COURSE_TITLE;
	}

	public void setCOURSE_TITLE(String course_title) {
		COURSE_TITLE = course_title;
	}

	public Date getEND_DATE() {
		return END_DATE;
	}

	public void setEND_DATE(Date end_date) {
		END_DATE = end_date;
	}

	public String getCITY() {
		return CITY;
	}

	public void setCITY(String city) {
		CITY = city;
	}

	public String getMANAGER_ID() {
		return MANAGER_ID;
	}

	public void setMANAGER_ID(String manager_id) {
		MANAGER_ID = manager_id;
	}

	public int getALLOW_SELF_ENROLLMENT() {
		return ALLOW_SELF_ENROLLMENT;
	}

	public void setALLOW_SELF_ENROLLMENT(int allow_self_enrollment) {
		ALLOW_SELF_ENROLLMENT = allow_self_enrollment;
	}

	public String getGENERALE() {
		return GENERALE;
	}

	public void setGENERALE(String generale) {
		GENERALE = generale;
	}

	public String getCATALOGENTRY_OID() {
		return CATALOGENTRY_OID;
	}

	public void setCATALOGENTRY_OID(String catalogentry_oid) {
		CATALOGENTRY_OID = catalogentry_oid;
	}

	public String getCODE_STATE() {
		return CODE_STATE;
	}

	public void setCODE_STATE(String code_state) {
		CODE_STATE = code_state;
	}

	public Date getSEND_TO_METAMORFOSI() {
		return SEND_TO_METAMORFOSI;
	}

	public void setSEND_TO_METAMORFOSI(Date send_to_metamorfosi) {
		SEND_TO_METAMORFOSI = send_to_metamorfosi;
	}

	public double getDAYS() {
		return DAYS;
	}

	public void setDAYS(double days) {
		DAYS = days;
	}

	public String getDIDATTICA() {
		return DIDATTICA;
	}

	public void setDIDATTICA(String didattica) {
		DIDATTICA = didattica;
	}

	public int getREQUIRES_APPROVER_APPROVAL() {
		return REQUIRES_APPROVER_APPROVAL;
	}

	public void setREQUIRES_APPROVER_APPROVAL(int requires_approver_approval) {
		REQUIRES_APPROVER_APPROVAL = requires_approver_approval;
	}

	public int getREQUIRES_MANAGER_APPROVAL() {
		return REQUIRES_MANAGER_APPROVAL;
	}

	public void setREQUIRES_MANAGER_APPROVAL(int requires_manager_approval) {
		REQUIRES_MANAGER_APPROVAL = requires_manager_approval;
	}

	public String getAREA_TEMATICA_CODE() {
		return AREA_TEMATICA_CODE;
	}

	public void setAREA_TEMATICA_CODE(String area_tematica_code) {
		AREA_TEMATICA_CODE = area_tematica_code;
	}



	public String getCompetenceCenter() {
		return competenceCenter;
	}

	public void setCompetenceCenter(String competenceCenter) {
		this.competenceCenter = competenceCenter;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	

	public String getCompetanceAreaResp() {
		return competanceAreaResp;
	}

	public void setCompetanceAreaResp(String competanceAreaResp) {
		this.competanceAreaResp = competanceAreaResp;
	}

	public String getUnitResp() {
		return unitResp;
	}

	public void setUnitResp(String unitResp) {
		this.unitResp = unitResp;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public double getRealDays() {
		return realDays;
	}

	public void setRealDays(double realDays) {
		this.realDays = realDays;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}

	public String getOID() {
		return OID;
	}

	public void setOID(String oid) {
		OID = oid;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getCf() {
		return cf;
	}

	public void setCf(String cf) {
		this.cf = cf;
	}

	public int getROWNUM() {
		return ROWNUM;
	}

	public void setROWNUM(int rownum) {
		ROWNUM = rownum;
	}

	public String getDISTINGUISHED_NAME() {
		return DISTINGUISHED_NAME;
	}

	public void setDISTINGUISHED_NAME(String distinguished_name) {
		DISTINGUISHED_NAME = distinguished_name;
	}

	public String getDomainManager() {
		return domainManager;
	}

	public void setDomainManager(String domainManager) {
		this.domainManager = domainManager;
	}

	public ArrayList<Department> getUserDepartments() {
		return userDepartments;
	}

	public void setUserDepartments(ArrayList<Department> userDepartments) {
		this.userDepartments = userDepartments;
	}

	public String getDomainCode() {
		return domainCode;
	}

	public void setDomainCode(String domainCode) {
		this.domainCode = domainCode;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getCompetenceAreaCode() {
		return competenceAreaCode;
	}

	public void setCompetenceAreaCode(String competenceAreaCode) {
		this.competenceAreaCode = competenceAreaCode;
	}

	public String getCompetenceCenterCode() {
		return competenceCenterCode;
	}

	public void setCompetenceCenterCode(String competenceCenterCode) {
		this.competenceCenterCode = competenceCenterCode;
	}

	public String getUnitCode() {
		return unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getCompetanceArea() {
		return competanceArea;
	}

	public void setCompetanceArea(String competanceArea) {
		this.competanceArea = competanceArea;
	}

	public ArrayList<User> getDomainManagers() {
		return domainManagers;
	}

	public void setDomainManagers(ArrayList<User> domainManagers) {
		this.domainManagers = domainManagers;
	}

	public Date getREQUEST_DATE() {
		return REQUEST_DATE;
	}

	public void setREQUEST_DATE(Date request_date) {
		REQUEST_DATE = request_date;
	}

	public String getENROLLMENT_OID() {
		return ENROLLMENT_OID;
	}

	public void setENROLLMENT_OID(String eNROLLMENTOID) {
		ENROLLMENT_OID = eNROLLMENTOID;
	}

	public String getUSER_EMAIL() {
		return USER_EMAIL;
	}

	public void setUSER_EMAIL(String uSEREMAIL) {
		USER_EMAIL = uSEREMAIL;
	}

	public String getMANAGER_EMAIL() {
		return MANAGER_EMAIL;
	}

	public void setMANAGER_EMAIL(String mANAGEREMAIL) {
		MANAGER_EMAIL = mANAGEREMAIL;
	}

	public void setCompetenceCenterResp(String competenceCenterResp) {
		this.competenceCenterResp = competenceCenterResp;
	}

	public String getCompetenceCenterResp() {
		return competenceCenterResp;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setRequester(String requester) {
		this.requester = requester;
	}

	public String getRequester() {
		return requester;
	}

	public void setRequesterName(String requesterName) {
		this.requesterName = requesterName;
	}

	public String getRequesterName() {
		return requesterName;
	}

	public void setRequesterSName(String requesterSName) {
		this.requesterSName = requesterSName;
	}

	public String getRequesterSName() {
		return requesterSName;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountry() {
		return country;
	}

	public String getRequestPriority() {
		return requestPriority;
	}

	public void setRequestPriority(String requestPriority) {
		this.requestPriority = requestPriority;
	}

	public String getCATEGORY() {
		return CATEGORY;
	}

	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}

	public String getGROUP() {
		return GROUP;
	}

	public void setGROUP(String gROUP) {
		GROUP = gROUP;
	}

	public String getSKILL() {
		return SKILL;
	}

	public void setSKILL(String sKILL) {
		SKILL = sKILL;
	}

	public void setApprRejDate(Date apprRejDate) {
		this.apprRejDate = apprRejDate;
	}

	public Date getApprRejDate() {
		return apprRejDate;
	}

}