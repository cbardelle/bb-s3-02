package eu.unicreditgroup.cn.bizx.mapper;

import eu.unicreditgroup.cn.bizx.bean.MappedColumn;
import eu.unicreditgroup.cn.bizx.bean.SourceRow;

/**
 * Mapping to a fixed value
 * 
 * @author UV00074
 *
 */
public class FixedValueMapper implements IMapper {
		
	@Override
	public MappedColumn map( String[] srcVal, String defaultVal, SourceRow sourceRow) {
		return new MappedColumn(defaultVal);		
	}

}
