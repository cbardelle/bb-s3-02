package eu.unicreditgroup.cn.bizx.mapper;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import eu.unicreditgroup.cn.bizx.bean.MappedColumn;
import eu.unicreditgroup.cn.bizx.bean.MappedRow;
import eu.unicreditgroup.cn.bizx.bean.SourceRow;
import eu.unicreditgroup.cn.bizx.config.BizxColumn;
import eu.unicreditgroup.cn.bizx.config.LmsColumn;
import eu.unicreditgroup.cn.bizx.mapper.exception.InvalidColumnException;
import eu.unicreditgroup.cn.bizx.mapper.exception.InvalidRowException;
import eu.unicreditgroup.cn.bizx.validator.IValidator;
import eu.unicreditgroup.mailer.ReportDataBean;

public class TableMapper implements InitializingBean{

	Logger logger = Logger.getLogger(TableMapper.class);
	
	
	
	private int resultRowSize = 66;  
	
	/**
	 * metadata of all columns of the result table
	 */
	private List<BizxColumn> resultColumns;
	
	private ReportDataBean reportDataBean;
	
	private BlockingQueue<Exception> bizxExceptionsQueue;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		resultRowSize = resultColumns.size();
		if ( resultRowSize <= 0 ){
			throw new Exception("Configuratrion problem: result columns list is empty.");
		}
	}

	public MappedRow processRow(SourceRow sourceRow, String srcUserId){
		
		MappedRow mappedRow = new MappedRow();
		mappedRow.setData(new String[resultRowSize]);
				
		mappedRow.setValid(true);
		
		// map the result row
		int colIdx = 0;
		for(BizxColumn resCol : resultColumns){
			
			IMapper columnMapper = resCol.getMapper();		
			String defaultValue = resCol.getDefaultValue();			
			String mappedValue = null;			
			
			String[] srcValues = getSourceValues(resCol.getSrcCol(), sourceRow); 
					
			MappedColumn mappedCol = null;
			mappedCol = columnMapper.map(srcValues, defaultValue, sourceRow);
				
			if (mappedCol.getErrorMsg() != null) {
				
				// if failed to map mandatory column reject the record
				if (resCol.isMandatory() == true){
					mappedRow.setValid(false);
					
										
					InvalidRowException e = new InvalidRowException( String.format("Failed to map mandatory column %s, User : %s. %s", resCol.getResColName(), sourceRow.getIdOrUserName(), mappedCol.getErrorMsg()) );
					logger.error(e);					
					reportDataBean.addBizExceptionWithLimit(e);
					//do not process rejected row
					break;
				} else {
					// for non mandatory column just report the exception and use its default value
					// reportDataBean.incSourceFileInvalidColumnsCount();
					 
					InvalidColumnException e = new InvalidColumnException(String.format("Failed to map column %s, User : %s. %s.", resCol.getResColName(), sourceRow.getIdOrUserName(), mappedCol.getErrorMsg() ));
					
					logger.error(e);
					reportDataBean.addBizExceptionWithLimit(e);
					mappedValue = defaultValue;						
				}
			} else {
				mappedValue = mappedCol.getValue();
			}
			
			// run rejectable validators
			// for external users (from file source_aaext.csv) there are more mandatory columns (firsName, lastName, email)
			// external users without their mandatory columns will be rejected
			if (runAllValidators(resCol.getRejectableValidators(), mappedValue, srcValues, srcUserId, sourceRow) == false){
				// the row will be rejected
				mappedRow.setValid(false);
				InvalidRowException e = new InvalidRowException( String.format("Failed to map column %s, User : %s. %s", resCol.getResColName(), sourceRow.getIdOrUserName(), "Attribute value is not present in the dictionary!" ));
				logger.error(e);					
				reportDataBean.addBizExceptionWithLimit(e);										
				
			}
			
			
			// run non rejectable validator
			if (runAllValidators(resCol.getValidators(), mappedValue, srcValues, srcUserId, sourceRow) == false){
				mappedValue = defaultValue;
				logger.error("Validation for column " + resCol.getResColName() + " has failed and default value will be used: '" + defaultValue + "'");
				// reportDataBean.incSourceFileInvalidColumnsCount();				
			}
					
			
			if (mappedValue == null){
				mappedValue = "";			
			}			
			
			mappedRow.setValue(colIdx, mappedValue);						
			colIdx++;						
		}
	
		return mappedRow;
	}

	private String[] getSourceValues(List<LmsColumn> lmsCols,
			SourceRow sourceRow) {
		
		if(lmsCols == null || lmsCols.size() == 0){
			return null;
		}		
		String[] sourceValues = new String[lmsCols.size()];
		int i = 0;
		for(LmsColumn lmsCol : lmsCols)
		{	
			String value = sourceRow.getValue(lmsCol.getColIndex());
			sourceValues[i] = value;						
			i++;
		}		
		return sourceValues;		
	}


	public boolean runAllValidators(List<IValidator> validators, String value, String[] srcValues, String srcUserId, SourceRow sourceRow)
	{
		if (validators == null){
			return true;
		}
		boolean validRow = true;
		boolean result;
		for (IValidator validator : validators){
			result = validator.validate(value, srcValues, 0 /* not used */, sourceRow);
			if ( validRow == true && result == false){
				validRow = result;
			}
			
			// do not stop after first failure
			// run all validators to send back information 
			if (result == false){
				Exception e = new Exception("Validator of type " + validator.getClass().getName() + " has failed for value: '" + value + "' and User ID: " + srcUserId );
				logger.error(e);
				bizxExceptionsQueue.add(e);
			}			
		}
		
		return validRow;
	}
	
	
	

	public List<BizxColumn> getResultColumns() {
		return resultColumns;
	}

	public void setResultColumns(List<BizxColumn> resultColumns) {
		this.resultColumns = resultColumns;
	}


	public BlockingQueue<Exception> getBizxExceptionsQueue() {
		return bizxExceptionsQueue;
	}

	public void setBizxExceptionsQueue(BlockingQueue<Exception> bizxExceptionsQueue) {
		this.bizxExceptionsQueue = bizxExceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	
	
}
