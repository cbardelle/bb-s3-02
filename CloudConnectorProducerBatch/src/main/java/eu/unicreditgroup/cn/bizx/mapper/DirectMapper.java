package eu.unicreditgroup.cn.bizx.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizx.bean.MappedColumn;
import eu.unicreditgroup.cn.bizx.bean.SourceRow;


/**
 * Email mapping 
 *  
 * @author UV00074
 *
 */
public class DirectMapper implements IMapper {
	/**
	 * 	The  mapper will return passed values separated by '-'
	 *  or the default value when source table is null or empty 
	 * 
	 */	
	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn(); 
				
		if (srcVal == null || srcVal.length == 0) {
			mappedCol.setValue(defaultVal);
			return mappedCol; 
		} else {			
			StringBuilder sb = new StringBuilder();
			boolean allNull = true;
			for (int i = 0 ; i < srcVal.length ; i++ ){
				if  (StringUtils.isNotEmpty(srcVal[i])){
					allNull = false;
				}
			}
			if ( allNull == true ) {				
				mappedCol.setValue(defaultVal);
				return mappedCol;
			}
			
			
			for (int i = 0 ; i < srcVal.length ; i++ ){			
				sb.append(srcVal[i]);				
				if (i < srcVal.length - 1){				
					sb.append("-");				
				}
			}			
			mappedCol.setValue(sb.toString());
			return mappedCol;
		}
	}
}
