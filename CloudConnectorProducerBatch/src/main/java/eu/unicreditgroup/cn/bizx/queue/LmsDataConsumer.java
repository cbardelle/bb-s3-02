package eu.unicreditgroup.cn.bizx.queue;

import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.cn.bizx.bean.MappedRow;
import eu.unicreditgroup.cn.bizx.bean.SourceRow;
import eu.unicreditgroup.cn.bizx.config.LmsDataConf;
import eu.unicreditgroup.cn.bizx.mapper.TableMapper;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.cn.bizx.queue.SourceRowQueueUtil;

public class LmsDataConsumer implements Runnable{

	
	private ConfigParams configParams;
	
	private BlockingQueue<SourceRow> lmsDataQueue;
	
	private BlockingQueue<MappedRow> mappedRowsQueue;
	
	private BlockingQueue<Exception> bizxExceptionsQueue;
	
	private ReportDataBean reportDataBean;
	
	private int consumerNumber;
	
	private LmsDataConf lmsDataConf; 
	
	private TableMapper tableMapper;
	
	private static final Logger logger = Logger.getLogger(LmsDataConsumer.class);
	
	@Override	
	public void run() {
		
		
		SourceRow sourceRow = new SourceRow();
		try {	
			
			while(true){
				Thread.yield();
				
				sourceRow = lmsDataQueue.take();
				
				if (SourceRowQueueUtil.isShutdownRow(sourceRow)){
					logger.debug(String.format("[Cons %d] Shutdown row was found. Consumer will stop its work.", consumerNumber));
					lmsDataQueue.put(sourceRow);
					return;
				}
				
				MappedRow mappedRow = tableMapper.processRow(sourceRow, sourceRow.getValue(lmsDataConf.getUserId().getColIndex()));
			
				if ( mappedRow.getValid() == true )	{					
					//reportDataBean.incMappedRowsCount();					
					mappedRowsQueue.put(mappedRow);	
					
					
				} else {					
					//reportDataBean.incSourceFileInvalidRowsCount();					
					String errMsg = "Mapped row for userID: " + sourceRow.getValue(lmsDataConf.getUserId().getColIndex()) + " is invalid and won't be written to the result file.";					
					bizxExceptionsQueue.add( new Exception(errMsg) );					
					logger.error( errMsg );					
				}
							
			}
			
		} catch (Exception ex) {
			logger.debug(String.format("[Cons %d] Exception during processing soure row for userID '%s'.", consumerNumber, sourceRow.getValue(lmsDataConf.getUserId().getColIndex())), ex);			
			bizxExceptionsQueue.add(ex);
		} 
		
	}


	public int getConsumerNumber() {
		return consumerNumber;
	}

	public void setConsumerNumber(int consumerNumber) {
		this.consumerNumber = consumerNumber;
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public BlockingQueue<SourceRow> getLmsDataQueue() {
		return lmsDataQueue;
	}

	public void setLmsDataQueue(BlockingQueue<SourceRow> lmsDataQueue) {
		this.lmsDataQueue = lmsDataQueue;
	}



	public BlockingQueue<Exception> getBizxExceptionsQueue() {
		return bizxExceptionsQueue;
	}


	public void setBizxExceptionsQueue(BlockingQueue<Exception> bizxExceptionsQueue) {
		this.bizxExceptionsQueue = bizxExceptionsQueue;
	}


	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	

	public BlockingQueue<MappedRow> getMappedRowsQueue() {
		return mappedRowsQueue;
	}


	public void setMappedRowsQueue(BlockingQueue<MappedRow> mappedRowsQueue) {
		this.mappedRowsQueue = mappedRowsQueue;
	}


	public LmsDataConf getLmsDataConf() {
		return lmsDataConf;
	}

	public void setLmsDataConf(LmsDataConf lmsDataConf) {
		this.lmsDataConf = lmsDataConf;
	}

	public TableMapper getTableMapper() {
		return tableMapper;
	}

	public void setTableMapper(TableMapper tableMapper) {
		this.tableMapper = tableMapper;
	}

	
}
