package eu.unicreditgroup.cn.bizx.mapper;


import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizx.bean.MappedColumn;
import eu.unicreditgroup.cn.bizx.bean.SourceRow;
import eu.unicreditgroup.cn.bizx.dao.Dictionary;


public class BizxRoleMapper implements IMapper {
	
	private Dictionary dictionary;
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		String userId = srcVal[0]; 

		MappedColumn mappedCol = new MappedColumn();
		
		String bizxRole = dictionary.getAdminOrUserRole(userId);
		
		mappedCol.setValue(bizxRole);
		
		return mappedCol;
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
}
