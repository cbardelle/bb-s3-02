package eu.unicreditgroup.cn.bizx.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizx.bean.MappedColumn;
import eu.unicreditgroup.cn.bizx.bean.SourceRow;


/**
 * Email mapping 
 *  
 * @author UV00074
 *
 */
public class GenderMapper implements IMapper {
	
	private final String DEFAULT_GENDER_4_AA = "M";
	
	private final String ANAGRAFE_ALLEATI = "AA";
	
	
	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn();
				
		if ( ANAGRAFE_ALLEATI.equals(sourceRow.getHostLegalEntity()) ){			
			if (srcVal == null || srcVal.length == 0) {				
				mappedCol.setValue(DEFAULT_GENDER_4_AA);				
			} else {
				String gender = srcVal[0];				
				if ( StringUtils.isEmpty(gender) ){
					mappedCol.setValue(DEFAULT_GENDER_4_AA);
				} else {
					mappedCol.setValue(gender);
				}
				
			}
		} else {
			if (srcVal == null || srcVal.length == 0) {
				mappedCol.setValue(defaultVal);
				
			} else {			
				String gender = srcVal[0];
				mappedCol.setValue(gender);				
			}
		}
		return mappedCol;
	}
	
}
