package eu.unicreditgroup.cn.bizx.validator;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizx.bean.SourceRow;

public class NotNullValidator implements IValidator {

	@Override
	public boolean validate(String value, String[] srcValues, Integer resColIndex, SourceRow sourceRow) {
	
		return (StringUtils.isNotEmpty(value));
	}

	@Override
	public boolean isRowRejectable() {
		return false;
	}


}
