package eu.unicreditgroup.cn.bizx.queue;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizx.bean.SourceRow;


public class SourceRowQueueUtil {
	
	public static final String SHUTDOWN_STRING = "###SHUTDOWN_ROW###";
	
	private static SourceRow shutdownRow = new SourceRow();
	static	{
		
		String[] shutDn = {SHUTDOWN_STRING}; 
		shutdownRow.setData(shutDn);
	}
		
	public static SourceRow generateShutdownRow(){
		return shutdownRow;
	}
	
	public static boolean isShutdownRow(SourceRow row){
		
		if (row == null){
			return false;
		} else {
			return (
				row.getData().length == 1 &&
				SHUTDOWN_STRING.equals(row.getData()[0]) == true
			    );	  
				 
		}
	}
}
