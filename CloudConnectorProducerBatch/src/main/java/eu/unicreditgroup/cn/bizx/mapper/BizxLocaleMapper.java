package eu.unicreditgroup.cn.bizx.mapper;

import eu.unicreditgroup.cn.bizx.bean.MappedColumn;
import eu.unicreditgroup.cn.bizx.bean.SourceRow;
import eu.unicreditgroup.cn.bizx.dao.Dictionary;

public class BizxLocaleMapper implements IMapper {

	private final String LMS_LOCALE_IT = "Italian";
	private final String LMS_LOCALE_DE = "German";
	private final String LMS_LOCALE_GB = "English (UK)";

	private final String BIZX_LOCALE_IT = "it_IT";
	private final String BIZX_LOCALE_DE = "de_DE";
	private final String BIZX_LOCALE_GB = "en_GB";
	

	Dictionary dictionary;

	@Override
	public MappedColumn map(String[] srcVal, String defaultVal,
			SourceRow sourceRow) {

		MappedColumn mappedCol = new MappedColumn();

		String lmsLocale = srcVal[0];
		String bizxLocale = "";
		
		if ( LMS_LOCALE_IT.equalsIgnoreCase(lmsLocale) ){
			bizxLocale = BIZX_LOCALE_IT;
		} else if ( LMS_LOCALE_DE.equalsIgnoreCase(lmsLocale) ){
			bizxLocale = BIZX_LOCALE_DE;
		} else if ( LMS_LOCALE_GB.equalsIgnoreCase(lmsLocale) ){
			bizxLocale = BIZX_LOCALE_GB;
		} else {
			bizxLocale = BIZX_LOCALE_GB; // the default
		}
						
		mappedCol.setValue(bizxLocale);
		return mappedCol;
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

}
