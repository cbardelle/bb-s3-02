package eu.unicreditgroup.cn.bizx.step;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import eu.unicreditgroup.batch.step.BatchStep;
import eu.unicreditgroup.cn.bizx.bean.MappedRow;
import eu.unicreditgroup.cn.bizx.queue.BizxDataConsumer;
import eu.unicreditgroup.cn.bizx.queue.BizxRowsQueueUtil;
import eu.unicreditgroup.cn.bizx.queue.LmsDataConsumer;
import eu.unicreditgroup.cn.bizx.queue.LmsDataProducer;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.ftp.FTPUploader;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.utils.AppContext;
import eu.unicreditgroup.utils.BackupUtil;

public class StepGenerateFileForBizx implements BatchStep {
	
	Logger logger = Logger.getLogger(StepGenerateFileForBizx.class);	
	
	private LmsDataProducer lmsDataProducer;
	
	private ConfigParams configParams;
	
	private BizxDataConsumer bizxDataConsumer;
	
	private BlockingQueue<MappedRow> mappedRowsQueue;
	
	
	private BlockingQueue<Exception> bizxExceptionsQueue;
	
	private ReportDataBean reportDataBean; 
	
	private FTPUploader resultUploader;
	

	@Override
	public void execute(){
		logger.debug("Start of execution of step generate file for BizX");
		
		try {
			BackupUtil.backupFile(configParams.getResultFileFullPathBizxConn());
		} catch (Exception e2) {
			logger.error("Failed to backup the result file for BizX connector!");
			bizxExceptionsQueue.add(e2);
		}
		
		ApplicationContext ctx = AppContext.getApplicationContext();
					
		// read LMS data from HR_CONN_RES_FILE
		//Thread sourceTableProducerThread = new Thread(sourceTableProducer);
		Thread lmsDataProducerThread = new Thread(lmsDataProducer);
		
		lmsDataProducerThread.start();
		logger.debug("LMS data producer thread started.");


		
		logger.debug("Start of LMS data queue consumers creation.");
		
		List<Thread> lmsDataConsumerThreads = new ArrayList<Thread>();
		for (int i = 0; i < configParams.getLmsDataConsumerThreadsNumber(); i++) {
			logger.debug(String.format("Creating LMS data queue consumer number %d.", i));
			LmsDataConsumer consumer = ctx.getBean(LmsDataConsumer.class);
			consumer.setConsumerNumber(i);												
			Thread consumerThread = new Thread(consumer);
			lmsDataConsumerThreads.add(consumerThread);
			consumerThread.start();
		}
		logger.debug("LMS data queue consumers created.");		
		
		// get mapped BizX data and write to a file
		Thread bizxDataConsumerThread = new Thread(bizxDataConsumer);
		bizxDataConsumerThread.start();
		logger.debug("BizX rows consumer thread started.");
	
		
		for (Thread thread : lmsDataConsumerThreads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				logger.error("Error during waiting for lms data consumer threads.");
				bizxExceptionsQueue.add(e);
			}
		}		
		
		try {
			if (!BizxRowsQueueUtil.isShutdownRow(mappedRowsQueue.peek())){
				mappedRowsQueue.put(BizxRowsQueueUtil.generateShutdownRow());
			}
		} catch (InterruptedException e1) {
			logger.error("Exception occured : " + e1);
		}
	
		
				
		try {
			lmsDataProducerThread.join();
		} catch (InterruptedException e) {
			logger.error("Error during waiting for end of LMS data producer thread.");
			bizxExceptionsQueue.add(e);
		}

		
		/*
		try {
			bizxDataConsumerThread.join();
		} catch (InterruptedException e) {
			logger.error("Error during waiting for end of BizX data consumer thread.");
			exceptionsQueue.add(e);
		}
		*/
				
		
		
		if ( StringUtils.isNotEmpty( configParams.getResultFtpPathPathBizxConn() ) ){			
			
			if (reportDataBean.getFailedFastException() == null ){
				try {
					
					File resultFile = new File(configParams.getResultFileFullPathBizxConn());
					
					if ( !resultFile.exists() || resultFile.length() == 0){
						
						throw new Exception("The result file doesn't exist or is empty!");
					}
					
					logger.debug("Starting upload of result file ...");
					
					
					resultUploader.uploadFile(resultFile);
										
					
					logger.debug("Result file uploaded.");
				} catch (Exception e) {
					reportDataBean.setFailedFastException(new Exception("Failed to upload the result file",e));					
					bizxExceptionsQueue.add(e);
				}
			}
			
		} else {
			logger.debug("Upload of result file was skipped.");
		}
		
		
		logger.debug("End of execution of step generate file for BizX");		
	}



	public LmsDataProducer getLmsDataProducer() {
		return lmsDataProducer;
	}

	public void setLmsDataProducer(LmsDataProducer lmsDataProducer) {
		this.lmsDataProducer = lmsDataProducer;
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public BizxDataConsumer getBizxDataConsumer() {
		return bizxDataConsumer;
	}

	public void setBizxDataConsumer(BizxDataConsumer bizxDataConsumer) {
		this.bizxDataConsumer = bizxDataConsumer;
	}


	public BlockingQueue<MappedRow> getMappedRowsQueue() {
		return mappedRowsQueue;
	}

	public void setMappedRowsQueue(BlockingQueue<MappedRow> mappedRowsQueue) {
		this.mappedRowsQueue = mappedRowsQueue;
	}

	
	public BlockingQueue<Exception> getBizxExceptionsQueue() {
		return bizxExceptionsQueue;
	}

	
	public void setBizxExceptionsQueue(BlockingQueue<Exception> bizxExceptionsQueue) {
		this.bizxExceptionsQueue = bizxExceptionsQueue;
	}



	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}



	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}



	public FTPUploader getResultUploader() {
		return resultUploader;
	}



	public void setResultUploader(FTPUploader resultUploader) {
		this.resultUploader = resultUploader;
	}


	public void addException(Exception e)  {
		try {
			bizxExceptionsQueue.put(e);
		} catch (InterruptedException e1) {
			logger.debug(e1);
		}
		
	}

}
