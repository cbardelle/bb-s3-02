package eu.unicreditgroup.cn.approle.bean;

import eu.unicreditgroup.cn.approle.mapper.IRoleMapper;

public class ApprRoleColumn {

	String colName;
	
	IRoleMapper mapper;
	
	String defaultValue;

	public String getColName() {
		return colName;
	}

	public void setColName(String colName) {
		this.colName = colName;
	}

	public IRoleMapper getMapper() {
		return mapper;
	}

	public void setMapper(IRoleMapper mapper) {
		this.mapper = mapper;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	
	
}
