package eu.unicreditgroup.cn.approle.bean;

public class ApprRole {
	
	String lineManagerId;
	
	String officeCode;
	
	String officeDesc;

	public String getLineManagerId() {
		return lineManagerId;
	}

	public void setLineManagerId(String lineManagerId) {
		this.lineManagerId = lineManagerId;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getOfficeDesc() {
		return officeDesc;
	}

	public void setOfficeDesc(String officeDesc) {
		this.officeDesc = officeDesc;
	}
	
	
		

}
