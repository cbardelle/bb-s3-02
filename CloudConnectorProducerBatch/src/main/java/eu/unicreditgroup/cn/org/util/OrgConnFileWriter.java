package eu.unicreditgroup.cn.org.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import org.apache.log4j.Logger;

import eu.unicreditgroup.cn.org.bean.OrgData;
import eu.unicreditgroup.cn.org.step.StepOrgConnector;
import eu.unicreditgroup.config.ConfigParams;

public class OrgConnFileWriter {
	
	private ConfigParams configParams;
	
	
	private final String CC_COLUMN_DELIMITER = "|";	
	
	private final String CC_LINE_END = "!##!";
	
	
	private File resFile = null;
	
	private BufferedWriter resFileWriter = null;
	
	private StringBuilder sb;
	
	Logger logger = Logger.getLogger(OrgConnFileWriter.class);
	
	private final String DEFAULT_DOMAIN = "SYSOBJ";
	
			
	public OrgConnFileWriter() throws Exception {		
					
	}
		
	private void writeHeader() throws Exception{
		
		String fileName = configParams.getResultFileFullPathOrgConn();
		
		resFile = new File(fileName);		
		resFileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(resFile),"UTF-8"));		
				
		// YES IT'S HARDCODED
		 		
		//String header = "ORG_ID|ORG_DESC|ORG_ID_PARENT!##!\r\n";
		
		String header = "ORG_ID|ORG_DESC|ORG_ID_PARENT|DMN_ID!##!\r\n";
		
		resFileWriter.write(header);				
	}

	public void writeResultFile( List<OrgData> orgs ) throws Exception{
		
		writeHeader();
		
		for ( OrgData od : orgs  ){			
			writeResultRow(od);
		}
		
		System.out.println(orgs.size() + " were organizations where written to the result file!");
		
		closeFile();
	}
	
	
	private void writeResultRow( OrgData od ) throws Exception{
		
		sb = new StringBuilder();
		
		sb.append( od.getOrgId() ).append(CC_COLUMN_DELIMITER);						
		
		sb.append( od.getOrgDesc() ).append(CC_COLUMN_DELIMITER);
		
		//sb.append( od.getOrgAbbr() ).append( "-" ).append( od.getOrgDesc() ).append(CC_COLUMN_DELIMITER);
				
		sb.append( od.getOrgIdParent() ).append(CC_COLUMN_DELIMITER);
		sb.append( DEFAULT_DOMAIN );
				
		sb.append(CC_LINE_END).append("\r\n");				
		resFileWriter.write(sb.toString());	
	}
	
		
	private void closeFile() throws Exception{
		if (resFileWriter != null){
			resFileWriter.close();
		}
	}
	

	public ConfigParams getConfigParams() {
		return configParams;
	}



	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}
	
	
	
	

}
