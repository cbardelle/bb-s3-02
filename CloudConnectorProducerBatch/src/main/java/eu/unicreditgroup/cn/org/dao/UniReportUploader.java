package eu.unicreditgroup.cn.org.dao;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import au.com.bytecode.opencsv.CSVReader;
import eu.unicreditgroup.cn.org.bean.OrgData;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.mailer.ReportDataBean;


/**
 * Upload results of SF report (*.csv file) into DB
 * 
 * @author UV00074
 *
 */
public class UniReportUploader
{
	
	private ConfigParams configParams;

	private OrgDao orgDao;

	private BlockingQueue<Exception> orgExceptionsQueue;
	
	private ReportDataBean reportDataBean;
	
	private final char CSV_COL_DELIMITER = ',';
	private final char CSV_QUOTE         = '"';
	
	private static final Logger logger = Logger.getLogger(UniReportUploader.class);
	
	DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
		
	private final int UNI_ORG_LST_COLS_COUNT = 5;
	
	
	
	/**
	 * 
	 * Upload of the UniOrgList report result into db table ORG_CONN_PA_ORG
	 * 
	 * @return
	 * @throws Exception 
	 */
	public int uploadUniOrgListReport( ) throws Exception
	{
		int invalidRowsCounter  = 0;
		int rowsInsertedCounter = 0;		
		int rowsCounter         = 0;
		
		CSVReader reader = null;
		try 
		{
			
			//TODO check if the file exists
			
			reader = new CSVReader(new InputStreamReader(new FileInputStream(configParams.getStrRecurReportsDwnldDir() + "\\" +  configParams.getStrRecurReportUniOrgList()), "UTF-8")
										,CSV_COL_DELIMITER
										,CSV_QUOTE);
			
		    String[] nextLine;
			
			ArrayList<OrgData> orgs = new ArrayList<OrgData>();		
						
			reader.readNext(); //omit headers
		    while ((nextLine = reader.readNext()) != null) 
		    {
		    	rowsCounter++;
		    	if(nextLine.length != UNI_ORG_LST_COLS_COUNT)
		    	{
		    		logger.debug("Invalid " + rowsCounter + " row in CSV file.");
		    		invalidRowsCounter++;
		    		continue;
		    	}
		    
		    	//"ORG_ID","ORG_DESC","ORG_TYP_ID","DMN_ID","ORG_ID_PARENT"
		    	

		    	OrgData orgData = new OrgData();
		    	
		    	orgData.setOrgId(nextLine[0]);
		    	orgData.setOrgDesc(nextLine[1]);		    	
		    	orgData.setOrgTypId(nextLine[2]);
		    	orgData.setDmnId(nextLine[3]);
		    	orgData.setOrgIdParent(nextLine[4]);
		    	
				
				// check the mandatory columns 
				if ( orgData.getOrgId() != null /*&& curr.getCurrTitle() != null && curr.getCreateDte() != null*/ ) {
					
					rowsInsertedCounter++;			
										
					orgs.add( orgData );
					
				} else {					
					invalidRowsCounter++;					
				}				
		    }	    
		    
		    if ( orgs.size() == 0 ){		    	
		    	logger.debug("Report UniOrgList returned no data.");		    	
		    	return 0;		    	
		    }		    		    
		   orgDao.insertRowsToOrgTable(orgs);	  
		   logger.debug("Report UniOrgList was loaded into table ORG_CONN_PA_ORG.");
		   
		} 
		catch (Exception e) 
		{	
			
			String nfo = "Exception when reading the result file of report UniOrgList";
			logger.error(nfo, e);
			throw new Exception(nfo,e); // to be handled in the caller method			
		} 
		finally 
		{
			try
			{
				if (reader != null) reader.close();
			} 
			catch(Exception e)
			{
				logger.error(e);
			}			
			logger.debug("REPORT UniOrgList invalidRowsCounter = "  + invalidRowsCounter);			
			logger.debug("REPORT UniOrgList rowsInsertedCounter = " + rowsInsertedCounter);			
		}		
		return rowsInsertedCounter;
	}
	
	
	public OrgDao getOrgDao() {
		return orgDao;
	}



	public void setOrgDao(OrgDao orgDao) {
		this.orgDao = orgDao;
	}



	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	
	public BlockingQueue<Exception> getOrgExceptionsQueue() {
		return orgExceptionsQueue;
	}


	public void setOrgExceptionsQueue(BlockingQueue<Exception> orgExceptionsQueue) {
		this.orgExceptionsQueue = orgExceptionsQueue;
	}


	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}



	
}
