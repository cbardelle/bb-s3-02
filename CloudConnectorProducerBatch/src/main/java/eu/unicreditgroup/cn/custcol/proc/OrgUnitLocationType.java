package eu.unicreditgroup.cn.custcol.proc;

import java.util.List;

import eu.unicreditgroup.cn.custcol.beans.CustColValue;
import eu.unicreditgroup.cn.custcol.conf.CustColConf;
import eu.unicreditgroup.cn.custcol.dao.CustColDao;

public class OrgUnitLocationType extends CustColConf implements ICustColDataProvider, Runnable {

	private CustColProc custColProc;	
	private CustColDao custColDao; 	

	@Override
	public List<CustColValue> getCustColValues()  {
		
		return custColDao.getOrgUnitLocationTypeValues();		
	}

	@Override
	public void run() {
		custColProc.processValues(getCustColValues(), this);			
	}
	
	public CustColProc getCustColProc() {
		return custColProc;
	}

	public void setCustColProc(CustColProc custColProc) {
		this.custColProc = custColProc;
	}

	public CustColDao getCustColDao() {
		return custColDao;
	}

	public void setCustColDao(CustColDao custColDao) {
		this.custColDao = custColDao;
	}

	

}
