package eu.unicreditgroup.cn.custcol.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import eu.unicreditgroup.cn.bizx.config.BizxColumn;
import eu.unicreditgroup.cn.custcol.beans.CustColValue;

public class ResultFileWriter {
	
	private final String columnDelimiter;
	
	private final String lineEnd;	
	
	private File resFile = null;
	
	private BufferedWriter resFileWriter = null;
	
	private StringBuilder sb;
			
	public ResultFileWriter(String columnDelimiter, String lineEnd,
			String fileName) throws Exception {		
		this.columnDelimiter = columnDelimiter;
		this.lineEnd = lineEnd;					
		resFile = new File(fileName);		
		resFileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(resFile),"UTF-8"));		
		writeHeader();
	}
	
	
	public void writeHeader() throws Exception{
		
		// YES IT'S HARDCODED
		
		String header = "COL_NUM|LABEL|REF_ID|REF_DESC!##!\r\n";
		
		
		resFileWriter.write(header);		
	}

	
	public void writeResultRow( CustColValue ccv ) throws Exception{
		
		sb = new StringBuilder();
		
		sb.append( ccv.getColNum() ).append(columnDelimiter);						
		sb.append( ccv.getLabel() ).append(columnDelimiter);
		sb.append( ccv.getId() ).append(columnDelimiter);
		sb.append( ccv.getDesc() );
		
		sb.append(lineEnd).append("\r\n");				
		resFileWriter.write(sb.toString());		
		
		
	}
		
	public void closeFile() throws Exception{
		if (resFileWriter != null){
			resFileWriter.close();
		}
	}
	
	


}
