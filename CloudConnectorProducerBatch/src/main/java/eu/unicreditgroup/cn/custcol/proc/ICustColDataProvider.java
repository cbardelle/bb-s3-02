package eu.unicreditgroup.cn.custcol.proc;

import java.util.List;

import eu.unicreditgroup.cn.custcol.beans.CustColValue;

public interface ICustColDataProvider {
	
	
	
	public List<CustColValue> getCustColValues();
	
	
	
	
	

}
