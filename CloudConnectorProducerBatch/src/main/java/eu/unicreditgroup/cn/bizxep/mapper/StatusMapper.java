package eu.unicreditgroup.cn.bizxep.mapper;


import eu.unicreditgroup.cn.bizxep.bean.MappedColumn;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;



public class StatusMapper implements IMapper {
	
	private final String SAP_ACTIVE = "active";
	
	private final String BIZX_ACTIVE = "active";
	private final String BIZX_INACTIVE = "inactive";
	
	@Override
	public MappedColumn map(String[] srcVal, String defaultVal, SapSourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn();
		
		String bizxStatus = "";
		
		if (SAP_ACTIVE.equalsIgnoreCase(sourceRow.getUserStatusDesc())){
			bizxStatus = BIZX_ACTIVE;
		} else {
			bizxStatus = BIZX_INACTIVE;			
		} 
		
		
		mappedCol.setValue(bizxStatus);
		return mappedCol;
	}
	
}
