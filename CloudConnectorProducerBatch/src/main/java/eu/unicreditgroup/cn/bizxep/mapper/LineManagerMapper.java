package eu.unicreditgroup.cn.bizxep.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizxep.bean.MappedColumn;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;


/**
 * 
 *  
 * @author UV00074
 *
 */
public class LineManagerMapper implements IMapper {
		
	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SapSourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn(); 
				

		if ( StringUtils.isEmpty( sourceRow.getLineManagerId() )){
			mappedCol.setValue( defaultVal );
		} else {
			mappedCol.setValue( StringUtils.lowerCase( sourceRow.getLineManagerId() ));	
		}
		
		return mappedCol;

	}

	
}
