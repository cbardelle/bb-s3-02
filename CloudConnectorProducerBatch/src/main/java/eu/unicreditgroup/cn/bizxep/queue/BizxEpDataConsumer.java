package eu.unicreditgroup.cn.bizxep.queue;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.cn.bizxep.bean.MappedRow;
import eu.unicreditgroup.cn.bizxep.config.BizxEpColumn;
import eu.unicreditgroup.cn.bizxep.util.ResultFileWriter;
import eu.unicreditgroup.cn.bizxep.util.ResultTableWriter;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.mailer.ReportDataBean;


/**
 * 
 * The consumer of mapped rows from the mappedRowsQueue queue.
 * Rows are write down to the result file which will be sent to the BizX connector. 
 * 
 * @author UV00074
 *
 */
public class BizxEpDataConsumer implements Runnable {
	
	private static final Logger logger = Logger.getLogger(BizxEpDataConsumer.class);
	
	private ConfigParams configParams;
			
	private int consumerNumber;
	
	private ReportDataBean reportDataBean;
	
	private BlockingQueue<MappedRow> mappedEpRowsQueue;
	
	private BlockingQueue<Exception> bizxEpExceptionsQueue;
		 
	private final String BIZX_COLUMN_DELIMITER = ",";	
	
	private final String BIZX_LINE_END = "\r\n";
	
	private ResultTableWriter bizxResultTableWriter;
	
	private List<BizxEpColumn> resultColumnsBizxEp;
		
	
	/*
	public void closeResultFile(){
		if ( bizxFileWriter!= null ){
			try {				
				bizxFileWriter.close();
			} catch (Exception e) {
				logger.error("Failed to close result file", e);
			}
		}		
	}
	*/
	
	@Override
	public void run() {
		
		
		try {
			
			//bizxFileWriter = new ResultFileWriter(BIZX_COLUMN_DELIMITER, BIZX_LINE_END, configParams.getResultFileFullPathBizxEp());			
			//bizxFileWriter.writeHeader(resultColumnsBizxEp);			
			//bizxFileWriter.writeHeaderDescriptions(resultColumnsBizxEp);
						
			while(true){
				Thread.yield();
				
				MappedRow mappedRow = mappedEpRowsQueue.take();								
				
				if (BizxRowsQueueUtil.isShutdownRow(mappedRow)){
					logger.debug(String.format("[Cons %d] Shutdown row was found. Consumer will stop its work.", consumerNumber));
					mappedEpRowsQueue.put(mappedRow);
					
					return;					
				}
			
				//reportDataBean.incResultFileRowsCount();				
				bizxResultTableWriter.writeResultRow( mappedRow );
				
								
				reportDataBean.incBizxEpRowTotal();
				
				/*
				if (configParams.isGenearteCsvFile())
				{
					csvFileWriter.writeResultRow( mappedRow.getData() );
				}*/
								
			}
			
		} catch (Exception ex) {
			logger.error(String.format("Exception during processing mapped rows" ), ex);			
			bizxEpExceptionsQueue.add(ex);
			
			// when exception occurs during writing the result the file is not sent
			reportDataBean.setFailedFastException(ex);
		} finally {
			
			try {
				
				bizxResultTableWriter.flushData();
				
			} catch (Exception e) {
				
				logger.error(e);
				logger.debug("Exception during closing result file writer", e);
				
			}
			
		}
	}

	public void bizxResultTableWriterFlushData() throws Exception{
		
		bizxResultTableWriter.flushData();
		
	}
	

	public ConfigParams getConfigParams() {
		return configParams;
	}


	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}


	public int getConsumerNumber() {
		return consumerNumber;
	}


	public void setConsumerNumber(int consumerNumber) {
		this.consumerNumber = consumerNumber;
	}

	
	
	

	public BlockingQueue<MappedRow> getMappedEpRowsQueue() {
		return mappedEpRowsQueue;
	}

	public void setMappedEpRowsQueue(BlockingQueue<MappedRow> mappedEpRowsQueue) {
		this.mappedEpRowsQueue = mappedEpRowsQueue;
	}

	public BlockingQueue<Exception> getBizxEpExceptionsQueue() {
		return bizxEpExceptionsQueue;
	}

	public void setBizxEpExceptionsQueue(
			BlockingQueue<Exception> bizxEpExceptionsQueue) {
		this.bizxEpExceptionsQueue = bizxEpExceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}
	

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public List<BizxEpColumn> getResultColumnsBizxEp() {
		return resultColumnsBizxEp;
	}

	public void setResultColumnsBizxEp(List<BizxEpColumn> resultColumnsBizxEp) {
		this.resultColumnsBizxEp = resultColumnsBizxEp;
	}


	public ResultTableWriter getBizxResultTableWriter() {
		return bizxResultTableWriter;
	}


	public void setBizxResultTableWriter(ResultTableWriter bizxResultTableWriter) {
		this.bizxResultTableWriter = bizxResultTableWriter;
	}

	
	/**
	 * Cleanup and backup the data for Bizx EP
	 * 
	 * @throws Exception
	 */
	public void cleanAndBackupData() throws Exception{
		
			
		bizxResultTableWriter.getSourceDao().cleanBizxepUserTmp();
		bizxResultTableWriter.getSourceDao().backupBizxepUser();	
		
		
		
	}

	/**
	 * Merge the table BIZXEP_USER into BIZXEP_USER_TMP  
	 * to identify inactive users (who are not present in the current data from SAP/SSC) 
	 * @throws Exception 
	 * 
	 */
	public void mergeNewUsers() throws Exception {
		bizxResultTableWriter.getSourceDao().bizxepMergeNewUsers();
		
	}


	public void generateResultFile() throws Exception {
		
		
		ResultFileWriter bizxFileWriter;
		try {
			bizxFileWriter = new ResultFileWriter(BIZX_COLUMN_DELIMITER, BIZX_LINE_END, configParams.getResultFileFullPathBizxEp());
			bizxFileWriter.writeHeader(resultColumnsBizxEp);
			
			bizxFileWriter.writeHeaderDescriptions(resultColumnsBizxEp);
			
			List<MappedRow> users =	bizxResultTableWriter.getSourceDao().getBizxEpUserData();
			
			
			for (MappedRow row : users){
				
				bizxFileWriter.writeResultRow( row.getData() );				
				
			}
			
			bizxFileWriter.close();
			
			
		} catch (Exception e) {
			throw new Exception("Exception when generating the result file for BizX Ep", e);
		}
		
		
		
		
		
	}

	
	
	
	

}
