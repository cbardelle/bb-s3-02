package eu.unicreditgroup.cn.bizxep.mapper;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import eu.unicreditgroup.cn.bizxep.bean.MappedColumn;
import eu.unicreditgroup.cn.bizxep.bean.MappedRow;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;
import eu.unicreditgroup.cn.bizxep.config.BizxEpColumn;
import eu.unicreditgroup.cn.bizxep.config.SapColumn;
import eu.unicreditgroup.cn.bizxep.mapper.exception.InvalidColumnException;
import eu.unicreditgroup.cn.bizxep.mapper.exception.InvalidRowException;
import eu.unicreditgroup.cn.bizxep.validator.IValidator;
import eu.unicreditgroup.mailer.ReportDataBean;

public class BizxEpMapper implements InitializingBean{

	Logger logger = Logger.getLogger(BizxEpMapper.class);
	
	
	private int resultRowSize = 66;  
	
	/**
	 * metadata of all columns of the result table
	 */
	private List<BizxEpColumn> resultColumnsBizxEp;
	
	private ReportDataBean reportDataBean;
	
	private BlockingQueue<Exception> bizxEpExceptionsQueue;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		resultRowSize = resultColumnsBizxEp.size();
		if ( resultRowSize <= 0 ){
			throw new Exception("Configuratrion problem: result columns list for BizX EP file is empty.");
		}
	}

	public MappedRow processRow(SapSourceRow sourceRow, String srcUserId){
		
		
		MappedRow mappedRow = new MappedRow();
		mappedRow.setData(new String[resultRowSize]);
				
		mappedRow.setValid(true);
		
		// map the result row
		int colIdx = 0;
		for(BizxEpColumn resCol : resultColumnsBizxEp){
			
			IMapper columnMapper = resCol.getMapper();		
			String defaultValue = resCol.getDefaultValue();			
			String mappedValue = null;			
			
			String[] srcValues = getSourceValues(resCol.getSrcCol(), sourceRow); 
					
			MappedColumn mappedCol = null;
			mappedCol = columnMapper.map(srcValues, defaultValue, sourceRow);
				
			if (mappedCol.getErrorMsg() != null) {
				
				// if failed to map mandatory column reject the record
				if (resCol.isMandatory() == true){
					mappedRow.setValid(false);
					
					
										
					InvalidRowException e = new InvalidRowException( String.format("Failed to map mandatory column %s, User : %s. %s", resCol.getResColName(), sourceRow.getUserId(), mappedCol.getErrorMsg()) );
					logger.error(e);					
					reportDataBean.addBizxEpExceptionWithLimit(e);
					//do not process rejected row
					break;
				} else {
					// for non mandatory column just report the exception and use its default value
					// reportDataBean.incSourceFileInvalidColumnsCount();
					 
					InvalidColumnException e = new InvalidColumnException(String.format("Failed to map column %s, User : %s. %s.", resCol.getResColName(), sourceRow.getUserId(), mappedCol.getErrorMsg() ));
					
					logger.error(e);
					reportDataBean.addBizxEpExceptionWithLimit(e);
					mappedValue = defaultValue;						
				}
			} else {
				mappedValue = mappedCol.getValue();
			}
			
			// run rejectable validators
			// for external users (from file source_aaext.csv) there are more mandatory columns (firsName, lastName, email)
			// external users without their mandatory columns will be rejected
			if (runAllValidators(resCol.getRejectableValidators(), mappedValue, srcValues, srcUserId, sourceRow) == false){
				// the row will be rejected
				mappedRow.setValid(false);
				InvalidRowException e = new InvalidRowException( String.format("Failed to map column %s, User : %s. %s", resCol.getResColName(), sourceRow.getUserId(), "Attribute value is not present in the dictionary!" ));
				logger.error(e);					
				reportDataBean.addBizxEpExceptionWithLimit(e);										
				
			}
			
			
			// run non rejectable validator
			if (runAllValidators(resCol.getValidators(), mappedValue, srcValues, srcUserId, sourceRow) == false){
				mappedValue = defaultValue;
				logger.error("Validation for column " + resCol.getResColName() + " has failed and default value will be used: '" + defaultValue + "'");
				// reportDataBean.incSourceFileInvalidColumnsCount();				
			}
					
			
			if (mappedValue == null){
				mappedValue = "";			
			}			
			
			mappedRow.setValue(colIdx, mappedValue);						
			colIdx++;						
		}	
		return mappedRow;		
		
	}

	
	private String[] getSourceValues(List<SapColumn> sapCols,
			SapSourceRow sourceRow) {
		
		if(sapCols == null || sapCols.size() == 0){
			return null;
		}		
		String[] sourceValues = new String[sapCols.size()];
		int i = 0;
		for(SapColumn lmsCol : sapCols)
		{	
			String value = sourceRow.getUserId();
			sourceValues[i] = value;						
			i++;
		}		
		return sourceValues;		
	}


	public boolean runAllValidators(List<IValidator> validators, String value, String[] srcValues, String srcUserId, SapSourceRow sourceRow)
	{
		if (validators == null){
			return true;
		}
		boolean validRow = true;
		boolean result;
		for (IValidator validator : validators){
			result = validator.validate(value, srcValues, 0 /* not used */, sourceRow);
			if ( validRow == true && result == false){
				validRow = result;
			}
			
			// do not stop after first failure
			// run all validators to send back information 
			if (result == false){
				Exception e = new Exception("Validator of type " + validator.getClass().getName() + " has failed for value: '" + value + "' and User ID: " + srcUserId );
				logger.error(e);
				bizxEpExceptionsQueue.add(e);
			}			
		}		
		return validRow;
	}	
	


	public List<BizxEpColumn> getResultColumnsBizxEp() {
		return resultColumnsBizxEp;
	}

	public void setResultColumnsBizxEp(List<BizxEpColumn> resultColumnsBizxEp) {
		this.resultColumnsBizxEp = resultColumnsBizxEp;
	}

	public BlockingQueue<Exception> getBizxEpExceptionsQueue() {
		return bizxEpExceptionsQueue;
	}

	public void setBizxEpExceptionsQueue(
			BlockingQueue<Exception> bizxEpExceptionsQueue) {
		this.bizxEpExceptionsQueue = bizxEpExceptionsQueue;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}
	
	
}
