package eu.unicreditgroup.cn.bizxep.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizxep.bean.MappedColumn;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;


/**
 * 
 *  
 * @author UV00074
 *
 */
public class HrbpMapper implements IMapper {
		
	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SapSourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn(); 
				
		if ( StringUtils.isEmpty( sourceRow.getHrbpId() )){
			mappedCol.setValue( defaultVal );
		} else {
			mappedCol.setValue( StringUtils.lowerCase( sourceRow.getHrbpId() ));	
		}
		
		return mappedCol;

	}

	
}
