package eu.unicreditgroup.cn.bizxep.mapper;

import org.apache.commons.lang.StringUtils;

import eu.unicreditgroup.cn.bizxep.bean.MappedColumn;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;


/**
 * 
 *  
 * @author UV00074
 *
 */
public class JobCodeMapper implements IMapper {

	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SapSourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn(); 
		
		if (  "Y".equalsIgnoreCase( sourceRow.getRoleScc() ) == false ){ 
			
			StringBuffer sb = new StringBuffer();
			String value;

			sb.append( sourceRow.getGlobalJobDesc() );
			if (StringUtils.isNotEmpty( sourceRow.getGlobalJobAbbr() )){
				sb.append(" (").append( sourceRow.getGlobalJobAbbr() ).append(")");
				
				value = sb.toString();
			} else {
				value = "";
				 
			}
				
			mappedCol.setValue( value );
		
		}
		return mappedCol;

	}

	
}
