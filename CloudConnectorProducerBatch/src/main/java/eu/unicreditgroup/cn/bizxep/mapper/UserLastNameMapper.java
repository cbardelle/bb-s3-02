package eu.unicreditgroup.cn.bizxep.mapper;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

import eu.unicreditgroup.cn.bizxep.bean.MappedColumn;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;


/**
 * 
 *  
 * @author UV00074
 *
 */
public class UserLastNameMapper implements IMapper {

	@Override	
	public MappedColumn map( String[] srcVal, String defaultVal, SapSourceRow sourceRow) {
		
		MappedColumn mappedCol = new MappedColumn(); 
			
		mappedCol.setValue( WordUtils.capitalizeFully( sourceRow.getLastName() ));
		return mappedCol;
	
	}
	
}
