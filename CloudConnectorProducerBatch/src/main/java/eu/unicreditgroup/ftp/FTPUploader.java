package eu.unicreditgroup.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

public class FTPUploader {
	
	private static final Logger logger = Logger.getLogger(FTPUploader.class);
		
	private String host;

	private String user;

	private String password;
	
	private String directory;

	private final int ftpDefaultTimeout = 1200000; // 20 min 
	
	private final int ftpDataTimeout = 1200000; // 20 min 
	
	public void downloadFile(String fileName, String localFullPath) throws Exception {
		FTPClient ftp = new FTPClient();

		try {
			logger.debug(String.format("Trying to connect to ftp server: '%s'", host));
			
			ftp.setDefaultTimeout(ftpDefaultTimeout); 
			ftp.connect(host);
			ftp.setDataTimeout(ftpDataTimeout);  

			logger.debug(String.format("Trying to autohrize: '%s' : '%s'", user, password));
			ftp.login(user, password);
			
			int connectReply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(connectReply)) {
				ftp.disconnect();
				throw new IllegalStateException(String.format("FTP server: '%s' refused connection with reply: %d.", host, connectReply));
			}

			logger.debug("Setting binary file type.");
			ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
			
			if (StringUtils.isNotBlank(directory)){
				logger.debug(String.format("Trying to change directory to '%s'", directory));
				ftp.changeWorkingDirectory(directory);
				
				int cwdReply = ftp.getReplyCode();
				if (!FTPReply.isPositiveCompletion(cwdReply)) {
					ftp.disconnect();
					throw new IllegalStateException(String.format("Problem during changing directory. Error code: %d.", cwdReply));
				}
			}
			
			FileOutputStream fos = new FileOutputStream(localFullPath);			
			ftp.retrieveFile(fileName, fos);
			
			ftp.logout();
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}
	}
	
	
	public void moveFile(String oldFile, String newFile) throws Exception {
		FTPClient ftp = new FTPClient();

		try {
			logger.debug(String.format("Trying to connect to ftp server: '%s'", host));
			
			
			ftp.setDefaultTimeout(ftpDefaultTimeout); 
			ftp.connect(host);
			ftp.setDataTimeout(ftpDataTimeout);  

			logger.debug(String.format("Trying to autohrize: '%s' : '%s'", user, password));
			ftp.login(user, password);
			
			int connectReply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(connectReply)) {
				ftp.disconnect();
				throw new IllegalStateException(String.format("FTP server: '%s' refused connection with reply: %d.", host, connectReply));
			}

			logger.debug("Setting binary file type.");
			ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
			
	        try
	        {	        	
	        	logger.debug("Moving file from " + oldFile + " to " + newFile);
	        	ftp.rename(oldFile, newFile);
	        }
	        catch(Exception e)
	        {
	        	throw new Exception("Cannot move file from " + oldFile + " to " + newFile , e);
	        }
			
	        logger.debug("File successfully moved from " + oldFile + " to " + newFile);
	        
			ftp.logout();
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}
	}
	
	
	public void uploadFile(File inputFile) throws Exception {
		FTPClient ftp = new FTPClient();
		
		

		try {
			logger.debug(String.format("Trying to connect to ftp server: '%s'", host));

			ftp.setDefaultTimeout(ftpDefaultTimeout); 
			ftp.connect(host);
			ftp.setDataTimeout(ftpDataTimeout);  

			logger.debug(String.format("Trying to autohrize: '%s' : '%s'", user, password));
			ftp.login(user, password);
			
			int connectReply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(connectReply)) {
				ftp.disconnect();
				throw new IllegalStateException(String.format("FTP server: '%s' refused connection with reply: %d.", host, connectReply));
			}

			
			
			logger.debug("Setting binary file type.");
			ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
			
			if (StringUtils.isNotBlank(directory)){
				logger.debug(String.format("Trying to change directory to '%s'", directory));
				ftp.changeWorkingDirectory(directory);
				
				int cwdReply = ftp.getReplyCode();
				if (!FTPReply.isPositiveCompletion(cwdReply)) {
					ftp.disconnect();
					throw new IllegalStateException(String.format("Problem during changing directory. Error code: %d.", cwdReply));
				}
			}
			
			logger.debug(String.format("Trying to send file: '%s'", inputFile.getAbsoluteFile()));
			ftp.storeFile(inputFile.getName(), new FileInputStream(inputFile));
						
			logger.debug("File successfully sent.");
			
			ftp.logout();
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}
	}


	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = StringUtils.trim(host);
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = StringUtils.trim(user);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = StringUtils.trim(password);
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = StringUtils.trim(directory);
	}

	
}	
