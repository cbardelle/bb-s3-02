<html>

<head>
	
	<style>
		body {
			font-family: verdana, sans-serif;
			font-size: 10pt;
			padding: 6px;
		}
		
		.headInfoDoneWithoutErrors {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #3db00b;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		.headInfoDoneWithErrors {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #FF8C00;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		.headInfoFailedFast {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #d8001d;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		
		
		
		.resultTable {
			width: 100%;
			border: 1px #6a8546 solid;
			text-align: center;
		}
		
		.resultTableName {
			background-color: #6a8546;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
			padding: 3px;
			width: 280px;
			margin-top: 10px;
		}
		
		.resultName {
			font-weight: bolder;
			width: 300px;
			text-align: left;
			vertical-align: top;
			font-size: 10pt;
		}
		
		.resultValue {
			text-align: left;
			padding-left: 5px;
			font-size: 10pt;
		}
		
		.errorValue {
			color: #d8001d;
			font-weight: bolder;
		}

		.warningValue {
			color: #ed872c;
			font-weight: bolder;
		}

	</style>

</head>

<body>



	<#if ( rb.totalErrorsCnt == 0 )>
		<div class="headInfoDoneWithoutErrors"> 
			&radic; Batch finished successfully.
		</div>				
	<#else>	
		<div class="headInfoDoneWithErrors"> 
			Batch finished with exceptions.
		</div>
	</#if>
	
	
	<#-- Generic execution info  -->
	<div class="resultTableName">Execution</div>
	<table class="resultTable">
		<tr>
			<td class="resultName">Start</td>
			<td class="resultValue">
				<#if rb.batchStartDate??>
					${rb.batchStartDate?string("yyyy-MM-dd HH:mm:ss")}
				<#else> 
					Undefined 
				</#if>
			</td>
		</tr>
		<tr>
			<td class="resultName">End</td>
			<td  class="resultValue">
				<#if rb.batchEndDate?? >
					${rb.batchEndDate?string("yyyy-MM-dd HH:mm:ss")}
				<#else> 
					Undefined 
				</#if>
			</td>
		</tr>
	</table>

		
	
	<#if (rb.configParams.stepGenerateFileForBizxEnabled == true)>
		<div class="resultTableName">Generating result file for the BizX connector</div>
		<table class="resultTable">
			<tr>
				<td class="resultName">Number of source lms rows </td>
				<td  class="resultValue">
					${rb.lmsSourceRowTotal}
				</td>
			</tr>
			<tr>
				<td class="resultName">Number of successfully mapped Bizx rows </td>
				<td  class="resultValue">
					${rb.bizxRowTotal}
				</td>
			</tr>
			<tr>
				<td class="resultName">Number of exceptions </td>
				<td  class="resultValue">
					${rb.bizxExceptionsQueue?size}
				</td>
			</tr>	
		</table>			
	</#if>
	
	<#if (rb.configParams.stepGenerateFileForBizxEpEnabled == true)>
		<div class="resultTableName">Generating result file for the BizX EP</div>
		<table class="resultTable">
			<tr>
				<td class="resultName">Number of source SAP rows </td>
				<td  class="resultValue">
					${rb.bizxEpSapUsersCount}
				</td>
			</tr>
			<tr>
				<td class="resultName">Number of source SSC rows </td>
				<td  class="resultValue">
					${rb.bizxEpSscUsersCount}
				</td>
			</tr>
			<tr>
				<td class="resultName">Number of successfully mapped rows for BizX EP </td>
				<td  class="resultValue">
					${rb.bizxEpRowTotal}
				</td>
			</tr>			
			<tr>
				<td class="resultName">List of non existing roles combination for cust. field 15</td>
				<td  class="resultValue">
					${rb.strBizxEpRoleExceptions}
				</td>
			</tr>			
			<tr>
				<td class="resultName">Number of exceptions </td>
				<td  class="resultValue">
					${rb.bizxEpExceptionsQueue?size}
				</td>
			</tr>	
		</table>			
	</#if>
		
	<#if (rb.configParams.stepApprRoleConnEnabled == true)>
		<div class="resultTableName">Generating result file for the Approval Role Connector </div>
		<table class="resultTable">
			<tr>
				<td class="resultName">Number of new approval roles</td>
				<td  class="resultValue">
					${rb.appRoleConnNewRolesCount}
				</td>
			</tr>
			<tr>
				<td class="resultName">Number of removed approval roles</td>
				<td  class="resultValue">
					${rb.appRoleConnRemovedCount}
				</td>
			</tr>
			<tr>
				<td class="resultName">Number of exceptions </td>
				<td  class="resultValue">
					${rb.apprRoleExceptionsQueue?size}
				</td>
			</tr>	
		</table>			
	</#if>
	

	<#if (rb.configParams.stepCustColConnectorEnabled == true)>
		<div class="resultTableName">Generating result file for the Custom Column Connector</div>
		<table class="resultTable">			
			<tr>
				<td class="resultName">Total number of records written</td>
				<td  class="resultValue">
					${rb.custColumConnTotalWrite}
				</td>
			</tr>
			<tr>
				<td class="resultName">Total number of values with different description for the same ID</td>
				<td  class="resultValue">
					${rb.custColumConnDiffDescr}
				</td>
			</tr>

			<tr>
				<td class="resultName">Number of exceptions </td>
				<td  class="resultValue">
					${rb.custColExceptionsQueue?size}
				</td>
			</tr>	
		</table>			
	</#if>


	<#if (rb.configParams.stepOrgConnectorEnabled == true)>
	
		<div class="resultTableName">Generating result file for the Org Connector</div>
		<table class="resultTable">			
			<tr>
				<td class="resultName">Total number of records written</td>
				<td  class="resultValue">
					${rb.orgRowTotal}
				</td>
			</tr>
			<tr>
				<td class="resultName">Number of exceptions </td>
				<td  class="resultValue">
					${rb.orgExceptionsQueue?size}
				</td>
			</tr>	
		</table>			
	</#if>
</body>
</html>
