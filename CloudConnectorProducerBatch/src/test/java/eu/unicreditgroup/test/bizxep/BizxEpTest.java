package eu.unicreditgroup.test.bizxep;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Test;

import eu.unicreditgroup.cn.bizxep.bean.MappedRow;
import eu.unicreditgroup.cn.bizxep.bean.SapSourceRow;
import eu.unicreditgroup.cn.bizxep.mapper.BizxEpMapper;
import eu.unicreditgroup.test.BaseSpringTest;

public class BizxEpTest extends BaseSpringTest {

	@Resource(name = "bizxEpMapper")
	private BizxEpMapper bizxEpMapper;
	
	

	@Test
	public void testSapUserAllRoles() {
				
		SapSourceRow row = new SapSourceRow();
		
		row.setUserStatusDesc("Active");
		row.setUserId("UV00000");
		row.setFirstName("PiotR");
		row.setLastName("NowaK");
		row.setGender("M");
		row.setOfficeEmail("p.nowak@gmail.com");
		row.setLineManagerId("1234");
		row.setHrbpId("5678");
		row.setGlobalJobDesc("GlobalJobDesc");
		row.setGlobalJobAbbr("GlobalJobAbbr");
		row.setDivisionDesc("DivisionDesc");
		row.setOfficeCountryDesc("OfficeCountryDesc");
		row.setHireDate("01.01.2000");
		row.setUserOfficeDesc("UserOfficeDesc");
		row.setOfficeFixPhone("OfficeFixPhone");
		row.setOfficeAddr("OfficeAddr");
		row.setOfficeCity("OfficeCity");
		row.setOfficeProvince("OfficeProvince");
		row.setOfficePostalCode("OfficePostalCode");
		row.setDateOfBirth("01.01.1970");
		row.setHostLegalEntDesc("HostLegalEntDesc");
		row.setHostLegalEntCode("HostLegalEntCode");
		row.setUserSapId("12345678");
		row.setGlobalJobBandDesc("GlobalJobBandDesc");
		row.setCompetenceLineDesc("CompetenceLineDesc");
		row.setLineManager("Y");
		row.setHrbp("Y");
		row.setRoleHrbp("Y");
		row.setRoleSeniorHr("Y");
		row.setRoleRecruitingCoordinator("Y");
		row.setRoleAdmin("Y");
		row.setRoleScc("");		
		MappedRow mr = bizxEpMapper.processRow(row, row.getUserId());
		
		
		assertEquals( mr.getValue(0),"active" );//0 STATUS
		assertEquals( mr.getValue(1),"uv00000" );//1 USERID
		assertEquals( mr.getValue(2),"uv00000" );//2 USERNAME
		assertEquals( mr.getValue(3),"Piotr" );//3 FIRSTNAME
		assertEquals( mr.getValue(4),"Nowak" );//4 LASTNAME
		assertEquals( mr.getValue(5),"" );//5 MI
		assertEquals( mr.getValue(6),"M" );//6 GENDER
		assertEquals( mr.getValue(7),"p.nowak@gmail.com" );//7 EMAIL
		assertEquals( mr.getValue(8),"NO_MANAGER" );//8 MANAGER
		assertEquals( mr.getValue(9),"NO_HR" );//9 HR
		assertEquals( mr.getValue(10),"CompetenceLineDesc" );//10 DEPARTMENT
		assertEquals( mr.getValue(11),"GlobalJobDesc (GlobalJobAbbr)" );//11 JOBCODE
		assertEquals( mr.getValue(12),"DivisionDesc" );//12 DIVISION
		assertEquals( mr.getValue(13),"OfficeCountryDesc" );//13 LOCATION		
		assertEquals( mr.getValue(14),"CET" );//14 TIMEZONE
		assertEquals( mr.getValue(15),"01/01/2000" );//15 HIREDATE		
		assertEquals( mr.getValue(16),"uv00000" );//16 EMPID		
		assertEquals( mr.getValue(17),"UserOfficeDesc" );//17 TITLE
		assertEquals( mr.getValue(18),"OfficeFixPhone" );//18 BIZ_PHONE
		assertEquals( mr.getValue(19),"" );//19 FAX
		assertEquals( mr.getValue(20),"OfficeAddr" );//20 ADDR1
		assertEquals( mr.getValue(21),"" );//21 ADDR2
		assertEquals( mr.getValue(22),"OfficeCity" );//22 CITY
		assertEquals( mr.getValue(23),"OfficeProvince" );//23 STATE
		assertEquals( mr.getValue(24),"OfficePostalCode" );//24 ZIP
		assertEquals( mr.getValue(25),"" );//25 COUNTRY
		assertEquals( mr.getValue(26),"" );//26 REVIEW_FREQ
		assertEquals( mr.getValue(27),"01/01/1970" );//27 LAST_REVIEW_DATE
		assertEquals( mr.getValue(28),"HostLegalEntDesc (HostLegalEntCode)" );//28 CUSTOM01
		assertEquals( mr.getValue(29),"" );//29 CUSTOM02
		assertEquals( mr.getValue(30),"" );//30 CUSTOM03
		assertEquals( mr.getValue(31),"12345678");//31 CUSTOM04
		assertEquals( mr.getValue(32),"" );//32 CUSTOM05
		assertEquals( mr.getValue(33),"" );//33 CUSTOM06
		assertEquals( mr.getValue(34),"" );//34 CUSTOM07		
		assertEquals( mr.getValue(35),"GlobalJobBandDesc" );//35 CUSTOM08		
		assertEquals( mr.getValue(36),"" );//36 CUSTOM09		
		assertEquals( mr.getValue(37),"CompetenceLineDesc" );//37 CUSTOM10
		assertEquals( mr.getValue(38),"" );//38 CUSTOM11
		assertEquals( mr.getValue(39),"" );//39 CUSTOM12
		assertEquals( mr.getValue(40),"" );//40 CUSTOM13
		assertEquals( mr.getValue(41),"" );//41 CUSTOM14
		assertEquals( mr.getValue(42),"RCM_MHSRA" );//42 CUSTOM15		
		assertEquals( mr.getValue(43),"");//43 MATRIX_MANAGER
		assertEquals( mr.getValue(44),"en_GB" );//44 DEFAULT_LOCALE
		assertEquals( mr.getValue(45),"" );//45 PROXY
		assertEquals( mr.getValue(46),"" );//46 CUSTOM_MANAGER
		assertEquals( mr.getValue(47),"" );//47 SECOND_MANAGER
		assertEquals( mr.getValue(48),"SSO" );//48 LOGIN_METHOD
	}
	
	@Test
	public void testSapUserSsc() {
				
		SapSourceRow row = new SapSourceRow();
		
		row.setUserStatusDesc("Active");
		row.setUserId("UV00000");
		row.setFirstName("PiotR");
		row.setLastName("NowaK");
		row.setGender("M");
		row.setOfficeEmail("p.nowak@gmail.com");
		row.setLineManagerId("1234");
		row.setHrbpId("5678");
		row.setGlobalJobDesc("GlobalJobDesc");
		row.setGlobalJobAbbr("GlobalJobAbbr");
		row.setDivisionDesc("DivisionDesc");
		row.setOfficeCountryDesc("OfficeCountryDesc");
		row.setHireDate("01.01.2000");
		row.setUserOfficeDesc("UserOfficeDesc");
		row.setOfficeFixPhone("OfficeFixPhone");
		row.setOfficeAddr("OfficeAddr");
		row.setOfficeCity("OfficeCity");
		row.setOfficeProvince("OfficeProvince");
		row.setOfficePostalCode("OfficePostalCode");
		row.setDateOfBirth("01.01.1970");
		row.setHostLegalEntDesc("HostLegalEntDesc");
		row.setHostLegalEntCode("HostLegalEntCode");
		row.setUserSapId("12345678");
		row.setGlobalJobBandDesc("GlobalJobBandDesc");
		row.setCompetenceLineDesc("CompetenceLineDesc");
		row.setLineManager("");
		row.setHrbp("");
		row.setRoleHrbp("");
		row.setRoleSeniorHr("");
		row.setRoleRecruitingCoordinator("");
		row.setRoleAdmin("");
		row.setRoleScc("Y");		
		MappedRow mr = bizxEpMapper.processRow(row, row.getUserId());
		
		
		assertEquals( mr.getValue(0),"active" );//0 STATUS
		assertEquals( mr.getValue(1),"uv00000" );//1 USERID
		assertEquals( mr.getValue(2),"uv00000" );//2 USERNAME
		assertEquals( mr.getValue(3),"Piotr" );//3 FIRSTNAME
		assertEquals( mr.getValue(4),"Nowak" );//4 LASTNAME
		assertEquals( mr.getValue(5),"" );//5 MI
		assertEquals( mr.getValue(6),"M" );//6 GENDER
		assertEquals( mr.getValue(7),"p.nowak@gmail.com" );//7 EMAIL
		assertEquals( mr.getValue(8),"NO_MANAGER" );//8 MANAGER
		assertEquals( mr.getValue(9),"NO_HR" );//9 HR
		assertEquals( mr.getValue(10),"" );//10 DEPARTMENT
		assertEquals( mr.getValue(11),"" );//11 JOBCODE
		assertEquals( mr.getValue(12),"" );//12 DIVISION
		assertEquals( mr.getValue(13),"" );//13 LOCATION		
		assertEquals( mr.getValue(14),"CET" );//14 TIMEZONE
		assertEquals( mr.getValue(15),"01/01/1950" );//15 HIREDATE		
		assertEquals( mr.getValue(16),"uv00000" );//16 EMPID		
		assertEquals( mr.getValue(17),"" );//17 TITLE
		assertEquals( mr.getValue(18),"" );//18 BIZ_PHONE
		assertEquals( mr.getValue(19),"" );//19 FAX
		assertEquals( mr.getValue(20),"" );//20 ADDR1
		assertEquals( mr.getValue(21),"" );//21 ADDR2
		assertEquals( mr.getValue(22),"" );//22 CITY
		assertEquals( mr.getValue(23),"" );//23 STATE
		assertEquals( mr.getValue(24),"" );//24 ZIP
		assertEquals( mr.getValue(25),"" );//25 COUNTRY
		assertEquals( mr.getValue(26),"" );//26 REVIEW_FREQ
		assertEquals( mr.getValue(27),"" );//27 LAST_REVIEW_DATE
		assertEquals( mr.getValue(28),"SSC" );//28 CUSTOM01
		assertEquals( mr.getValue(29),"" );//29 CUSTOM02
		assertEquals( mr.getValue(30),"" );//30 CUSTOM03
		assertEquals( mr.getValue(31),"");//31 CUSTOM04
		assertEquals( mr.getValue(32),"" );//32 CUSTOM05
		assertEquals( mr.getValue(33),"" );//33 CUSTOM06
		assertEquals( mr.getValue(34),"" );//34 CUSTOM07		
		assertEquals( mr.getValue(35),"" );//35 CUSTOM08		
		assertEquals( mr.getValue(36),"" );//36 CUSTOM09		
		assertEquals( mr.getValue(37),"" );//37 CUSTOM10
		assertEquals( mr.getValue(38),"" );//38 CUSTOM11
		assertEquals( mr.getValue(39),"" );//39 CUSTOM12
		assertEquals( mr.getValue(40),"" );//40 CUSTOM13
		assertEquals( mr.getValue(41),"" );//41 CUSTOM14
		assertEquals( mr.getValue(42),"RCM_C" );//42 CUSTOM15		
		assertEquals( mr.getValue(43),"");//43 MATRIX_MANAGER
		assertEquals( mr.getValue(44),"en_GB" );//44 DEFAULT_LOCALE
		assertEquals( mr.getValue(45),"" );//45 PROXY
		assertEquals( mr.getValue(46),"" );//46 CUSTOM_MANAGER
		assertEquals( mr.getValue(47),"" );//47 SECOND_MANAGER
		assertEquals( mr.getValue(48),"SSO" );//48 LOGIN_METHOD
	}

	

}