//--copy all users to the history table
CREATE TABLE HR_CONN_SAP_SRC_HST
(
USER_STATUS_ID  VARCHAR2(1),
USER_STATUS_DESC  VARCHAR2(80),
USER_ID  VARCHAR2(30),
FIRST_NAME  VARCHAR2(80),
LAST_NAME  VARCHAR2(80),
GENDER  VARCHAR2(1),
GLOBAL_JOB_CODE  VARCHAR2(15),
GLOBAL_JOB_ABBR  VARCHAR2(25),
GLOBAL_JOB_DESC  VARCHAR2(70),
USER_OFFICE_CODE  VARCHAR2(20),
USER_OFFICE_ABBR  VARCHAR2(30),
USER_OFFICE_DESC  VARCHAR2(80),
HOST_LEGAL_ENT_CODE  VARCHAR2(20),
HOST_LEGAL_ENT_DESC  VARCHAR2(80),
CONTRACT_TYPE_CODE  VARCHAR2(5),
CONTRACT_TYPE_DESC  VARCHAR2(40),
CONTRACT_DETAILS  VARCHAR2(20),
OFFICE_ADDR  VARCHAR2(150),
OFFICE_CITY  VARCHAR2(100),
OFFICE_PROVINCE  VARCHAR2(3),
OFFICE_PROVINCE_DESC  VARCHAR2(50),
OFFICE_POSTAL_CODE  VARCHAR2(30),
OFFICE_COUNTRY  VARCHAR2(3),
OFFICE_COUNTRY_DESC  VARCHAR2(50),
OFFICE_EMAIL  VARCHAR2(350),
HIRE_DATE  VARCHAR2(30),
TERM_DATE  VARCHAR2(30),
LINE_MANAGER_ID  VARCHAR2(20),
RESUME  VARCHAR2(30),
COST_CENTER_CODE  VARCHAR2(20),
OFFICE_MOBILE_PHONE  VARCHAR2(100),
OFFICE_FIX_PHONE  VARCHAR2(100),
FISCAL_CODE  VARCHAR2(50),
ORG_1ST_LEVEL_CODE  VARCHAR2(15),
ORG_1ST_LEVEL_ABBR  VARCHAR2(25),
ORG_1ST_LEVEL_DESC  VARCHAR2(70),
ORG_2ND_LEVEL_CODE  VARCHAR2(15),
ORG_2ND_LEVEL_ABBR  VARCHAR2(25),
ORG_2ND_LEVEL_DESC  VARCHAR2(70),
HOME_LEGAL_ENT_CODE  VARCHAR2(10),
HOME_LEGAL_ENT_DESC  VARCHAR2(50),
DIVISION_CODE  VARCHAR2(20),
DIVISION_ABBR  VARCHAR2(30),
DIVISION_DESC  VARCHAR2(60),
COMPETENCE_LINE_CODE  VARCHAR2(20),
COMPETENCE_LINE_ABBR  VARCHAR2(30),
COMPETENCE_LINE_DESC  VARCHAR2(60),
HRBP_ID  VARCHAR2(30),
HRBP  VARCHAR2(1),
LINE_MANAGER  VARCHAR2(1),
GLOBAL_JOB_BAND_CODE  VARCHAR2(15),
GLOBAL_JOB_BAND_ABBR  VARCHAR2(20),
GLOBAL_JOB_BAND_DESC  VARCHAR2(60),
JOB_ROLE_CODE  VARCHAR2(15),
JOB_ROLE_ABBR  VARCHAR2(25),
JOB_ROLE_DESC  VARCHAR2(70),
PAY_GRADE_CODE  VARCHAR2(10),
PAY_GRADE_DESC  VARCHAR2(60),
PREV_JOB_ROLE_CODE  VARCHAR2(15),
PREV_JOB_ROLE_ABBR  VARCHAR2(25),
PREV_JOB_ROLE_DESC  VARCHAR2(70),
SECOND_PREV_JOB_ROLE_CODE  VARCHAR2(15),
SECOND_PREV_JOB_ROLE_ABBR  VARCHAR2(25),
SECOND_PREV_JOB_ROLE_DESC  VARCHAR2(70),
JOB_ROLE_START_DATE  VARCHAR2(30),
DATE_OF_BIRTH  VARCHAR2(30),
NATION_ID  VARCHAR2(3),
NATION_DESC  VARCHAR2(80),
EXPATRIATE  VARCHAR2(1),
EDU_TITLE_ID  VARCHAR2(5),
EDU_TITLE_DESC  VARCHAR2(50),
LOCAL_LANG  VARCHAR2(3),
LOCAL_LANG_DESC  VARCHAR2(100),
CURRENCY_ID  VARCHAR2(3),
COST_CENTER_DESC  VARCHAR2(80),
USER_SAP_ID varchar2(30),
SUPERVISOR_SAP_ID varchar2(30),
HRBP_SAP_ID varchar2(30),
JOB_SENIORITY_CODE VARCHAR2(1),		/* New attr for UBIS */
JOB_SENIORITY_DESC VARCHAR2(15),	/* New attr for UBIS */
SERVICE_LINE_CODE VARCHAR2(8),		/* New attr for UBIS */
SERVICE_LINE_DESC VARCHAR2(40),		/* New attr for UBIS */
BUSINESS_LINE_CODE VARCHAR2(8),		/* New attr for UBIS */
BUSINESS_LINE_DESC VARCHAR2(40),	/* New attr for UBIS */
EXPAT_TYPE VARCHAR2(1),                                                                                                                                                                                  
FUNCTIONAL_RESP VARCHAR2(1),                                                                                                                                                                                  
FTE VARCHAR2(6),                                                                                                                                                       
ORG_DISTRICT_CODE VARCHAR2(8),                                                                                                                                                                                 
ORG_DISTRICT_NAME VARCHAR2(40),                                                                                                                                                                                 
ORG_UNIT_LOC_TYPE VARCHAR2(1),
GLOBAL_JOB_LDR_CODE	VARCHAR2(15),
GLOBAL_JOB_LDR_ABBR	VARCHAR2(20),
GLOBAL_JOB_LDR_DESC	VARCHAR2(60),
GLOBAL_JOB_LVL_CODE	VARCHAR2(15),
GLOBAL_JOB_LVL_ABBR	VARCHAR2(20),
GLOBAL_JOB_LVL_DESC	VARCHAR2(60),
PRODUCT_LINE_CODE VARCHAR2(8),
PRODUCT_LINE_DESC VARCHAR2(40),
CREATED_DATE DATE,
LAST_UPDATED_DATE DATE, 
REMOVED_DATE DATE,
IS_LM_NOT_PRESENT CHAR(1),
CONSTRAINT HR_CONN_SAP_SRC_HST_PK  PRIMARY KEY (USER_ID)
);  

ALTER TABLE HR_CONN_SAP_SRC_HST
add CONSTRAINT HR_CONN_SAP_SRC_HST_PK PRIMARY KEY (USER_ID);

  
--alter table
--   HR_CONN_SAP_SRC_HST
--add
--   IS_LM_NOT_PRESENT  char(1);
  

CREATE TABLE HR_CONN_COLS_HST
  (
    USER_ID     VARCHAR2(30) NOT NULL,
    COLUMN_NAME VARCHAR2(100),
    OLD_VALUE   VARCHAR2(300),
    NEW_VALUE   VARCHAR2(300),
    CHANGE_DATE DATE
  );
  
  
CREATE TABLE HR_CONN_HST_BATCH_LOG
  (
    START_DATE DATE,
    END_DATE DATE,
	SRC_DATA_DELIVERY_DATE DATE,	
    EXEC_RESULT   VARCHAR2(50),
    USERS_ADDED   NUMBER,
    USERS_UPDATED NUMBER,
    USERS_REMOVED NUMBER
  );

  


