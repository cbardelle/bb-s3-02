<html>

<head>
	
	<style>
		body {
			font-family: verdana, sans-serif;
			font-size: 10pt;
			padding: 6px;
		}
		
		.headInfoDoneWithoutErrors {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #3db00b;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		.headInfoDoneWithErrors {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #FF8C00;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		.headInfoFailedFast {
			margin-top: 5px;
			margin-bottom: 5px;
			padding: 10px;
			background-color: #d8001d;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
		}
		
		
		
		
		.resultTable {
			width: 100%;
			border: 1px #6a8546 solid;
			text-align: center;
		}
		
		.resultTableName {
			background-color: #6a8546;
			color: #ffffff;
			font-weight: bolder;
			font-size: 10pt;
			font-family: verdana, sans-serif;
			padding: 3px;
			width: 280px;
			margin-top: 10px;
		}
		
		.resultName {
			font-weight: bolder;
			width: 300px;
			text-align: left;
			vertical-align: top;
			font-size: 10pt;
		}
		
		.resultValue {
			text-align: left;
			padding-left: 5px;
			font-size: 10pt;
		}
		
		.errorValue {
			color: #d8001d;
			font-weight: bolder;
		}

		.warningValue {
			color: #ed872c;
			font-weight: bolder;
		}

	</style>

</head>

<body>
	
	<div class="headInfoDoneWithErrors">Batch finished.</div>
	
	
	<div class="resultTableName">Changed attributes used in assignment profiles for last 7 days</div>
	<table class="resultTable">		
		<tr>
			<th class="resultName">Plateau attr desc</th>
			<th class="resultName">Attr SAP code</th>
			<th class="resultName">New value</th>
			<th class="resultName">Old value</th>
			<th class="resultName">List of assignment profiles</th>
		</tr>					
		<#list tad as rowb>
		<tr>
			<td class="resultValue">${rowb.attrName}</td>			
			<td class="resultValue">${rowb.attrCode}</td>			
			<td class="resultValue">${rowb.dictValue}</td>			
			<td class="resultValue">${rowb.mappedValue}</td>			
			<td class="resultValue">${rowb.assignProfId}</td>
		</tr>
		</#list>
	</table>
		
		
</body>

</html>


