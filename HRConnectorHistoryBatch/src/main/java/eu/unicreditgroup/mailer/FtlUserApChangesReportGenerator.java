package eu.unicreditgroup.mailer;

import eu.unicreditgroup.beans.TrackedAttr;
import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

public class FtlUserApChangesReportGenerator extends AbstractReportGenerator {

	protected static final String EMAIL_SENDER_CONFIG = "conf/apReportSender.ini";

	private static final Logger logger = Logger.getLogger(FtlReportGenerator.class);

	public FtlUserApChangesReportGenerator() {
	}

	/* (non-Javadoc)
	 * @see it.ucilearning.elvis.util.ReportGenerator#generateAndSendReport(it.ucilearning.elvis.bean.ReportDataBean)
	 */
	public void generateAndSendReport(List<TrackedAttr> tad){
		logger.debug("Starting email generation...");
		
		try {
			//build and send mail
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd__HH_mm_ss");
		
			EmailSender emailSender = new EmailSender(EMAIL_SENDER_CONFIG);
			emailSender.setSubject( "HR Connector History Batch AP Changes report" );

			emailSender.setMessage( prepateFtlMessage(tad) );
			sendEmail(emailSender);

		} catch (Exception e) {
			logger.debug("Exception during email sending.", e);
		}
	}

	
	private String prepateFtlMessage( List<TrackedAttr> tad) throws IOException {
		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(getClass(), "/META-INF/ftl/");
		cfg.setObjectWrapper(ObjectWrapper.BEANS_WRAPPER);
		
		Map<String, Object> params = new HashMap<String, Object>();
    	params.put("tad", tad);
    	
    	 // Get the template object
        Template t = cfg.getTemplate("user_ap_changed_report.ftl");
        StringWriter sw = new StringWriter();
        
        // Merge the data-model and the template
        try {
            t.process(params,sw);
        } catch (TemplateException e) {
        	logger.error("Problem during mail generation", e);
        	sw.append(e.toString());
        } 
        
        return sw.toString();
	}
	
	
}



















