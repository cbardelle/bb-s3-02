package eu.unicreditgroup.beans;

public class ChangedColInf {
	
	String userId;
	String columnName;
	String oldValue;
	String newValue;
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getOldValue() {
		return oldValue;
	}
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}
	public String getNewValue() {
		return newValue;
	}
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
	@Override
	public String toString() {
		
		return userId + " " + columnName  + " " + oldValue  + " " + newValue; 
				
	}
	
	
	

}
