package eu.unicreditgroup.batch;

import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import eu.unicreditgroup.batch.processor.HistoryRowProcessor;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.utils.SourceRowQueueUtil;

public class SourceTableConsumer implements Runnable {

	private static final Logger logger = Logger.getLogger(SourceTableConsumer.class);
	
	private ConfigParams configParams;
			
	private int consumerNumber;
	
	private ReportDataBean reportDataBean;
	
	private BlockingQueue<SourceRow> sourceRowsQueue;
		 
	private HistoryRowProcessor historyRowProcessor;
	
	private BlockingQueue<Exception> exceptionsQueue;
	

	@Override
	public void run() {
		
		SourceRow sourceRow = new SourceRow();
		try {	
			
			while(true){
				Thread.yield();
				
				sourceRow = sourceRowsQueue.take();
				
				if (SourceRowQueueUtil.isShutdownRow(sourceRow)){
					logger.debug(String.format("[Cons %d] Shutdown row was found. Consumer will stop its work.", consumerNumber));
					sourceRowsQueue.put(sourceRow);
					return;
				}
				
				historyRowProcessor.processRow(sourceRow);
				
							
			}
			
		} catch (Exception ex) {						
			logger.debug(String.format("[Cons %d] Exception during processing soure row for userID '%s'.", consumerNumber, sourceRow.getValue(2)), ex);
			exceptionsQueue.add(ex);
		}
	}

	public HistoryRowProcessor getHistoryRowProcessor() {
		return historyRowProcessor;
	}

	public void setHistoryRowProcessor(HistoryRowProcessor historyRowProcessor) {
		this.historyRowProcessor = historyRowProcessor;
	}


	public int getConsumerNumber() {
		return consumerNumber;
	}


	public void setConsumerNumber(int consumerNumber) {
		this.consumerNumber = consumerNumber;
	}


	public ConfigParams getConfigParams() {
		return configParams;
	}


	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}


	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}


	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}


	public BlockingQueue<SourceRow> getSourceRowsQueue() {
		return sourceRowsQueue;
	}


	public void setSourceRowsQueue(BlockingQueue<SourceRow> sourceRowsQueue) {
		this.sourceRowsQueue = sourceRowsQueue;
	}

	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}	
	
	
	
	
}
