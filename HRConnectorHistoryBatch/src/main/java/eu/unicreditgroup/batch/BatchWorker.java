package eu.unicreditgroup.batch;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import eu.unicreditgroup.batch.processor.JobLocationDescriptionProcessor;
import eu.unicreditgroup.batch.processor.JobPositionDescriptionProcessor;
import eu.unicreditgroup.batch.processor.LineManagerProcessor;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.beans.TrackedAttr;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.dao.HistoryBatchLogDao;
import eu.unicreditgroup.dao.SapSourceHistoryDao;
import eu.unicreditgroup.dao.SourceTableReader;
import eu.unicreditgroup.dao.TrackedAttrDao;
import eu.unicreditgroup.mailer.FtlUserApChangesReportGenerator;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.utils.AppContext;
import eu.unicreditgroup.utils.SourceRowQueueUtil;
import com.plateausystems.elms.webservices.axis.Caller;
import com.plateausystems.elms.webservices.axis.Learner;
import com.plateausystems.elms.webservices.axis.Learners;
import com.plateausystems.elms.webservices.axis.LearnersSearchCriteria;
import com.plateausystems.elms.webservices.axis.PlateauLMS;
import com.plateausystems.elms.webservices.axis.CloudPlateauLMSService;
import com.plateausystems.elms.webservices.axis.UpdateLearners;


/**
 * @author UV00074
 *
 */
public class BatchWorker {
	
	Logger logger = Logger.getLogger(BatchWorker.class);
	
	private ConfigParams configParams;
	
	private SourceTableProducer sourceTableProducer;
	
	private BlockingQueue<SourceRow> sourceRowsQueue;
			
	private ReportDataBean reportDataBean;
	
	private SourceTableReader sourceTableReader;
	
	private SapSourceHistoryDao sapSourceHistoryDao; 
	
	private BlockingQueue<Exception> exceptionsQueue;
	
	private HistoryBatchLogDao historyBatchLogDao;
	
	private TrackedAttrDao trackedAttrDao;
	
	private JobLocationDescriptionProcessor jobLocationDescriptionProcessor;
	private JobPositionDescriptionProcessor jobPositionDescriptionProcessor;
	private LineManagerProcessor lineManagerProcessor;
			
	private final String WS_CLIENT_USER = "WS_CLIENT"; 
	
	private final String WS_CLIENT_PWD = "AD126TG%rt2";
	
	/**
	 * Main method here. Executes steps as set in config file.
	 * 
	 */	
	public void execute(){		
		reportDataBean.setBatchStartDate(new Date());
		
		// Process users with invalid (null or not exisitng) line manager
		// in this step LM is reseted in LMS using Plateau WebServices		
		if (configParams.isStepProcessUsersWithInvalidLineManager()){			              
			try {
				
				logger.debug("Step process users with invalid line manager...");
				List<String> usersWithInvalidLineManager = sapSourceHistoryDao.getUsersWithInvalidLineManager();
				
				reportDataBean.setUsersWithInvalidLineManager(usersWithInvalidLineManager);
				
				reportDataBean.setUsersWithInvalidLineManagerCount(usersWithInvalidLineManager.size());
				
				logger.debug("In the source file found: " + usersWithInvalidLineManager.size() + " user(s) with invalid line manager"); // + usersWithInvalidLineManager );
				
				List<String> updatedUsers = new ArrayList<String>();
				
				// initialize the reportDataBean with empty object 
				// (when the WS call fails it won't be null) 
				reportDataBean.setUsersWithInvalidLineManagerWS(updatedUsers);
				
				
				updatedUsers = lineManagerProcessor.resetLineManager(usersWithInvalidLineManager);
				
												
				reportDataBean.setUsersWithInvalidLineManagerWS(updatedUsers);
				
			} catch (Exception e){
				logger.error("Exception during process users with invalid line manager", e);
				exceptionsQueue.add(e);
			}				
		} else {
			logger.debug("Step process users with invalid line manager is disabled, nothing to do.");
		}
			
	
		// update tables HR_CONN_SAP_SRC_HST, HR_CONN_COLS_HST
		if (configParams.isStepProcessHistoryData()){
			
			try {				
				reportDataBean.setStepProcessHistoryDataStartDate(new Date());				
				executeProcessHistoryDataStep();
				
				// get statistics - new & updated users				
				reportDataBean.setNewUsersCount(sapSourceHistoryDao.getNewUsersCount());
				reportDataBean.setUpdatedUsersCount(sapSourceHistoryDao.getUpdatedUsersCount());
				
				reportDataBean.setStepProcessHistoryDataEndDate(new Date());				
			} catch (Exception e){
				logger.error("Exception during step process history data", e);
				exceptionsQueue.add(e);
			}
			
		} else {
			logger.debug("Step process history data is disabled, nothingto do.");
		}
		
		// update info about removed users in HR_CONN_SAP_SRC_HST
		if (configParams.isStepProcessUsersRemovedFromSapSource()){
			try {
				
				int removedUsersCount = sapSourceHistoryDao.updateRemovedUsers();
				reportDataBean.setRemovedUsersCount(removedUsersCount);
				
			} catch (Exception e){
				logger.error("Exception during process users removed from SAP source", e);
				exceptionsQueue.add(e);
			}				
		} else {
			logger.debug("Step process users removed from SAP source is disabled, nothing to do.");
		}
		
		//TODO cleanup
		// 2012-06-11 - refactoring processing of users with invalid line managers simplified
		// and performed in one step: StepProcessUsersWithInvalidLineManager
		/*
		if (configParams.isStepProcessUsersWithNonExisitingLineManager()){
			
			// run only when data in the history table was updated
			if (configParams.isStepProcessHistoryData() && configParams.isStepProcessUsersRemovedFromSapSource())
			{
				try {
					
					logger.debug("Step process users with non-existing line manager...");
					
					// identify new users with non-exisiting LM
					
					List<String> usersWithNonExistingLM = sapSourceHistoryDao.getNewUsersWithNonExisitingLineManager();
					
					System.out.println("User with non exisitng LM : " + usersWithNonExistingLM);
					
					reportDataBean.setUsersWithNonExistingLineManager(usersWithNonExistingLM);
					               
					reportDataBean.setUsersWithWithNonExistingLineManagerCount(usersWithNonExistingLM.size());
					
					logger.debug(usersWithNonExistingLM.size() + " user(s) with non-existing line manager: " + usersWithNonExistingLM );
					
					// update previously identifed not existing Line managers
					
					int result = sapSourceHistoryDao.updateOldUsersWithNonExLineManager();
					
					logger.debug(result + " user(s) with non exisitng line manager updated");
					
					// set the column is_lm_not_present for all new users with non existing LM
					sapSourceHistoryDao.updateNewUsersWithNonExLineManager(usersWithNonExistingLM);
				
					
				} catch (Exception e){
					logger.error("Exception during process users with non-existing line manager", e);
					exceptionsQueue.add(e);
				}	
			} else {
				logger.debug("Step process users with non-existing Line manager should be executed in conjunction with step process history data and step process removed users, nothing to do.");				
			}
		} else {
			logger.debug("Step process users with non-existing line manager is disabled, nothing to do.");
		}
		*/
		
		// process Job location description changes (Web Service)
		if (configParams.isStepProcessJobLocationDescriptionChanges()){
			
			try{
				logger.debug("Step process Job Location Description changes...");
				jobLocationDescriptionProcessor.execute();
			} catch (Exception e){
				logger.error("Exception during process Job Location Description changes", e);
				exceptionsQueue.add(e);
			}	
		} else {
			logger.debug("Step process Job Location Description changes is disabled, nothing to do.");
		}

		
		// Job Position description changes 
		// HTML UNIT
		if (configParams.isStepProcessJobPositionDescriptionChanges()){
			
			try{
				logger.debug("Step process Job Position Description changes...");
				jobPositionDescriptionProcessor.execute();
			} catch (Exception e){
				logger.error("Exception during process Job Position Description changes", e);
				exceptionsQueue.add(e);
			}
		} else {
			logger.debug("Step process Job Position Description changes is disabled, nothing to do.");
		}

		

		//sends e-mail with last 7 days AP attributes changes.
		if( configParams.isStepSendUsersApChangedReport() )
		{
			int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
			if(configParams.getSendUsersApChangesAtWeekDay() == dayOfWeek )
			{
				logger.debug("Step send Users Assignment Profile changed report");
				
				List<TrackedAttr> tad = trackedAttrDao.fetchTrackedAttrsSinceLastWeek();
				FtlUserApChangesReportGenerator ftlUserAP = new FtlUserApChangesReportGenerator();
				logger.debug("Send Users Assignment Profile changed report");
				ftlUserAP.generateAndSendReport(tad);				
			}

		} 
		else 
		{
			logger.debug("Step send Users Assignment Profile changed report is disabled, nothing to do.");
		}
		
		reportDataBean.setBatchEndDate(new Date());	
		
		long diff = reportDataBean.getBatchEndDate().getTime() - reportDataBean.getBatchStartDate().getTime(); 
		
		long diffMinutes = diff / (60 * 1000);
			 
		System.out.println("--------------------------------------------------");
		System.out.println("Batch executed in " + diffMinutes);
		
		
		// write info to the log table
		historyBatchLogDao.addLogInfo();
		
	}
	
	
	

	public void executeProcessHistoryDataStep(){
		logger.debug("Start of execution for processing history data");
		ApplicationContext ctx = AppContext.getApplicationContext();
		
		// clean up source queue
		if ( sourceRowsQueue.size() > 1 ||
			sourceRowsQueue.size() == 1 && 
			!SourceRowQueueUtil.isShutdownRow(sourceRowsQueue.peek()))
		{
			logger.error("Definitly not good and shouldn't get here becasue the  queue should be epmty!!!");
		}
		
		sourceRowsQueue.clear();
			
		// read data from database
		Thread sourceTableProducerThread = new Thread(sourceTableProducer);
		sourceTableProducerThread.start();
		logger.debug("Source table producer thread started.");

		
		// consume rows read from db
		logger.debug("Start of source table queue consumers creation.");
		List<Thread> sourceTableConsumerThreads = new ArrayList<Thread>();
		for (int i = 0; i < configParams.getSourceRowProcessThreadsNumber(); i++) {
			logger.debug(String.format("Creating source table queue consumer number %d.", i));
			SourceTableConsumer consumer = ctx.getBean(SourceTableConsumer.class);			
			consumer.setConsumerNumber(i);												
			Thread consumerThread = new Thread(consumer);
			sourceTableConsumerThreads.add(consumerThread);
			consumerThread.start();
		} 
		logger.debug("Source table queue consumers created.");
		
		// wait for threads to end gracefully ;)
		for (Thread thread : sourceTableConsumerThreads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				logger.error("Error during waiting for end of created source table consumer threads.");				
			}
		}		
	
		try {
			sourceTableProducerThread.join();
		} catch (InterruptedException e) {
			logger.error("Error during waiting for end of source table producer thread.");
			
		}
		
		// after the processing history data merge the hst table
		// TODO this could be possibly done more efficiently in the feature
		logger.debug("Starting to merge HR_CONN_SAP_SRC_HST table...");
		sapSourceHistoryDao.mergeHistoryTable();
		logger.debug("Finished merging HR_CONN_SAP_SRC_HST table.");
		
		logger.debug("End of execution for processing history data");		
	}


	public ConfigParams getConfigParams() {
		return configParams;
	}


	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}


	public SourceTableProducer getSourceTableProducer() {
		return sourceTableProducer;
	}


	public void setSourceTableProducer(SourceTableProducer sourceTableProducer) {
		this.sourceTableProducer = sourceTableProducer;
	}

	public BlockingQueue<SourceRow> getSourceRowsQueue() {
		return sourceRowsQueue;
	}


	public void setSourceRowsQueue(BlockingQueue<SourceRow> sourceRowsQueue) {
		this.sourceRowsQueue = sourceRowsQueue;
	}


	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}


	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}


	public SourceTableReader getSourceTableReader() {
		return sourceTableReader;
	}


	public void setSourceTableReader(SourceTableReader sourceTableReader) {
		this.sourceTableReader = sourceTableReader;
	}

	public SapSourceHistoryDao getSapSourceHistoryDao() {
		return sapSourceHistoryDao;
	}

	public void setSapSourceHistoryDao(SapSourceHistoryDao sapSourceHistoryDao) {
		this.sapSourceHistoryDao = sapSourceHistoryDao;
	}

	public void setTrackedAttrDao(TrackedAttrDao trackedAttrDao) {
		this.trackedAttrDao = trackedAttrDao;
	}
	
	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}

	public HistoryBatchLogDao getHistoryBatchLogDao() {
		return historyBatchLogDao;
	}

	public void setHistoryBatchLogDao(HistoryBatchLogDao historyBatchLogDao) {
		this.historyBatchLogDao = historyBatchLogDao;
	}


	public JobLocationDescriptionProcessor getJobLocationDescriptionProcessor() {
		return jobLocationDescriptionProcessor;
	}


	public void setJobLocationDescriptionProcessor(
			JobLocationDescriptionProcessor jobLocationDescriptionProcessor) {
		this.jobLocationDescriptionProcessor = jobLocationDescriptionProcessor;
	}


	public JobPositionDescriptionProcessor getJobPositionDescriptionProcessor() {
		return jobPositionDescriptionProcessor;
	}


	public void setJobPositionDescriptionProcessor(
			JobPositionDescriptionProcessor jobPositionDescriptionProcessor) {
		this.jobPositionDescriptionProcessor = jobPositionDescriptionProcessor;
	}




	public LineManagerProcessor getLineManagerProcessor() {
		return lineManagerProcessor;
	}




	public void setLineManagerProcessor(LineManagerProcessor lineManagerProcessor) {
		this.lineManagerProcessor = lineManagerProcessor;
	}
	
	
	
}
