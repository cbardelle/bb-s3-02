package eu.unicreditgroup.batch.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.plateausystems.elms.webservices.axis.CloudPlateauLMSService;
import com.plateausystems.elms.webservices.axis.CloudWebSrvFactory;
import com.plateausystems.elms.webservices.axis.Learner;
import com.plateausystems.elms.webservices.axis.Learners;
import com.plateausystems.elms.webservices.axis.LearnersSearchCriteria;
import com.plateausystems.elms.webservices.axis.PlateauLMS;
import com.plateausystems.elms.webservices.axis.UpdateLearners;

public class LineManagerProcessor {
	
private CloudWebSrvFactory cloudWebSrvFactory;	
	
	
public List<String> resetLineManager(List<String> usersWithResetedLineManager) throws Exception {
		
		 
		Logger logger = Logger.getLogger(LineManagerProcessor.class);
		
		PlateauLMS cloudWS = cloudWebSrvFactory.getCloudWS();
		
		UpdateLearners ul = new UpdateLearners();		
		ul.setCaller(cloudWebSrvFactory.getCaller());
		
		List<String> updatedUsers = new ArrayList<String>();
		
		for (String userId : usersWithResetedLineManager ){

			LearnersSearchCriteria lsc = new LearnersSearchCriteria();
						
			lsc.setCaller(cloudWebSrvFactory.getCaller());
			lsc.setLearnerID(userId);
					
			Learners learners = cloudWS.findLearners(lsc);
			
			if (learners.getLearner().size() == 1){
				
				//logger.debug("Found user for reseting line manager : " + userId);
				Learner user = learners.getLearner().get(0);
				
				//set line manager to null only for users with not empty Supervisor ID
				if (StringUtils.isNotEmpty(user.getSupervisorID()) ){
					logger.debug("Line manager for user: " + userId + " should be null, actual value in Plateau: " + user.getSupervisorID());
					user.setSupervisorID("");
					ul.getLearner().add(user);
					updatedUsers.add(userId);
					
				}
				
			} else if (learners.getLearner().size() == 0){
				logger.debug("Cannot find user for reseting line manager : " + userId);				
			}
		}
		
		logger.debug("Reseting line managers for users : " + updatedUsers);
		cloudWS.updateLearners(ul);
		
		return updatedUsers;
	}


	public CloudWebSrvFactory getCloudWebSrvFactory() {
		return cloudWebSrvFactory;
	}
	
	
	public void setCloudWebSrvFactory(CloudWebSrvFactory cloudWebSrvFactory) {
		this.cloudWebSrvFactory = cloudWebSrvFactory;
	}

	
	

}
