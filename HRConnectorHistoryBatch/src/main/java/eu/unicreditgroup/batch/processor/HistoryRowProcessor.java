package eu.unicreditgroup.batch.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import eu.unicreditgroup.beans.ChangedColInf;
import eu.unicreditgroup.beans.HistorySourceRow;
import eu.unicreditgroup.beans.SourceRow;
import eu.unicreditgroup.beans.User;
import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.config.SourceColumn;
import eu.unicreditgroup.config.SourceTableConf;
import eu.unicreditgroup.dao.ColumnsHistoryDao;
import eu.unicreditgroup.dao.SapSourceHistoryDao;

public class HistoryRowProcessor {
	
	private SourceTableConf sourceTableConf;
	
	private ConfigParams configParams;
	
	private SapSourceHistoryDao sapSourceHistoryDao;	
	
	private ColumnsHistoryDao columnsHistoryDao;
		
	private BlockingQueue<Exception> exceptionsQueue;
	
	
	private static final Logger logger = Logger.getLogger(HistoryRowProcessor.class);
	
	
	@Transactional("ltp")
	public void processRow(SourceRow sourceRow) throws Exception
	{
		String userId = sourceRow.getValue(sourceTableConf.getUserIdIndex());
				
		HistorySourceRow histRow = sapSourceHistoryDao.getUser(userId);
		
		
		//user doesn't exist
		if (histRow == null){
			
			// add new user
			sapSourceHistoryDao.addNewUser(sourceRow.getData());
						
		} else {
			
			// check for changes
			List<ChangedColInf> changedColumns = compareWithNewUser(sourceRow.getData(), histRow.getData());
			
			
			// check that users was removed but has shown up again
			String removedDate = histRow.getValue(sapSourceHistoryDao.getColRemovedDateIndex());
			if (StringUtils.isNotEmpty(removedDate)){
				// reset the date 
				
				sapSourceHistoryDao.resetRemovedDate(userId);
				// and add to be tracked
				ChangedColInf  removedDateCol = new ChangedColInf();
				removedDateCol.setColumnName(sapSourceHistoryDao.getColRemovedDateName());
				removedDateCol.setNewValue("");
				removedDateCol.setOldValue(removedDate);
				removedDateCol.setUserId(userId);
				changedColumns.add(removedDateCol);
			}
			
			
			// are there any changes?
			if (changedColumns.size() > 0){				
				columnsHistoryDao.addChangedColInf(changedColumns);
				
				//update user record in the history table, modify the last updated column
				sapSourceHistoryDao.updateUserRecord(userId, changedColumns);
				
			}			
		}		
	}	
	
	public List<ChangedColInf> compareWithNewUser(String[] newUserData, String[] oldUserData){
				
		String[] colsToCheck = configParams.getColumnsToTrack();
		Map colsMap =  sourceTableConf.getSourceColumnsMap();
		
		ArrayList<ChangedColInf> changedCols = new ArrayList<ChangedColInf>();
		int i;
		String oldValue;
		String newValue;
		
		for(String columnName: colsToCheck){
			
			SourceColumn srcColumn = (SourceColumn) colsMap.get(columnName);			
			i = srcColumn.getSrcColIndex();
			oldValue = oldUserData[i];
			newValue = newUserData[i];	
			
			if ( StringUtils.equals(oldValue, newValue) == false ){
				ChangedColInf changedCol = new ChangedColInf();
				changedCol.setColumnName(columnName);
				changedCol.setNewValue(newValue);
				changedCol.setOldValue(oldValue);
				changedCol.setUserId(newUserData[sourceTableConf.getUserIdIndex()]);
				changedCols.add(changedCol);				
			}			
		}
				
		return changedCols;
		
	}

	public SourceTableConf getSourceTableConf() {
		return sourceTableConf;
	}

	public void setSourceTableConf(SourceTableConf sourceTableConf) {
		this.sourceTableConf = sourceTableConf;
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public SapSourceHistoryDao getSapSourceHistoryDao() {
		return sapSourceHistoryDao;
	}

	public void setSapSourceHistoryDao(SapSourceHistoryDao sapSourceHistoryDao) {
		this.sapSourceHistoryDao = sapSourceHistoryDao;
	}

	public ColumnsHistoryDao getColumnsHistoryDao() {
		return columnsHistoryDao;
	}

	public void setColumnsHistoryDao(ColumnsHistoryDao columnsHistoryDao) {
		this.columnsHistoryDao = columnsHistoryDao;
	}

	public BlockingQueue<Exception> getExceptionsQueue() {
		return exceptionsQueue;
	}

	public void setExceptionsQueue(BlockingQueue<Exception> exceptionsQueue) {
		this.exceptionsQueue = exceptionsQueue;
	}	
	
	

}

