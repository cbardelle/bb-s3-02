package eu.unicreditgroup.batch.processor;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;

import com.plateausystems.elms.webservices.axis.Caller;
import com.plateausystems.elms.webservices.axis.CloudPlateauLMSService;
import com.plateausystems.elms.webservices.axis.CloudWebSrvFactory;
import com.plateausystems.elms.webservices.axis.JobLocation;
import com.plateausystems.elms.webservices.axis.Organization;
import com.plateausystems.elms.webservices.axis.PlateauLMS;
import com.plateausystems.elms.webservices.axis.UpdateGeneralReferences;
import com.plateausystems.elms.webservices.axis.UpdateOrganizations;

import eu.unicreditgroup.config.ConfigParams;
import eu.unicreditgroup.dao.JobLocationDao;
import eu.unicreditgroup.mailer.ReportDataBean;
import eu.unicreditgroup.utils.SSLUtilities;

//import eu.unicreditgroup.ws.client.Caller;
//import eu.unicreditgroup.ws.client.JobLocation;
//import eu.unicreditgroup.ws.client.PlateauLMS;
//import eu.unicreditgroup.ws.client.PlateauLMSService;
//import eu.unicreditgroup.ws.client.UpdateGeneralReferences;

public class JobLocationDescriptionProcessor {
	
	Logger logger = Logger.getLogger(JobLocationDescriptionProcessor.class);
	
	private JobLocationDao jobLocationDao;
	private ConfigParams configParams;
	private ReportDataBean reportDataBean;
	
	private CloudWebSrvFactory cloudWebSrvFactory;
	
	private final String ORG_DOMAIN_ID = "SYSOBJ";
	
	public void execute(){
	
		//1. Display errors when there are different descriptions for the same code
		List<Map<String, Object>> differentDescErrorList = jobLocationDao.getDifferentDescErrorList();
		reportDataBean.setNumOfJobLocDescDifferentForTheSameCode(differentDescErrorList.size());
		if(differentDescErrorList.size()>0){
			logger.warn("There are "+differentDescErrorList.size()+" different descriptions for the same code:");
			Iterator<Map<String, Object>> differentDescErrorIterator = differentDescErrorList.iterator();
			Map<String, Object> differentDescError;
			while( differentDescErrorIterator.hasNext() ){
				differentDescError = differentDescErrorIterator.next();
				logger.warn("ID: "+differentDescError.get("USER_OFFICE_CODE")+", ABBR: "+differentDescError.get("USER_OFFICE_ABBR")+", DESC: "+ differentDescError.get("USER_OFFICE_DESC"));
			}
		}
		
		boolean canUpdateDictTable = false;
		try {
			//2. Update Job Location Description in Plateau
			List<Map<String, Object>> jobLocationList = jobLocationDao.getModifiedDescForSendToPlateau();
			logger.info(jobLocationList.size()+" Job Location Descriptions need to be updated in Plateau.");

			
			
			updateJobLocationDescriptionInPlateau(jobLocationList);
			
			// we need to update also descriptions for organizations because 
			// it stores now also job locations (departments)
			
			updateOrgDescriptionInLMS(jobLocationList);
			
			
			//After successful update in Plateau we can update dict table.
			canUpdateDictTable = true;
				
		} catch (Exception e) {
			logger.error("Error in updateJobLocationDescriptionInPlateau! Must skip update of old Job Location Descriptions in dict table!", e);
		}
		
		if(canUpdateDictTable){

			//3. Update old Job Location Descriptions in dict table with new values
			int updatedDesc = jobLocationDao.updateOldDescInDictTable();
			logger.info(updatedDesc+" old Job Location Descriptions have been updated in dict table.");
			reportDataBean.setNumOfJobLocDescUpdatesInDictTable(updatedDesc);
		}
		
		//4. Add new Job Loc Desc to dict table
		int insertedDesc = jobLocationDao.insertNewDescToDictTable();
		logger.info(insertedDesc+" new Job Location Descriptions have been inserted to dict table.");
		reportDataBean.setNumOfJobLocDescInsertsToDictTable(insertedDesc);
	}

	
	
	
	public void updateJobLocationDescriptionInPlateau( List<Map<String, Object>> jobLocationList ) throws Exception{
		
		// acccept all SSL certs
		SSLUtilities.trustAllHttpsCertificates();
		
		
		PlateauLMS cloudWS = cloudWebSrvFactory.getCloudWS(); 
		
		
		UpdateGeneralReferences updateGeneralReferences = new UpdateGeneralReferences();
		updateGeneralReferences.setCaller(cloudWebSrvFactory.getCaller());
		
		if(jobLocationList.size()>0){
			
			Iterator<Map<String, Object>> iterator = jobLocationList.iterator();
			Map<String, Object> jobLocationMap;
			String jobLocationId;
			String jobLocationDesc;
			
			while( iterator.hasNext() ){
				jobLocationMap = iterator.next();
				
				// USE this after change!!!!!!!!!!!!!!!!
				jobLocationId = jobLocationMap.get("CODE").toString();
				jobLocationDesc = jobLocationMap.get("DESCR").toString();
				
				//jobLocationId = jobLocationMap.get("USER_OFFICE_CODE")+"-"+jobLocationMap.get("USER_OFFICE_ABBR");
				//jobLocationDesc = jobLocationMap.get("USER_OFFICE_DESC").toString();
				
				JobLocation jobLocation = new JobLocation();
				jobLocation.setAction("UPDATE");
				jobLocation.setJobLocationID( jobLocationId );
				jobLocation.setJobLocationDescription( jobLocationDesc );

				updateGeneralReferences.getJobLocation().add(jobLocation);
				
				logger.info("ID: "+jobLocationId+", DESC: "+ jobLocationDesc);
			}

//			JobLocation jl = new JobLocation();		
//			jl.setAction("UPDATE");
//			jl.setJobLocationID("AL_FAKE");
//			jl.setJobLocationDescription("updated by WS_CLIENT");
//			updateGeneralReferences.getJobLocation().add(jl);

			cloudWS.updateGeneralReferences(updateGeneralReferences);
		}
		logger.info(jobLocationList.size()+" Job Location Descriptions have been updated by Plateau Web Service.");
		reportDataBean.setNumOfJobLocDescUpdatesSentByWS(jobLocationList.size());
		
	}

	/**
	 * 
	 * Since the cloud version the organization data in LMS is replaced 
	 * with job locations (offices or departments).
	 * So there's a need to update orgs descriptions the same way as descriptions for job locations
	 *  
	 * @param jobLocationList
	 * @throws Exception
	 */
	public void updateOrgDescriptionInLMS( List<Map<String, Object>> jobLocationList ) throws Exception{
		
		// acccept all SSL certs
		SSLUtilities.trustAllHttpsCertificates();
		
		
		PlateauLMS cloudWS = cloudWebSrvFactory.getCloudWS(); 
		
		//cloudWS.updateOrganizations(updateOrganizations);
		
		
		UpdateOrganizations updateOrganizations = new UpdateOrganizations();
		updateOrganizations.setCaller(cloudWebSrvFactory.getCaller());
		
		
		//UpdateGeneralReferences updateGeneralReferences = new UpdateGeneralReferences();
		//updateGeneralReferences.setCaller(cloudWebSrvFactory.getCaller());
		
		
		if(jobLocationList.size()>0){
			
			Iterator<Map<String, Object>> iterator = jobLocationList.iterator();
			Map<String, Object> jobLocationMap;
			String jobLocationId;
			String jobLocationDesc;
			
			while( iterator.hasNext() ){
				jobLocationMap = iterator.next();
				
				jobLocationId = jobLocationMap.get("CODE").toString();
				jobLocationDesc = jobLocationMap.get("DESCR").toString();
								
				Organization organization = new Organization();
				
				organization.setOrgID(jobLocationId);
				organization.setDescription(jobLocationDesc);
				organization.setDomainID(ORG_DOMAIN_ID);
				
				updateOrganizations.getOrganization().add(organization);
								
				logger.info("ORG ID: "+jobLocationId+", ORG DESC: "+ jobLocationDesc);
			}

			cloudWS.updateOrganizations(updateOrganizations);
		}
		logger.info(jobLocationList.size()+" Organizations descriptions have been updated by SF LMS Web Service.");
		//reportDataBean.setNumOfJobLocDescUpdatesSentByWS(jobLocationList.size());
		
	}

	

	public JobLocationDao getJobLocationDao() {
		return jobLocationDao;
	}

	public void setJobLocationDao(JobLocationDao jobLocationDao) {
		this.jobLocationDao = jobLocationDao;
	}

	public ConfigParams getConfigParams() {
		return configParams;
	}

	public void setConfigParams(ConfigParams configParams) {
		this.configParams = configParams;
	}

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	public CloudWebSrvFactory getCloudWebSrvFactory() {
		return cloudWebSrvFactory;
	}

	public void setCloudWebSrvFactory(CloudWebSrvFactory cloudWebSrvFactory) {
		this.cloudWebSrvFactory = cloudWebSrvFactory;
	}
	
	

}
