package eu.unicreditgroup.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import eu.unicreditgroup.batch.SourceTableConsumer;
import eu.unicreditgroup.beans.ChangedColInf;
import eu.unicreditgroup.beans.HistorySourceRow;
import eu.unicreditgroup.beans.User;
import eu.unicreditgroup.config.SourceTableConf;
import eu.unicreditgroup.mailer.ReportDataBean;


public class SapSourceHistoryDao {

	private SimpleJdbcTemplate simpleJdbcTemplate;
	
	private DataSource dataSource;
		
	private SourceTableConf sourceTableConf;
		
	private static int[] columnTypes;
	
	
			
	private String SQL_INSERT_INTO_HST_TABLE = "";	
	
		
	private ReportDataBean reportDataBean;
	
	
	public int getColRemovedDateIndex() {
						
		return sourceTableConf.getColRemovedDateIndex();
	}
	
	public String getColRemovedDateName() {

		return sourceTableConf.getColRemovedDateName();
	}

		
	public void setDataSource(DataSource dataSource) {
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
        this.dataSource = dataSource;
    }	
	
	private int columnsCount;
	
	/*
	private int getColumnsCount(){
		
		if (columnsCount == 0){
			columnsCount = sourceTableConf.getSourceColumnsCount() + sourceTableConf.getAdditionalColsConut();
		}
		return columnsCount;
	}*/
	
	private class HstSourceRowMapper implements RowMapper<HistorySourceRow> {
		@Override
		public HistorySourceRow mapRow(ResultSet rs, int rowNum) throws SQLException {
			HistorySourceRow hstSrcRow = new HistorySourceRow();
					
			String[] row = new String[sourceTableConf.getSourceAndAdditionalColumnsCount()];
			
			for ( int i = 0; i < sourceTableConf.getSourceAndAdditionalColumnsCount() ; i++ ){
				row[i] = rs.getString(i+1);
			}
			hstSrcRow.setData(row);			
			return hstSrcRow;
		}
	}
	
	
	private String getInsertIntoHistoryTableQuery(){
		
		if (StringUtils.isNotEmpty(SQL_INSERT_INTO_HST_TABLE)){
			return SQL_INSERT_INTO_HST_TABLE;
		}
		
		String sqlTemplate = " INSERT INTO HR_CONN_SAP_SRC_HST  VALUES ( %s )";
		
		int sourceColumnsCount = sourceTableConf.getSourceAndAdditionalColumnsCount();
		
		columnTypes = new int[sourceColumnsCount];
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < sourceColumnsCount ; i++){
			columnTypes[i] = java.sql.Types.VARCHAR;
			// build the inserting sql (?, ?, ?, ... )
			if (i > 0){
				sb.append(",");
			}			
			sb.append("?");			
		}		
		SQL_INSERT_INTO_HST_TABLE = String.format(sqlTemplate, sb.toString() );
		return SQL_INSERT_INTO_HST_TABLE; 		
	}
	
	// if result is NULL user doesn�t exists
	public HistorySourceRow getUser(String userId){
		
		String FIND_USER_SQL = "select * from hr_conn_sap_src_hst where user_id = '%s'";
		
		FIND_USER_SQL = String.format(FIND_USER_SQL, userId);
		
		List<HistorySourceRow> result = simpleJdbcTemplate.query(FIND_USER_SQL, new HstSourceRowMapper());
		
		if (result.size() == 0){
			return null;
		} else {
			return result.get(0);
		}		
	}
	
	
	
	@Transactional("ltp")
	public void addNewUser(String[] rowData){
				
		String[] newUser = Arrays.copyOf(rowData, sourceTableConf.getSourceAndAdditionalColumnsCount());
		
		List<Object[]> rows = new ArrayList<Object[]>();
    	rows.add( newUser );    	
    	simpleJdbcTemplate.batchUpdate( getInsertIntoHistoryTableQuery(), rows, columnTypes);    	
    	//update creation date    	
    	updateCreationDate( rowData[sourceTableConf.getUserIdIndex()]);
    	   					
	}
	
	private void updateCreationDate(String userId){
		
		String sql = "update hr_conn_sap_src_hst set (created_date) = to_date('%s','%s') where user_id = '%s'";
		sql = String.format(sql, reportDataBean.getSrcDataDeliveryDateAsString().toString(), reportDataBean.SQL_DATE_FORMAT, userId);
		simpleJdbcTemplate.update(sql);
	}

	public void updateRemovedDate(String userId){
		String sql = "update hr_conn_sap_src_hst set (removed_date) = to_date('%s','%s') where user_id = '%s'";
		sql = String.format(sql, reportDataBean.getSrcDataDeliveryDateAsString().toString(), reportDataBean.SQL_DATE_FORMAT, userId);
		simpleJdbcTemplate.update(sql);

	}
	
	public void updateLastUpdatedDate(String userId){
		String sql = "update hr_conn_sap_src_hst set (last_updated_date) = to_date('%s','%s') where user_id = '%s'";
		sql = String.format(sql, reportDataBean.getSrcDataDeliveryDateAsString().toString(), reportDataBean.SQL_DATE_FORMAT, userId);
		simpleJdbcTemplate.update(sql);		
	}

	public SourceTableConf getSourceTableConf() {
		return sourceTableConf;
	}

	public void setSourceTableConf(SourceTableConf sourceTableConf) {
		this.sourceTableConf = sourceTableConf;
	}

	/**
	 * Update user record in the history table, 
	 * modify the last updated column with current time
	 * 
	 * @param userId
	 * @param changedColumns
	 */
	@Transactional("ltp")
	public void updateUserRecord(String userId,
			List<ChangedColInf> changedColumns) {
		
		String sqlTemplate = "update hr_conn_sap_src_hst set ( %s ) = ? where user_id = ? ";
		String updateSql; 
		
		for (ChangedColInf changedCol : changedColumns){
			
			String newValue = changedCol.getNewValue();
			
			// make sure the null is set
			if (StringUtils.isEmpty(newValue)){
				newValue = "";
			}
			
			updateSql = String.format(sqlTemplate,changedCol.getColumnName() );
			
			simpleJdbcTemplate.update(updateSql, new Object[]{ newValue, userId } );
			
			//logger.debug("Updating col " + changedCol.getColumnName() + ", " + changedCol.getNewValue() + ", " + userId);
		}
		
		// set update date
		updateLastUpdatedDate(userId);		
				
	}
	
	/**
	 * Return all active users with null or not exisitng line manager from the source table
	 *  
	 * @return
	 */
	public List<String> getUsersWithInvalidLineManager(){
					
	String sql =" with " +  
				" lm as " +  /* list of existing and acitve in Plateau line managers */
				" (  " + 
				" select s.user_id as line_manager " + 
				" from hr_conn_sap_src s " + 
				" where s.user_id in (select distinct s2.line_manager_id from hr_conn_sap_src s2 where s2.line_manager_id is not null) " +  
				" and s.user_status_id = 3 " + 
				" ), " + 
				" lm_excp as " + /* line managers exceptions for HR Connector */
				" ( " +
				" select user_id from hr_conn_lm_excp " +
				" ) " + 
				" select distinct src.user_id " +  /* active users with set but not existing line manager */
				" from hr_conn_sap_src src " +  
				" where " +  
				" src.line_manager_id is not null " + 
				" and src.user_status_id = 3 " + 
				" and src.line_manager_id not in (select * from lm) " +
				" and src.user_id not in (select * from lm_excp) " +
				" union all " + 
				" select distinct src.user_id " +  /* active users with not set line manager */
				" from hr_conn_sap_src src " +  
				" where " +  
				" src.line_manager_id is null " + 
				" and src.user_status_id = 3 " +
				" and src.user_id not in (select * from lm_excp) ";	
		
		return simpleJdbcTemplate.query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {								
				return rs.getString("user_id");
			}				
		});		
	}	
	
	//TODO cleanup
	// 2012-06-11 - refactoring processing of users with invalid line managers simplified  
	/*
	**
	 * Return all new users from the source history table
	 * who were not removed, are active and were *not* identified previously 
	 * @return
	 *
	public List<String> getNewUsersWithNonExisitingLineManager(){

		String sql = " select distinct user_id from hr_conn_sap_src_hst sh where line_manager_id not in " +
			"  (select user_id   from hr_conn_sap_src_hst hst " +
			"  where hst.user_id in (select distinct line_manager_id from hr_conn_sap_src_hst where line_manager_id is not null) )" +
			" and sh.is_lm_not_present is null and sh.removed_date is null and sh.user_status_id = 3";
		
		return simpleJdbcTemplate.query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {								
				return rs.getString("user_id");
			}				
		});		
		
	}*/
	
	 
	
	

	public ReportDataBean getReportDataBean() {
		return reportDataBean;
	}

	public void setReportDataBean(ReportDataBean reportDataBean) {
		this.reportDataBean = reportDataBean;
	}

	
	/**
	 * Update column REMOVED_DATE
	 * for all users that are not longer present in the 
	 * SAP source file 
	 * 
	 */
	@Transactional("ltp")
	public int updateRemovedUsers() {
		
		String sql = "select user_id from hr_conn_sap_src_hst where user_id not in " + 
					" (select user_id from hr_conn_sap_src) and removed_date is null";
		
		
		// get removed users
		List<String> removedUserIds = simpleJdbcTemplate.query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {								
				return rs.getString("user_id");
			}				
		});
				
		// set removed date fr removed rows 
		for (String userId : removedUserIds){
			updateRemovedDate(userId);
		}
		
		if (removedUserIds == null){
			return 0;
		} else {
			return removedUserIds.size();
		}
		
	}

	public void resetRemovedDate(String userId) {
		String sql = "update hr_conn_sap_src_hst set (removed_date) = null where user_id = '%s'";
		sql = String.format(sql, userId);
		simpleJdbcTemplate.update(sql);		
	}

	public long getNewUsersCount() {
		
		String sql = "select count(*) from hr_conn_sap_src_hst where created_date = to_date('%s','%s')";
		sql = String.format(sql, reportDataBean.getSrcDataDeliveryDateAsString().toString(), reportDataBean.SQL_DATE_FORMAT);
		
		return simpleJdbcTemplate.queryForLong(sql);
		
	}

	public long getUpdatedUsersCount() {
		String sql = "select count(*) from hr_conn_sap_src_hst where last_updated_date = to_date('%s','%s')";
		sql = String.format(sql, reportDataBean.getSrcDataDeliveryDateAsString().toString(), reportDataBean.SQL_DATE_FORMAT);
		
		return simpleJdbcTemplate.queryForLong(sql);
	}
	
	
	//TODO refactor some day to be more efficient...
	public boolean isUserLineManagerPresent(String userId){
		String sql = "select user_id from hr_conn_sap_src where user_id in " +
		" (select  line_manager_id from hr_conn_sap_src where user_id = '%s')";
		
		sql = String.format(sql, userId);
		
		List<String> lm = simpleJdbcTemplate.query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {								
				return rs.getString("user_id");
			}				
		});		
		
		if ( lm.size()  == 0 ){
			return false;
		} else {
			return true;
		}
	}
	

	//TODO cleanup
	// 2012-06-11 - refactoring processing of users with invalid line managers simplified	
	/*
	public int updateOldUsersWithNonExLineManager() {
		
		String sql = " select user_id from hr_conn_sap_src_hst  where is_lm_not_present = 'Y' " +
		" and removed_date is null and user_status_id = 3";
		
		
		String update = "update hr_conn_sap_src_hst set is_lm_not_present ='' where user_id = '%s'";
		
		List<String> oldUsersWithNonExLM = simpleJdbcTemplate.query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {								
				return rs.getString("user_id");
			}				
		});		
		
		int result = 0;
		for(String user : oldUsersWithNonExLM )
		{
			if (isUserLineManagerPresent(user ) == true) {
				result += 1;
				// LM is now present so reset the flag
				simpleJdbcTemplate.update(String.format(update,user));
			}
		}
		return result;
	}
	*/

	//TODO cleanup
	// 2012-06-11 - refactoring processing of users with invalid line managers simplified
	/*
	public void updateNewUsersWithNonExLineManager(	List<String> usersWithNonExistingLM ) {		
		String update = "update hr_conn_sap_src_hst set is_lm_not_present ='Y' where user_id = '%s'";
		for(String user : usersWithNonExistingLM )
		{
			simpleJdbcTemplate.update(String.format(update,user));			
		}		
	}
	*/

	
	//TODO UPDATE WHEN THERE ARE NEW SOURCE COLUMNS
	/**
	 * Update all columns of the history table 
	 * when they exist in the hr_conn_sap_src file
	 */
	public void mergeHistoryTable() {
		String sql = " MERGE INTO hr_conn_sap_src_hst h USING hr_conn_sap_src s ON (h.user_id = s.user_id ) " +
		" WHEN MATCHED THEN " +
		"   UPDATE SET  " +
		"  h.USER_STATUS_ID =s.USER_STATUS_ID, " +
		" h.USER_STATUS_DESC=s.USER_STATUS_DESC, " +
		" h.FIRST_NAME=s.FIRST_NAME, " +
		" h.LAST_NAME=s.LAST_NAME, " +
		" h.GENDER=s.GENDER, " +
		" h.GLOBAL_JOB_CODE=s.GLOBAL_JOB_CODE, " +
		" h.GLOBAL_JOB_ABBR=s.GLOBAL_JOB_ABBR, " +
		" h.GLOBAL_JOB_DESC=s.GLOBAL_JOB_DESC, " +
		" h.USER_OFFICE_CODE=s.USER_OFFICE_CODE, " +
		" h.USER_OFFICE_ABBR=s.USER_OFFICE_ABBR, " +
		" h.USER_OFFICE_DESC=s.USER_OFFICE_DESC, " +
		" h.HOST_LEGAL_ENT_CODE=s.HOST_LEGAL_ENT_CODE, " +
		" h.HOST_LEGAL_ENT_DESC=s.HOST_LEGAL_ENT_DESC, " +
		" h.CONTRACT_TYPE_CODE=s.CONTRACT_TYPE_CODE, " +
		" h.CONTRACT_TYPE_DESC=s.CONTRACT_TYPE_DESC, " +
		" h.CONTRACT_DETAILS=s.CONTRACT_DETAILS, " +
		" h.OFFICE_ADDR=s.OFFICE_ADDR, " +
		" h.OFFICE_CITY=s.OFFICE_CITY, " +
		" h.OFFICE_PROVINCE=s.OFFICE_PROVINCE, " +
		" h.OFFICE_PROVINCE_DESC=s.OFFICE_PROVINCE_DESC, " +
		" h.OFFICE_POSTAL_CODE=s.OFFICE_POSTAL_CODE, " +
		" h.OFFICE_COUNTRY=s.OFFICE_COUNTRY, " +
		" h.OFFICE_COUNTRY_DESC=s.OFFICE_COUNTRY_DESC, " +
		" h.OFFICE_EMAIL=s.OFFICE_EMAIL, " +
		" h.HIRE_DATE=s.HIRE_DATE, " +
		" h.TERM_DATE=s.TERM_DATE, " +
		" h.LINE_MANAGER_ID=s.LINE_MANAGER_ID, " +
		" h.RESUME=s.RESUME, " +
		" h.COST_CENTER_CODE=s.COST_CENTER_CODE, " +
		" h.OFFICE_MOBILE_PHONE=s.OFFICE_MOBILE_PHONE, " +
		" h.OFFICE_FIX_PHONE=s.OFFICE_FIX_PHONE, " +
		" h.FISCAL_CODE=s.FISCAL_CODE, " +
		" h.ORG_1ST_LEVEL_CODE=s.ORG_1ST_LEVEL_CODE, " +
		" h.ORG_1ST_LEVEL_ABBR=s.ORG_1ST_LEVEL_ABBR, " +
		" h.ORG_1ST_LEVEL_DESC=s.ORG_1ST_LEVEL_DESC, " +
		" h.ORG_2ND_LEVEL_CODE=s.ORG_2ND_LEVEL_CODE, " +
		" h.ORG_2ND_LEVEL_ABBR=s.ORG_2ND_LEVEL_ABBR, " +
		" h.ORG_2ND_LEVEL_DESC=s.ORG_2ND_LEVEL_DESC, " +
		" h.HOME_LEGAL_ENT_CODE=s.HOME_LEGAL_ENT_CODE, " +
		" h.HOME_LEGAL_ENT_DESC=s.HOME_LEGAL_ENT_DESC, " +
		" h.DIVISION_CODE=s.DIVISION_CODE, " +
		" h.DIVISION_ABBR=s.DIVISION_ABBR, " +
		" h.DIVISION_DESC=s.DIVISION_DESC, " +
		" h.COMPETENCE_LINE_CODE=s.COMPETENCE_LINE_CODE, " +
		" h.COMPETENCE_LINE_ABBR=s.COMPETENCE_LINE_ABBR, " +
		" h.COMPETENCE_LINE_DESC=s.COMPETENCE_LINE_DESC, " +
		" h.HRBP_ID=s.HRBP_ID, " +
		" h.HRBP=s.HRBP, " +
		" h.LINE_MANAGER=s.LINE_MANAGER, " +
		" h.GLOBAL_JOB_BAND_CODE=s.GLOBAL_JOB_BAND_CODE, " +
		" h.GLOBAL_JOB_BAND_ABBR=s.GLOBAL_JOB_BAND_ABBR, " +
		" h.GLOBAL_JOB_BAND_DESC=s.GLOBAL_JOB_BAND_DESC, " +
		" h.JOB_ROLE_CODE=s.JOB_ROLE_CODE, " +
		" h.JOB_ROLE_ABBR=s.JOB_ROLE_ABBR, " +
		" h.JOB_ROLE_DESC=s.JOB_ROLE_DESC, " +
		" h.PAY_GRADE_CODE=s.PAY_GRADE_CODE, " +
		" h.PAY_GRADE_DESC=s.PAY_GRADE_DESC, " +
		" h.PREV_JOB_ROLE_CODE=s.PREV_JOB_ROLE_CODE, " +
		" h.PREV_JOB_ROLE_ABBR=s.PREV_JOB_ROLE_ABBR, " +
		" h.PREV_JOB_ROLE_DESC=s.PREV_JOB_ROLE_DESC, " +
		" h.SECOND_PREV_JOB_ROLE_CODE=s.SECOND_PREV_JOB_ROLE_CODE, " +
		" h.SECOND_PREV_JOB_ROLE_ABBR=s.SECOND_PREV_JOB_ROLE_ABBR, " +
		" h.SECOND_PREV_JOB_ROLE_DESC=s.SECOND_PREV_JOB_ROLE_DESC, " +
		" h.JOB_ROLE_START_DATE=s.JOB_ROLE_START_DATE, " +
		" h.DATE_OF_BIRTH=s.DATE_OF_BIRTH, " +
		" h.NATION_ID=s.NATION_ID, " +
		" h.NATION_DESC=s.NATION_DESC, " +
		" h.EXPATRIATE=s.EXPATRIATE, " +
		" h.EDU_TITLE_ID=s.EDU_TITLE_ID, " +
		" h.EDU_TITLE_DESC=s.EDU_TITLE_DESC, " +
		" h.LOCAL_LANG=s.LOCAL_LANG, " +
		" h.LOCAL_LANG_DESC=s.LOCAL_LANG_DESC, " +
		" h.CURRENCY_ID=s.CURRENCY_ID, " +
		" h.COST_CENTER_DESC=s.COST_CENTER_DESC, " +
		" h.USER_SAP_ID=s.USER_SAP_ID, " +
		" h.SUPERVISOR_SAP_ID=s.SUPERVISOR_SAP_ID, " +
		" h.HRBP_SAP_ID=s.HRBP_SAP_ID, " +
		" h.JOB_SENIORITY_CODE=s.JOB_SENIORITY_CODE, " +
		" h.JOB_SENIORITY_DESC=s.JOB_SENIORITY_DESC, " +
		" h.SERVICE_LINE_CODE=s.SERVICE_LINE_CODE, " +
		" h.SERVICE_LINE_DESC=s.SERVICE_LINE_DESC, " +
		" h.BUSINESS_LINE_CODE=s.BUSINESS_LINE_CODE, " +
		" h.BUSINESS_LINE_DESC=s.BUSINESS_LINE_DESC, " +
		" h.EXPAT_TYPE=s.EXPAT_TYPE, " +
		" h.FUNCTIONAL_RESP=s.FUNCTIONAL_RESP, " +
		" h.FTE=s.FTE, " +
		" h.ORG_DISTRICT_CODE=s.ORG_DISTRICT_CODE, " +
		" h.ORG_DISTRICT_NAME=s.ORG_DISTRICT_NAME, " +
		" h.ORG_DISTRICT_DESC=s.ORG_DISTRICT_DESC, " +
		" h.ORG_UNIT_LOC_TYPE=s.ORG_UNIT_LOC_TYPE, " +
		" h.GLOBAL_JOB_LDR_CODE=s.GLOBAL_JOB_LDR_CODE, " +
		" h.GLOBAL_JOB_LDR_ABBR=s.GLOBAL_JOB_LDR_ABBR, " +
		" h.GLOBAL_JOB_LDR_DESC=s.GLOBAL_JOB_LDR_DESC, " +
		" h.GLOBAL_JOB_LVL_CODE=s.GLOBAL_JOB_LVL_CODE, " +
		" h.GLOBAL_JOB_LVL_ABBR=s.GLOBAL_JOB_LVL_ABBR, " +
		" h.GLOBAL_JOB_LVL_DESC=s.GLOBAL_JOB_LVL_DESC," +
		 "h.PRODUCT_LINE_CODE=s.PRODUCT_LINE_CODE," +
		 "h.PRODUCT_LINE_DESC=s.PRODUCT_LINE_DESC";
				
		simpleJdbcTemplate.update(sql);
		
	}
	
}
