package eu.unicreditgroup.utils;

import org.springframework.util.StringUtils;

public class ParametersListParser {

	
	private static final String DELIMITERS = " ,;";

	/**
	 * Parses given string and returns
	 * 
	 * @param paramsString
	 * @return
	 */
	public String[] parse(String paramsString) {
		
		if (paramsString == null){
			paramsString = "";
		}
		
		return StringUtils.tokenizeToStringArray(paramsString, DELIMITERS, true, true);
		
		
	}

}
