package eu.unicreditgroup.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;


/**
 * OLD VERSION BASED ON INI4J
 * 
 
public class IniConfigModifier extends Wini
{
	
	public static void main(String[] args) 
	{
		try{
			
			if(args.length > 1)
			{
				//load configuration file
				String fileName = args[0];
				IniConfigModifier self = new IniConfigModifier(new File(fileName));
				Ini.Section section = self.get(null);
				
				for (int i = 1; i  < args.length ; i++ )
				{
					//pamietaj zeby rozbic wzgledem  =
					String[] paramValue = args[i].split("=");
					section.put( paramValue[0], paramValue[1] );
				}
				self.store();
			}
			else
			{
				System.out.println("Missing argurements! Correct usage: <fileURI> <expression1> [ [expression2]...[expressionN] ]");
			}
		}
		catch(IOException exc)
		{
			System.out.println("Cannot modifie configuration file, incorrect parameters or empty file");
		}
		catch(Exception exc)
		{
			System.out.println("Unexcepted costam");
		}
		finally
		{
			
		}
	}
	
	public IniConfigModifier(File file) throws IOException
	{
		super(file);
	}
}
*/

public class IniConfigModifier extends Properties
{
	public static void main(String[] args) {
		try{
			
			if(args.length > 1)
			{
				//load configuration file
				String fileName = args[0];
				IniConfigModifier self = new IniConfigModifier();
				self.load(new FileInputStream(fileName));
				
				for (int i = 1; i  < args.length ; i++ )
				{
					//pamietaj zeby rozbic wzgledem  =
					String[] paramValue = args[i].split("=");
					if(paramValue.length != 2)
					{
						System.out.println("Incorect expression at position: "+i+" ("+args[i]+")");
						System.exit(0);
					}
					else{
						self.setProperty( paramValue[0], paramValue[1] );	
					}
					
				}
				self.store(new FileWriter(fileName), null);
			}
			else
			{
				System.out.println("Missing argurements! Correct usage: <fileURI> <expression1> [ [expression2]...[expressionN] ]");
			}
			System.out.println( "Configuration file modified succesfully!" );
		}
		catch(IOException exc)
		{
			System.out.println( "File connection Error: Cannot modifie configuration file!" );
		}
		catch(Exception exc)
		{
			System.out.println( "File connection Error: "+exc.getMessage() );
		}
		finally
		{
			
		}
	}
	
	public IniConfigModifier() throws IOException{
		super();
	}
}
