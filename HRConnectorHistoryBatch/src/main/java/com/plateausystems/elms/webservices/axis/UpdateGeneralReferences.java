
package com.plateausystems.elms.webservices.axis;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Caller" type="{http://axis.webservices.elms.plateausystems.com}Caller"/>
 *         &lt;element name="JobLocation" type="{http://axis.webservices.elms.plateausystems.com}JobLocation" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "caller",
    "jobLocation"
})
@XmlRootElement(name = "UpdateGeneralReferences")
public class UpdateGeneralReferences {

    @XmlElement(name = "Caller", required = true, nillable = true)
    protected Caller caller;
    @XmlElement(name = "JobLocation", required = true, nillable = true)
    protected List<JobLocation> jobLocation;

    /**
     * Gets the value of the caller property.
     * 
     * @return
     *     possible object is
     *     {@link Caller }
     *     
     */
    public Caller getCaller() {
        return caller;
    }

    /**
     * Sets the value of the caller property.
     * 
     * @param value
     *     allowed object is
     *     {@link Caller }
     *     
     */
    public void setCaller(Caller value) {
        this.caller = value;
    }

    /**
     * Gets the value of the jobLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the jobLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getJobLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JobLocation }
     * 
     * 
     */
    public List<JobLocation> getJobLocation() {
        if (jobLocation == null) {
            jobLocation = new ArrayList<JobLocation>();
        }
        return this.jobLocation;
    }

}
