
package com.plateausystems.elms.webservices.axis;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomColumnVOList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomColumnVOList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomColumn" type="{http://axis.webservices.elms.plateausystems.com}CustomColumnWSVO" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomColumnVOList", propOrder = {
    "customColumn"
})
public class CustomColumnVOList {

    @XmlElement(name = "CustomColumn", required = true, nillable = true)
    protected List<CustomColumnWSVO> customColumn;

    /**
     * Gets the value of the customColumn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customColumn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomColumn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomColumnWSVO }
     * 
     * 
     */
    public List<CustomColumnWSVO> getCustomColumn() {
        if (customColumn == null) {
            customColumn = new ArrayList<CustomColumnWSVO>();
        }
        return this.customColumn;
    }

}
