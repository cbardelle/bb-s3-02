
package com.plateausystems.elms.webservices.axis;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Caller" type="{http://axis.webservices.elms.plateausystems.com}Caller"/>
 *         &lt;element name="CurriculumID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CurriculumTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DomainIDs" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "caller",
    "curriculumID",
    "curriculumTitle",
    "isActive",
    "domainIDs"
})
@XmlRootElement(name = "CurriculaSearchCriteria")
public class CurriculaSearchCriteria {

    @XmlElement(name = "Caller", required = true, nillable = true)
    protected Caller caller;
    @XmlElement(name = "CurriculumID", required = true, nillable = true)
    protected String curriculumID;
    @XmlElement(name = "CurriculumTitle", required = true, nillable = true)
    protected String curriculumTitle;
    @XmlElement(name = "IsActive", required = true, type = Boolean.class, nillable = true)
    protected Boolean isActive;
    @XmlElement(name = "DomainIDs", required = true, nillable = true)
    protected List<String> domainIDs;

    /**
     * Gets the value of the caller property.
     * 
     * @return
     *     possible object is
     *     {@link Caller }
     *     
     */
    public Caller getCaller() {
        return caller;
    }

    /**
     * Sets the value of the caller property.
     * 
     * @param value
     *     allowed object is
     *     {@link Caller }
     *     
     */
    public void setCaller(Caller value) {
        this.caller = value;
    }

    /**
     * Gets the value of the curriculumID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurriculumID() {
        return curriculumID;
    }

    /**
     * Sets the value of the curriculumID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurriculumID(String value) {
        this.curriculumID = value;
    }

    /**
     * Gets the value of the curriculumTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurriculumTitle() {
        return curriculumTitle;
    }

    /**
     * Sets the value of the curriculumTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurriculumTitle(String value) {
        this.curriculumTitle = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the domainIDs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the domainIDs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDomainIDs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getDomainIDs() {
        if (domainIDs == null) {
            domainIDs = new ArrayList<String>();
        }
        return this.domainIDs;
    }

}
